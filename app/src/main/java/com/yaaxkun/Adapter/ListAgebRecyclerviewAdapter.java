package com.yaaxkun.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yaaxkun.Model.Row;
import com.yaaxkun.R;

import java.util.List;

public class ListAgebRecyclerviewAdapter extends RecyclerView.Adapter<ListAgebRecyclerviewAdapter.MyViewHolder>{

    private Context mContext;
    private List<Row> agebs;

    public ListAgebRecyclerviewAdapter(Context mContext, List<Row> agebs) {
        this.mContext = mContext;
        this.agebs = agebs;
    }

    @Override
    public ListAgebRecyclerviewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_item_list_agebs, parent, false);
        return new ListAgebRecyclerviewAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ListAgebRecyclerviewAdapter.MyViewHolder holder, final int position) {

        holder.encuesta_id.setText(String.valueOf(agebs.get(position).getEncuestaId()));
        holder.hora_inicio.setText(agebs.get(position).getEncuestaInicio());
        holder.hora_final.setText(agebs.get(position).getEncuestaFin());
        holder.encuestador_id.setText(String.valueOf(agebs.get(position).getEncuestadorId()));
        holder.estatus.setText(agebs.get(position).getStatus());
        holder.latitud.setText(String.valueOf(agebs.get(position).getEncuestaLatitude()));
        holder.longitud.setText(String.valueOf(agebs.get(position).getEncuestaLongitude()));
        holder.instrumento_id.setText(String.valueOf(agebs.get(position).getInstrumentoId()));
        holder.ageb_id.setText(String.valueOf(agebs.get(position).getAgebId()));
        holder.grabado.setText(agebs.get(position).getGrabado());
    }

    @Override
    public int getItemCount() {
        return agebs.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView encuesta_id, hora_inicio, hora_final, encuestador_id, estatus, latitud, longitud, instrumento_id, ageb_id, grabado;

        public MyViewHolder(View itemView){

            super(itemView);
            encuesta_id = (TextView) itemView.findViewById(R.id.encuesta_id);
            hora_inicio = (TextView) itemView.findViewById(R.id.hora_inicio);
            hora_final = (TextView) itemView.findViewById(R.id.hora_final);
            encuestador_id = (TextView) itemView.findViewById(R.id.encuestador_id);
            estatus = (TextView) itemView.findViewById(R.id.estatus);
            latitud = (TextView) itemView.findViewById(R.id.latitud);
            longitud = (TextView) itemView.findViewById(R.id.longitud);
            instrumento_id = (TextView) itemView.findViewById(R.id.instrumento_id);
            ageb_id = (TextView) itemView.findViewById(R.id.ageb_id);
            grabado = (TextView) itemView.findViewById(R.id.grabado);
        }
    }
}