package com.yaaxkun.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yaaxkun.Activity.ListAdminAgebActivity;
import com.yaaxkun.Model.Estadistica;
import com.yaaxkun.R;

import java.util.List;

public class AdminAgebRecyclerviewAdapter extends RecyclerView.Adapter<AdminAgebRecyclerviewAdapter.MyViewHolder>{

    private Context mContext;
    private List<Estadistica> estadisticas;

    public AdminAgebRecyclerviewAdapter(Context mContext, List<Estadistica> estadisticas) {
        this.mContext = mContext;
        this.estadisticas = estadisticas;
    }

    @Override
    public AdminAgebRecyclerviewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_item_admin_ageb, parent, false);
        return new AdminAgebRecyclerviewAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdminAgebRecyclerviewAdapter.MyViewHolder holder, final int position) {

        holder.ageb.setText(String.valueOf(estadisticas.get(position).getAgebId()));
        holder.total.setText(String.valueOf(estadisticas.get(position).getTotal()));
        holder.cards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("mensaje", estadisticas.get(position).getAgebId());
                Intent intent = new Intent(mContext, ListAdminAgebActivity.class);
                intent.putExtra("ageb", estadisticas.get(position).getAgebId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return estadisticas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        CardView cards;
        TextView ageb, total;

        public MyViewHolder(View itemView){

            super(itemView);
            cards = (CardView) itemView.findViewById(R.id.cardviewadminageb);
            ageb = (TextView) itemView.findViewById(R.id.textAgebAdmin);
            total = (TextView) itemView.findViewById(R.id.textTotalAdmin);
        }
    }
}
