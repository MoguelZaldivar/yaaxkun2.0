package com.yaaxkun.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yaaxkun.Model.Encuesta;

import com.yaaxkun.R;

import java.util.List;

public class ListAdminAgebRecyclerviewAdapter extends RecyclerView.Adapter<ListAdminAgebRecyclerviewAdapter.MyViewHolder>{

    private Context mContext;
    private String agebId;
    private String motivo;
    private List<Encuesta> agebs;

    public ListAdminAgebRecyclerviewAdapter(Context mContext, String agebId, String motivo, List<Encuesta> agebs) {
        this.mContext = mContext;
        this.agebId = agebId;
        this.motivo = motivo;
        this.agebs = agebs;
    }

    @Override
    public ListAdminAgebRecyclerviewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_item_admin_list_agebs, parent, false);
        return new ListAdminAgebRecyclerviewAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ListAdminAgebRecyclerviewAdapter.MyViewHolder holder, final int position) {

        holder.encuesta_id.setText(String.valueOf(agebs.get(position).getEncuestaId()));
        holder.hora_inicio.setText(agebs.get(position).getEncuestaInicio());
        holder.hora_final.setText(agebs.get(position).getEncuestaFin());
        holder.encuestador_id.setText(agebs.get(position).getEncuestador());
        holder.estatus.setText(agebs.get(position).getStatus());
        holder.latitud.setText(String.valueOf(agebs.get(position).getEncuestaLatitude()));
        holder.longitud.setText(String.valueOf(agebs.get(position).getEncuestaLongitude()));
        holder.ageb_id.setText(agebId);
        holder.grabado.setText(agebs.get(position).getGrabado());
    }

    @Override
    public int getItemCount() {
        return agebs.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView encuesta_id, hora_inicio, hora_final, encuestador_id, estatus, latitud, longitud, ageb_id, grabado;

        public MyViewHolder(View itemView){

            super(itemView);
            encuesta_id = (TextView) itemView.findViewById(R.id.encuesta_idAdmin);
            hora_inicio = (TextView) itemView.findViewById(R.id.hora_inicioAdmin);
            hora_final = (TextView) itemView.findViewById(R.id.hora_finalAdmin);
            encuestador_id = (TextView) itemView.findViewById(R.id.encuestador_idAdmin);
            estatus = (TextView) itemView.findViewById(R.id.estatusAdmin);
            latitud = (TextView) itemView.findViewById(R.id.latitudAdmin);
            longitud = (TextView) itemView.findViewById(R.id.longitudAdmin);
            ageb_id = (TextView) itemView.findViewById(R.id.ageb_idAdmin);
            grabado = (TextView) itemView.findViewById(R.id.grabadoAdmin);
        }
    }
}