package com.yaaxkun.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.yaaxkun.Activity.ListAgebActivity;
import com.yaaxkun.Model.IdAgeb;
import com.yaaxkun.Model.Row;
import com.yaaxkun.R;

import java.util.List;

public class AgebRecyclerviewAdapter extends RecyclerView.Adapter<AgebRecyclerviewAdapter.MyViewHolder>{

    private Context mContext;
    private List<IdAgeb> agebs;
    private List<Row> row;

    public AgebRecyclerviewAdapter(Context mContext, List<IdAgeb> agebs) {
        this.mContext = mContext;
        this.agebs = agebs;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_item_ageb, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.ageb.setText(String.valueOf(agebs.get(position).getAgebId()));
        holder.total.setText(String.valueOf(agebs.get(position).getRowCount()));
        holder.objetivo.setText(String.valueOf(agebs.get(position).getObjetivo()));
        holder.cards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row = agebs.get(position).getRows();

                SharedPreferences sharedPref = mContext.getSharedPreferences("Login", Context.MODE_PRIVATE);
                Gson gson = new Gson();
                String json = gson.toJson(row);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("ListAgeb", json);
                editor.apply();

                Intent intent = new Intent(mContext, ListAgebActivity.class);
                intent.putExtra("valid", true);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return agebs.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        CardView cards;
        TextView ageb, total, objetivo;

        public MyViewHolder(View itemView){

            super(itemView);
            cards = (CardView) itemView.findViewById(R.id.cardviewageb);
            ageb = (TextView) itemView.findViewById(R.id.textAgeb);
            total = (TextView) itemView.findViewById(R.id.textTotal);
            objetivo = (TextView) itemView.findViewById(R.id.textObjetivo);
        }
    }
}
