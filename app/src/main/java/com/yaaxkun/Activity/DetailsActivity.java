package com.yaaxkun.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.yaaxkun.Interface.ApiClient;
import com.yaaxkun.Interface.Interface;
import com.yaaxkun.Model.ReportXEncuestados;
import com.yaaxkun.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        final TextView validas = (TextView) findViewById(R.id.textValidas);
        final TextView invalidas = (TextView) findViewById(R.id.textInvalidas);
        final CardView cardValidas = (CardView) findViewById(R.id.cardValidas);
        final CardView cardInvalidas = (CardView) findViewById(R.id.cardInvalidas);

        SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        final String token = sharedPref.getString("token", "");

        Call<ReportXEncuestados> call = ApiClient.getApiClient().create(Interface.class).reportXEncuestadoe(token);
        call.enqueue(new Callback<ReportXEncuestados>() {
            @Override
            public void onResponse(Call<ReportXEncuestados> call, Response<ReportXEncuestados> response) {
                if (response.code() == 400) {
                    Toast.makeText(getApplicationContext(), "Error 400", Toast.LENGTH_LONG).show();
                }else if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("sucessfull")) {
                            validas.setText(String.valueOf(response.body().getValidas().getRowCount()));
                            invalidas.setText(String.valueOf(response.body().getInvalidas().getRowCount()));

                            cardValidas.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(DetailsActivity.this, DetailsXAgebActivity.class);
                                    startActivity(intent);
                                }
                            });

                            cardInvalidas.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(DetailsActivity.this, DetailsXAgebInvalidActivity.class);
                                    intent.putExtra("valid", false);
                                    startActivity(intent);
                                }
                            });
                        }
                    } else
                        Toast.makeText(getApplicationContext(), "Error en response.body()", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<ReportXEncuestados> call, Throwable t) {

            }
        });
    }
}
