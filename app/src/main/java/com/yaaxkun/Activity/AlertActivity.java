package com.yaaxkun.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.ContactsContract;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.yaaxkun.R;

public class AlertActivity extends AppCompatActivity {

    private Button save_button;
    private EditText num1, num2, num3, num4, num5;
    private ImageView cont1, cont2, cont3, cont4, cont5;
    private CardView alert;

    private final int PICK_CONTACT1 = 1;
    private final int PICK_CONTACT2 = 2;
    private final int PICK_CONTACT3 = 3;
    private final int PICK_CONTACT4 = 4;
    private final int PICK_CONTACT5 = 5;
    private static final int REQUEST_RUNTIME_PERMISSION = 123;

    private double lat = 0.0;
    private double lng = 0.0;
    private String phoneNumber;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertss);

        save_button = (Button) findViewById(R.id.save_button);
        num1 = (EditText) findViewById(R.id.num1);
        num2 = (EditText) findViewById(R.id.num2);
        num3 = (EditText) findViewById(R.id.num3);
        num4 = (EditText) findViewById(R.id.num4);
        num5 = (EditText) findViewById(R.id.num5);
        cont1 = (ImageView) findViewById(R.id.cont1);
        cont2 = (ImageView) findViewById(R.id.cont2);
        cont3 = (ImageView) findViewById(R.id.cont3);
        cont4 = (ImageView) findViewById(R.id.cont4);
        cont5 = (ImageView) findViewById(R.id.cont5);
        alert = (CardView) findViewById(R.id.alertCard);

        final SharedPreferences prefe = getSharedPreferences("Login",Context.MODE_PRIVATE);

        String contacto1num = prefe.getString("num1", "");
        String contacto2num = prefe.getString("num2", "");
        String contacto3num = prefe.getString("num3", "");
        String contacto4num = prefe.getString("num4", "");
        String contacto5num = prefe.getString("num5", "");

        num1.setText(contacto1num);
        num2.setText(contacto2num);
        num3.setText(contacto3num);
        num4.setText(contacto4num);
        num5.setText(contacto5num);

        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String n1 = num1.getText().toString();
                String n2 = num2.getText().toString();
                String n3 = num3.getText().toString();
                String n4 = num4.getText().toString();
                String n5 = num5.getText().toString();

                SharedPreferences.Editor editor = prefe.edit();

                editor.putString("num1", n1);
                editor.putString("num2", n2);
                editor.putString("num3", n3);
                editor.putString("num4", n4);
                editor.putString("num5", n5);

                editor.commit();
                Toast.makeText(getApplicationContext(), "Guardado", Toast.LENGTH_LONG).show();
                finish();
            }
        });

        cont1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                startActivityForResult(intent, PICK_CONTACT1);
            }
        });

        cont2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                startActivityForResult(intent, PICK_CONTACT2);
            }
        });

        cont3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                startActivityForResult(intent, PICK_CONTACT3);
            }
        });

        cont4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                startActivityForResult(intent, PICK_CONTACT4);
            }
        });

        cont5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                startActivityForResult(intent, PICK_CONTACT5);
            }
        });

        alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSms();
            }
        });
    }

    @SuppressLint("RestrictedApi")
    public void sendSms() {
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(!enabled){
            Toast.makeText(getApplicationContext(), "Encienda su GPS", Toast.LENGTH_LONG).show();
            Intent GPSSettings = new Intent (Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(GPSSettings);
        }else{

            locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(5000);
            locationRequest.setFastestInterval(3000);
            locationRequest.setSmallestDisplacement(10);

            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    for (Location location : locationResult.getLocations()) {
                        lat = location.getLatitude();
                        lng = location.getLongitude();
                    }
                }
            };

            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(AlertActivity.this);
            if (ActivityCompat.checkSelfPermission(AlertActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AlertActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) getApplicationContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                return;
            }
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(AlertActivity.this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {

                    lat = location.getLatitude();
                    lng = location.getLongitude();

                    SharedPreferences prefe = getSharedPreferences("Login", Context.MODE_PRIVATE);
                    String contacto1num = prefe.getString("num1", "");
                    String contacto2num = prefe.getString("num2", "");
                    String contacto3num = prefe.getString("num3", "");
                    String contacto4num = prefe.getString("num4", "");
                    String contacto5num = prefe.getString("num5", "");

                    String msj = "Mensaje enviado por el usuario. Ubicacion: " +" http://maps.google.com/?q="+String.valueOf(lat)+','+String.valueOf(lng);

                    try {
                        SmsManager smsManager = SmsManager.getDefault();
                        if (contacto1num != "") {
                            smsManager.sendTextMessage(contacto1num, null, msj, null, null);
                        }
                        if (contacto2num != "") {
                            smsManager.sendTextMessage(contacto2num, null, msj, null, null);
                        }
                        if (contacto3num != "") {
                            smsManager.sendTextMessage(contacto3num, null, msj, null, null);
                        }
                        if (contacto4num != "") {
                            smsManager.sendTextMessage(contacto4num, null, msj, null, null);
                        }
                        if (contacto5num != "") {
                            smsManager.sendTextMessage(contacto5num, null, msj, null, null);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getApplicationContext(), "Mensaje Enviado", Toast.LENGTH_LONG).show();

                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("mensaje", String.valueOf(requestCode));

        if(requestCode == PICK_CONTACT1){
            if(resultCode == RESULT_OK){
                if (data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        Cursor c = null;
                        try {
                            c = getContentResolver().query(uri, new String[]{
                                            ContactsContract.CommonDataKinds.Phone.NUMBER},
                                    null, null, null);

                            if (c != null && c.moveToFirst()) {
                                String number = c.getString(0);
                                num1.setText(number);
                            }
                        } finally {
                            if (c != null) {
                                c.close();
                            }
                        }
                    }
                }
            }

        }


        if(requestCode == PICK_CONTACT2){
            if(resultCode == RESULT_OK){
                if (data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        Cursor c = null;
                        try {
                            c = getContentResolver().query(uri, new String[]{
                                            ContactsContract.CommonDataKinds.Phone.NUMBER},
                                    null, null, null);

                            if (c != null && c.moveToFirst()) {
                                String number = c.getString(0);
                                num2.setText(number);
                            }
                        } finally {
                            if (c != null) {
                                c.close();
                            }
                        }
                    }
                }
            }
        }

        if(requestCode == PICK_CONTACT3){
            if(resultCode == RESULT_OK){
                if (data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        Cursor c = null;
                        try {
                            c = getContentResolver().query(uri, new String[]{
                                            ContactsContract.CommonDataKinds.Phone.NUMBER},
                                    null, null, null);

                            if (c != null && c.moveToFirst()) {
                                String number = c.getString(0);
                                num3.setText(number);
                            }
                        } finally {
                            if (c != null) {
                                c.close();
                            }
                        }
                    }
                }
            }
        }

        if(requestCode == PICK_CONTACT4){
            if(resultCode == RESULT_OK){
                if (data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        Cursor c = null;
                        try {
                            c = getContentResolver().query(uri, new String[]{
                                            ContactsContract.CommonDataKinds.Phone.NUMBER},
                                    null, null, null);

                            if (c != null && c.moveToFirst()) {
                                String number = c.getString(0);
                                num4.setText(number);
                            }
                        } finally {
                            if (c != null) {
                                c.close();
                            }
                        }
                    }
                }
            }
        }

        if(requestCode == PICK_CONTACT5){
            if(resultCode == RESULT_OK){
                if (data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        Cursor c = null;
                        try {
                            c = getContentResolver().query(uri, new String[]{
                                            ContactsContract.CommonDataKinds.Phone.NUMBER},
                                    null, null, null);

                            if (c != null && c.moveToFirst()) {
                                String number = c.getString(0);
                                num5.setText(number);
                            }
                        } finally {
                            if (c != null) {
                                c.close();
                            }
                        }
                    }
                }
            }
        }
    }


    private void updateLoc (Location location) {
        if (location != null) {
            lat = location.getLatitude();
            lng = location.getLongitude();
        }
    }

    final LocationListener locListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            updateLoc(location);
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
        @Override
        public void onProviderEnabled(String provider) {}
        @Override
        public void onProviderDisabled(String provider) {}
    };

}
