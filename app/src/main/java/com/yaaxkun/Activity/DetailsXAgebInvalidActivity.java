package com.yaaxkun.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.widget.Toast;

import com.yaaxkun.Adapter.AgebInvalidRecyclerviewAdapter;
import com.yaaxkun.Interface.ApiClient;
import com.yaaxkun.Interface.Interface;
import com.yaaxkun.Model.IdAgebInvalid;
import com.yaaxkun.Model.ReportXAgebInvalid;
import com.yaaxkun.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsXAgebInvalidActivity extends AppCompatActivity {

    private RecyclerView myrv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailxagebinvalid);

        myrv = (RecyclerView) findViewById(R.id.recyclerview_ageb_invalid);

        SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        final String token = sharedPref.getString("token", "");

        Call<ReportXAgebInvalid> call = ApiClient.getApiClient().create(Interface.class).reportXAgebInvalid(token);
        call.enqueue(new Callback<ReportXAgebInvalid>() {
            @Override
            public void onResponse(Call<ReportXAgebInvalid> call, Response<ReportXAgebInvalid> response) {
                if (response.code() == 400) {
                    Toast.makeText(getApplicationContext(), "Error 400", Toast.LENGTH_LONG).show();
                }else if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("sucessfull")) {

                            List<IdAgebInvalid> list = response.body().getAgebs();

                            if(list.size() == 0){
                                android.app.AlertDialog.Builder builder;
                                builder = new android.app.AlertDialog.Builder(DetailsXAgebInvalidActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                                builder.setTitle("Sin registros")
                                        .setMessage("Aun no cuenta con registros, porfavor de realizar encuestas para poder observar los registros.")
                                        .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(DetailsXAgebInvalidActivity.this, MainActivity.class);
                                                startActivity(intent);
                                            }
                                        })
                                        .show();
                            }else{
                                AgebInvalidRecyclerviewAdapter myAdapter = new AgebInvalidRecyclerviewAdapter(getApplicationContext(), list);
                                myrv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                myrv.setAdapter(myAdapter);
                            }
                        }
                    } else
                        Toast.makeText(getApplicationContext(), "Error en response.body()", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<ReportXAgebInvalid> call, Throwable t) {

            }
        });
    }
}

