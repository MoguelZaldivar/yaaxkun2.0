package com.yaaxkun.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.yaaxkun.Interface.ApiClient;
import com.yaaxkun.Interface.Interface;
import com.yaaxkun.Model.Register;
import com.yaaxkun.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private String android_id;
    private JSONObject resp;
    private String imagePath;
    private TextView namePhoto;
    private File myDir;
    private File newfile;
    private EditText name, fName, sName, email, phone, password, email2, password2;
    private Spinner sex;
    private CheckBox admin;
    private boolean sexsend;

    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int CAMERA_REQUEST = 1888;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final Button button = (Button) findViewById(R.id.btnEnviar);
        //ImageView capture = (ImageView) findViewById(R.id.capture);
        name = (EditText) findViewById(R.id.edtNombre);
        fName = (EditText) findViewById(R.id.edtPrimer_apel);
        sName = (EditText) findViewById(R.id.edtSegundoApel);
        phone = (EditText) findViewById(R.id.edtPhone);
        email = (EditText) findViewById(R.id.edtEmail);
        password = (EditText) findViewById(R.id.edtPassword);
        //namePhoto = (TextView) findViewById(R.id.textPhoto);
        email2 = (EditText) findViewById(R.id.edtEmailx2);
        password2 = (EditText) findViewById(R.id.edtPasswordx2);
        sex = (Spinner) findViewById(R.id.sexspinner);
        admin = (CheckBox) findViewById(R.id.checkBox);

        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//        android_id = "abcdefghi123";

        myDir = new File(Environment.getExternalStorageDirectory() + "/yaaxkundigital");
        if (!myDir.exists()){ myDir.mkdirs(); }

        if (!myDir.exists()) {
            myDir.mkdirs();

        }

        /*capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                }
            }
        });*/

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean Validation = validate();
                if (validate()) {
                    if (email.getText().toString().toLowerCase().equals(email2.getText().toString().toLowerCase()) && password.getText().toString().equals(password2.getText().toString()) && Validation) {
                        //File file = new File(myDir, imagePath);
                        //Log.d("mensaje", String.valueOf(newfile));
                    /*RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), newfile);
                    MultipartBody.Part part = MultipartBody.Part.createFormData("upload", newfile.getName(), fileReqBody);*/

                        String NAME = name.getText().toString().toUpperCase();
                        String FNAME = fName.getText().toString().toUpperCase();
                        String SNAME = sName.getText().toString().toUpperCase();
                        String PHONE = phone.getText().toString();
                        String EMAIL = email.getText().toString().toLowerCase();
                        String PASS = password.getText().toString();
                        String SEX = sex.getSelectedItem().toString();

                        if (SEX.equals("Masculino")) {
                            sexsend = true;
                        } else if (SEX.equals("Femenino")) {
                            sexsend = false;
                        }

                        TelephonyManager tManager = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);


                        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), NAME);
                        RequestBody fName = RequestBody.create(MediaType.parse("text/plain"), FNAME);
                        RequestBody sName = RequestBody.create(MediaType.parse("text/plain"), SNAME);
                        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), EMAIL);
                        RequestBody password = RequestBody.create(MediaType.parse("text/plain"), PASS);
                        RequestBody idDevice = RequestBody.create(MediaType.parse("text/plain"), android_id);
                        RequestBody cellphone = RequestBody.create(MediaType.parse("text/plain"), PHONE);
                        RequestBody marca = RequestBody.create(MediaType.parse("text/plain"), Build.MANUFACTURER);
                        RequestBody modelo = RequestBody.create(MediaType.parse("text/plain"), Build.MODEL);
                        RequestBody operadora = RequestBody.create(MediaType.parse("text/plain"), tManager.getNetworkOperatorName());
                        RequestBody api = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(Build.VERSION.SDK_INT));
                        RequestBody sex = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sexsend));

                        if (admin.isChecked())
                            requestRegisterAdmin(name, fName, sName, email, password, idDevice, cellphone, marca, modelo, operadora, api, sex);
                        else
                            requestRegisterEnc(name, fName, sName, email, password, idDevice, cellphone, marca, modelo, operadora, api, sex);

                    } else
                        Toast.makeText(getApplicationContext(), "Las contraseñas y/o correos no son iguales", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void requestRegisterEnc(RequestBody name, RequestBody fName, RequestBody sName, RequestBody email, RequestBody password, RequestBody idDevice, RequestBody cellphone, RequestBody marca, RequestBody modelo, RequestBody operadora, RequestBody api, RequestBody sex) {

        Call<Register> call = ApiClient.getApiClient().create(Interface.class).registerEncuestador(name, fName, sName, email, password, idDevice, cellphone, marca, modelo, operadora, api, sex);
        call.enqueue(new Callback<Register>() {
            @Override
            public void onResponse(Call<Register> call, Response<Register> response) {
                if (response.code() == 400) {
                    Log.d("mensaje", "400");
                    try {
                        resp = new JSONObject(response.errorBody().string());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    AlertDialog.Builder builder;
                    builder = new AlertDialog.Builder(RegisterActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                    try {
                        builder.setTitle("Parámetro inválido")
                                .setMessage(resp.getString("message"))
                                .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 200) {
                    Log.d("mensaje", "200");
                    Log.d("mensaje", response.body().getStatus());
                    Log.d("mensaje", response.body().getMessage());
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("sucessfull")) {
                            AlertDialog.Builder builder;
                            builder = new AlertDialog.Builder(RegisterActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                            builder.setTitle("Valide su Cuenta")
                                    .setMessage(response.body().getMessage())
                                    .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                            startActivity(intent);
                                        }
                                    })
                                    .show();
                        } else if (response.body().getStatus().equals("failed")) {
                            AlertDialog.Builder builder;
                            builder = new AlertDialog.Builder(RegisterActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                            builder.setTitle("Fallo al enviar su registro")
                                    .setMessage(response.body().getMessage())
                                    .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                            startActivity(intent);
                                        }
                                    })
                                    .show();
                        }
                    } else
                        Toast.makeText(getApplicationContext(), "Error en response.body()", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Register> call, Throwable t) {
            }
        });
    }

    private void requestRegisterAdmin(RequestBody name, RequestBody fName, RequestBody sName, RequestBody email, RequestBody password, RequestBody idDevice, RequestBody cellphone, RequestBody marca, RequestBody modelo, RequestBody operadora, RequestBody api, RequestBody sex) {

        Log.d("mensaje", "en admin");
        Log.d("mensaje", String.valueOf(name));
        Log.d("mensaje", String.valueOf(fName));
        Log.d("mensaje", String.valueOf(sName));
        Log.d("mensaje", String.valueOf(email));
        Log.d("mensaje", String.valueOf(password));
        Log.d("mensaje", String.valueOf(idDevice));
        Log.d("mensaje", String.valueOf(cellphone));
        Log.d("mensaje", String.valueOf(marca));
        Log.d("mensaje", String.valueOf(modelo));
        Log.d("mensaje", String.valueOf(operadora));
        Log.d("mensaje", String.valueOf(api));
        Log.d("mensaje", String.valueOf(sex));

        Call<Register> call = ApiClient.getApiClient().create(Interface.class).registerAdmin(name, fName, sName, email, password, idDevice, cellphone, marca, modelo, operadora, api, sex);
        call.enqueue(new Callback<Register>() {
            @Override
            public void onResponse(Call<Register> call, Response<Register> response) {
                Log.d("mensaje", "response");
                if (response.code() == 400) {
                    Log.d("mensaje", "400");
                    try {
                        resp = new JSONObject(response.errorBody().string());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    AlertDialog.Builder builder;
                    builder = new AlertDialog.Builder(RegisterActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                    try {
                        builder.setTitle("Parámetro inválido")
                                .setMessage(resp.getString("message"))
                                .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 200) {
                    Log.d("mensaje", "200");
                    Log.d("mensaje", response.body().getStatus());
                    Log.d("mensaje", response.body().getMessage());
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("sucessfull")) {
                            AlertDialog.Builder builder;
                            builder = new AlertDialog.Builder(RegisterActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                            builder.setTitle("Valide su Cuenta")
                                    .setMessage(response.body().getMessage())
                                    .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                            startActivity(intent);
                                        }
                                    })
                                    .show();
                        } else if (response.body().getStatus().equals("failed")) {
                            AlertDialog.Builder builder;
                            builder = new AlertDialog.Builder(RegisterActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                            builder.setTitle("Fallo al enviar su registro")
                                    .setMessage(response.body().getMessage())
                                    .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                            startActivity(intent);
                                        }
                                    })
                                    .show();
                        }
                    } else
                        Toast.makeText(getApplicationContext(), "Error en response.body()", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Register> call, Throwable t) {
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            saveinFile(photo);
        }
    }

    private void saveinFile(Bitmap photo) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 100);
            } else {
                if (!myDir.exists()) {
                    myDir.mkdirs();
                }
                imagePath = "/ine.jpg";
                newfile = new File(myDir, imagePath);
                namePhoto.setText(imagePath);
                Log.d("mensaje", "en: "+String.valueOf(newfile));
                if (newfile.exists())
                    newfile.delete();
                try {
                    FileOutputStream out = new FileOutputStream(newfile);
                    photo.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Toast.makeText(this, "¡Se ha guardado la imagen!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean validate(){
        if (name.getText().toString().equals("")) {
            Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
            name.startAnimation(shake);
            name.requestFocus();
            Toast.makeText(getApplicationContext(), "Nombre vacio", Toast.LENGTH_SHORT).show();
            return false;
        } else if(fName.getText().toString().equals("")){
            Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
            fName.startAnimation(shake);
            fName.requestFocus();
            Toast.makeText(getApplicationContext(), "Primer apellido vacio", Toast.LENGTH_SHORT).show();
            return false;
        } else if(sName.getText().toString().equals("")){
            Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
            sName.startAnimation(shake);
            sName.requestFocus();
            Toast.makeText(getApplicationContext(), "Segundo apellido vacio", Toast.LENGTH_SHORT).show();
            return false;
        }else if(phone.getText().toString().equals("") || phone.getText().toString().length() < 10){
            Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
            phone.startAnimation(shake);
            phone.requestFocus();
            Toast.makeText(getApplicationContext(), "Número de teléfono vacio ó no valido", Toast.LENGTH_SHORT).show();
            return false;
        }else if(email.getText().toString().equals("")){
            Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
            email.startAnimation(shake);
            email.requestFocus();
            Toast.makeText(getApplicationContext(), "Email vacio", Toast.LENGTH_SHORT).show();
            return false;
        } else if(password.getText().toString().equals("")){
            Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
            password.startAnimation(shake);
            password.requestFocus();
            Toast.makeText(getApplicationContext(), "Contraseña vacia", Toast.LENGTH_SHORT).show();
            return false;
        } else if(email2.getText().toString().equals("")){
            Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
            email2.startAnimation(shake);
            email2.requestFocus();
            Toast.makeText(getApplicationContext(), "Confirmación de correo vacio", Toast.LENGTH_SHORT).show();
            return false;
        } else if(password2.getText().toString().equals("")){
            Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
            password2.startAnimation(shake);
            password2.requestFocus();
            Toast.makeText(getApplicationContext(), "Confirmación de contraseña vacio", Toast.LENGTH_SHORT).show();
            return false;
        }else return true;
    }
}