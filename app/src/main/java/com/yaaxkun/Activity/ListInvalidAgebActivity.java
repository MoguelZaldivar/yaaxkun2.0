package com.yaaxkun.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.yaaxkun.Adapter.ListInvalidAgebRecyclerviewAdapter;
import com.yaaxkun.Model.RowInvalido;
import com.yaaxkun.R;

import java.lang.reflect.Type;
import java.util.List;

public class ListInvalidAgebActivity extends AppCompatActivity {

    private List<RowInvalido> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_invalid_ageb);

        SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        String data = sharedPref.getString("ListInvalidAgeb", "");
        Gson gson = new Gson();

        Type type = new TypeToken<List<RowInvalido>>() {}.getType();
        list = gson.fromJson(data, type);

        RecyclerView myrv = (RecyclerView) findViewById(R.id.recyclerview_listInvalidAgeb);
        ListInvalidAgebRecyclerviewAdapter myAdapter = new ListInvalidAgebRecyclerviewAdapter(getApplicationContext(), list);
        myrv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        myrv.setAdapter(myAdapter);
    }
}