package com.yaaxkun.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.view.View;

import com.yaaxkun.R;

public class AdminActivity extends AppCompatActivity {

    private String [] Permissions = {Manifest.permission.INTERNET, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_CONTACTS};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        CardView reports = (CardView) findViewById (R.id.adminCard1);
        CardView details = (CardView) findViewById (R.id.adminCard2);
        CardView alert = (CardView) findViewById (R.id.adminCard3);
        CardView logout = (CardView) findViewById (R.id.adminCard4);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("token", "");
                editor.apply();
                Intent intent = new Intent(AdminActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminActivity.this, AdminDetailsActivity.class);
                startActivity(intent);
            }
        });

        reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminActivity.this, ReportActivity.class);
                startActivity(intent);
            }
        });

        alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!hasPermissions(AdminActivity.this,Permissions)){
                    ActivityCompat.requestPermissions(AdminActivity.this, Permissions, 1);
                }else {
                    Intent intent = new Intent(AdminActivity.this, AlertActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private boolean hasPermissions(AdminActivity adminActivity, String[] permissions) {
        if (getApplicationContext() != null && permissions != null){
            for (int i = 0; i < permissions.length; i++){
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), String.valueOf(permissions[i]))!= PackageManager.PERMISSION_GRANTED){
                    return false;
                }
            }
        } return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(AdminActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        builder.setTitle("¿Desea Cerrar Sesión?")
                .setMessage("Al cerrar sesión, tendra que volver iniciar sesion para ver las encuestas")
                .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(AdminActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }).show();
    }
}