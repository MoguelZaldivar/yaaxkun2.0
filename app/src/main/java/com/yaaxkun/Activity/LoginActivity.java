package com.yaaxkun.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.yaaxkun.Interface.ApiClient;
import com.yaaxkun.Interface.Interface;
import com.yaaxkun.Model.Ageb;
import com.yaaxkun.Model.Login;
import com.yaaxkun.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private String android_id, logout;
    private JSONObject resp;
    private EditText email, password;
    private CheckBox chkAdmin;
    private TextView offline, goRegister;
    private List<Ageb> agebs = new ArrayList<Ageb>();
    private Date timestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button button = (Button) findViewById(R.id.loginButton);
        email = (EditText) findViewById(R.id.correo);
        password = (EditText) findViewById(R.id.edpass);
        goRegister = (TextView) findViewById(R.id.goRegister);
        //offline = (TextView) findViewById(R.id.offline);

        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//        android_id = "522fbe3831a60e30"; //130300096@ucaribe.edu.mx
//        android_id = "209d2b82b97ffe7c";
        Log.d("mensaje", android_id);
        SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);

        final String mail = sharedPref.getString("email", "");
        final String pass = sharedPref.getString("password", "");

        email.setText(mail);

        goRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login data = new Login(
                        email.getText().toString().toLowerCase(),
                        password.getText().toString(),
                        android_id,
                        "prototipo-zCDK"
                );
                if (validate()) {
                    Call<Login> call = ApiClient.getApiClient().create(Interface.class).login(data);
                    final ProgressDialog progressDialog= ProgressDialog.show(LoginActivity.this, "", "Cargando", true);
                    call.enqueue(new Callback<Login>() {
                        @Override
                        public void onResponse(Call<Login> call, Response<Login> response) {
                            if (response.code() == 400) {
                                try {
                                    progressDialog.dismiss();
                                    resp = new JSONObject(response.errorBody() != null ? response.errorBody().string() : null);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                AlertDialog.Builder builder;
                                builder = new AlertDialog.Builder(LoginActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                                try {
                                    builder.setTitle("Usuario No Encontrado")
                                            .setMessage(resp.getString("message"))
                                            .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            })
                                            .show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else if (response.code() == 200) {
                                if (response.body() != null) {
                                    if (response.body().getStatus().equals("sucessfull")) {

                                        timestamp = Calendar.getInstance().getTime();
                                        logout = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(timestamp);

                                        SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedPref.edit();
                                        editor.putString("token", response.body().getToken());
                                        editor.putString("email", email.getText().toString());
                                        editor.putString("password", password.getText().toString());
                                        editor.putString("idDevice", android_id);
                                        editor.putString("timestamp", logout);
                                        Gson gson = new Gson();


                                        agebs = response.body().getEncuestador().getAgebs();
                                        String json = gson.toJson(agebs);
                                        editor.putString("agebs", json);
                                        editor.apply();
                                        progressDialog.dismiss();
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        Toast.makeText(getApplicationContext(), "Bienvenido", Toast.LENGTH_LONG).show();
                                    }else if (response.body().getStatus().equals("failed")) {
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Respuesta del servidor vacia", Toast.LENGTH_LONG).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Login> call, Throwable t) {
                            progressDialog.dismiss();
                            if (email.getText().toString().equals(mail) && password.getText().toString().equals(pass)) {
                                AlertDialog.Builder builder;
                                builder = new AlertDialog.Builder(LoginActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                                builder.setTitle("Usuario Fuera de Linea")
                                        .setMessage("Ingresará en modo offline, unicamente podrá realizar encuestas y estas seran guardadas de manera interna en el teléfono, para poder enviar las encuestas, será necesario cerrar sesión e ingresar cuando tenga internet y se enviaran de forma automática.")
                                        .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                startActivity(intent);
                                            }
                                        })
                                        .show();
                            }else{
                                Toast.makeText(getApplicationContext(), "Correo y/o Contraseña incorrecta", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }//else if(validate() && chkAdmin.isChecked()){
//                    Call<Login> call = ApiClient.getApiClient().create(Interface.class).loginAdmin(data);
//                    final ProgressDialog progressDialog= ProgressDialog.show(LoginActivity.this, "", "Cargando", true);
//                    call.enqueue(new Callback<Login>() {
//                        @Override
//                        public void onResponse(Call<Login> call, Response<Login> response) {
//
//                            timestamp = Calendar.getInstance().getTime();
//                            logout = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(timestamp);
//
//                            if (response.code() == 400) {
//                                try {
//                                    progressDialog.dismiss();
//                                    resp = new JSONObject(response.errorBody() != null ? response.errorBody().string() : null);
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                                AlertDialog.Builder builder;
//                                builder = new AlertDialog.Builder(LoginActivity.this, android.R.style.Theme_Material_Dialog_Alert);
//                                try {
//                                    builder.setTitle("Usuario No Encontrado")
//                                            .setMessage(resp.getString("message"))
//                                            .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int which) {
//                                                }
//                                            })
//                                            .show();
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            } else if (response.code() == 200) {
//                                if (response.body() != null) {
//                                    if (response.body().getStatus().equals("sucessfull")) {
//                                        SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
//                                        SharedPreferences.Editor editor = sharedPref.edit();
//                                        editor.putString("token", response.body().getToken());
//                                        editor.putString("email", email.getText().toString());
//                                        editor.putString("password", password.getText().toString());
//                                        editor.putString("idDevice", android_id);
//                                        editor.putString("timestamp", logout);
//                                        editor.apply();
//                                        progressDialog.dismiss();
//                                        Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
//                                        startActivity(intent);
//                                        Toast.makeText(getApplicationContext(), "Bienvenido", Toast.LENGTH_LONG).show();
//                                    }else if (response.body().getStatus().equals("failed"))
//                                        progressDialog.dismiss();
//                                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
//                                } else
//                                    progressDialog.dismiss();
//                                Toast.makeText(getApplicationContext(), "Respuesta del servidor vacia", Toast.LENGTH_LONG).show();
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<Login> call, Throwable t) {
//                            if (email.getText().toString().equals(mail) && password.getText().toString().equals(pass) && !chkAdmin.isChecked()) {
//                                AlertDialog.Builder builder;
//                                builder = new AlertDialog.Builder(LoginActivity.this, android.R.style.Theme_Material_Dialog_Alert);
//                                builder.setTitle("Usuario Fuera de Linea")
//                                        .setMessage("Ingresará en modo offline, unicamente podrá realizar encuestas y estas seran guardadas de manera interna en el teléfono, para poder enviar las encuestas, será necesario cerrar sesión e ingresar cuando tenga internet y se enviaran de forma autmática.")
//                                        .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int which) {
//                                                SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
//                                                SharedPreferences.Editor editor = sharedPref.edit();
//                                                editor.putString("timestamp", logout);
//                                                editor.apply();
//                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                                                startActivity(intent);
//                                            }
//                                        })
//                                        .show();
//                            }else{
//                                Toast.makeText(getApplicationContext(), "Correo y/o Contraseña incorrecta", Toast.LENGTH_LONG).show();
//                            }
//                        }
//                    });
//                }
            }
        });
    }

    private boolean validate () {
        if (email.getText().toString().equals("")) {
            Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
            email.startAnimation(shake);
            Toast.makeText(getApplicationContext(), "Email Vacio", Toast.LENGTH_SHORT).show();
            return false;
        } else if (password.getText().toString().equals("")) {
            Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
            password.startAnimation(shake);
            Toast.makeText(getApplicationContext(), "Contraseña Vacia", Toast.LENGTH_SHORT).show();
            return false;
        } else return true;
    }

    public boolean isOnline() {
        Log.d("mensaje", "En la funcion online() ");
        Runtime runtime = Runtime.getRuntime();
        try {
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 187.191.20.12");
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 142.93.10.110");
            //verito85
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 187.155.220.180");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        }
        catch (IOException e)          { e.printStackTrace(); Log.d("mensaje", String.valueOf(e));}
        catch (InterruptedException e) { e.printStackTrace(); Log.d("mensaje", String.valueOf(e));}

        return false;
    }

    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }
}