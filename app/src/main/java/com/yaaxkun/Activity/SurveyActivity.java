package com.yaaxkun.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.yaaxkun.Adapter.ViewSurveyAdapter;
import com.yaaxkun.Fragment.FragmentQuestion26_2;
import com.yaaxkun.Fragment.FragmentQuestion1;
import com.yaaxkun.Fragment.FragmentQuestion2;
import com.yaaxkun.Fragment.FragmentQuestion3;
import com.yaaxkun.Fragment.FragmentQuestion4;
import com.yaaxkun.Fragment.FragmentQuestion5;
import com.yaaxkun.Fragment.FragmentQuestion6;
import com.yaaxkun.Fragment.FragmentQuestion7;
import com.yaaxkun.Fragment.FragmentQuestion8;

import com.yaaxkun.Fragment.FragmentQuestion9;
import com.yaaxkun.Fragment.FragmentQuestion10;
import com.yaaxkun.Fragment.FragmentQuestion11;
import com.yaaxkun.Fragment.FragmentQuestion12;
import com.yaaxkun.Fragment.FragmentQuestion13;
import com.yaaxkun.Fragment.FragmentQuestion14;
import com.yaaxkun.Fragment.FragmentQuestion15;
import com.yaaxkun.Fragment.FragmentQuestion16;
import com.yaaxkun.Fragment.FragmentQuestion17;
import com.yaaxkun.Fragment.FragmentQuestion18;
import com.yaaxkun.Fragment.FragmentQuestion19;

import com.yaaxkun.Fragment.FragmentQuestion20;
import com.yaaxkun.Fragment.FragmentQuestion21;
import com.yaaxkun.Fragment.FragmentQuestion22;
import com.yaaxkun.Fragment.FragmentQuestion23;
import com.yaaxkun.Fragment.FragmentQuestion24;
import com.yaaxkun.Fragment.FragmentQuestion25;
import com.yaaxkun.Fragment.FragmentQuestion26;
import com.yaaxkun.Fragment.FragmentQuestion26_2;
import com.yaaxkun.Fragment.FragmentQuestion27;
import com.yaaxkun.Fragment.FragmentQuestion28;
import com.yaaxkun.Fragment.FragmentQuestion29;
import com.yaaxkun.Fragment.FragmentQuestion30;
import com.yaaxkun.Fragment.FragmentQuestion31;
import com.yaaxkun.Fragment.FragmentQuestion32;
import com.yaaxkun.Fragment.FragmentQuestion33;
import com.yaaxkun.Fragment.FragmentQuestion34;
import com.yaaxkun.Fragment.FragmentQuestion35;

import com.yaaxkun.Fragment.FragmentQuestion36;
import com.yaaxkun.Fragment.FragmentFinal;

import android.content.pm.ActivityInfo;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.yaaxkun.Model.Ageb;
import com.yaaxkun.Model.Encuestum;
import com.yaaxkun.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import com.yaaxkun.Interface.ApiClient;
import com.yaaxkun.Interface.Interface;
import com.yaaxkun.Model.SurveySend;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

public class SurveyActivity extends AppCompatActivity {
    public ViewPager viewPager;
    private List<Encuestum> encuestaItems = new ArrayList<Encuestum>();
    private List<SurveySend> surveyList = new ArrayList<SurveySend>();
    private List<Ageb> agebs = new ArrayList<Ageb>();
    private JSONObject resp;
    private double lat = 0.0;
    private double lng = 0.0;
    private Date eInicio;
    private Date efinal;
    private String inicio, finall, agebObjetivo, manzanaObjetivo;
    private SurveySend data;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//Set Portrait

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        Intent intent2 = getIntent();

        lat = intent2.getDoubleExtra("latitude", 0.0);
        lng = intent2.getDoubleExtra("longitude", 0.0);

        Log.d("mensaje", "En Survey: "+String.valueOf(lat));
        Log.d("mensaje", "En Survey: "+String.valueOf(lng));

        if(lat != 0.0 && lng != 0.0){
            SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
            final String ageb = sharedPref.getString("agebs", "");
            Log.d("mensaje", "Agebs; "+ageb);
            Gson gson = new Gson();
            if (!ageb.isEmpty()) {
                Type type = new TypeToken<List<Ageb>>() {}.getType();
                agebs = gson.fromJson(ageb, type);

                ArrayList<String> arrayAgebs = new ArrayList<String>();

                for(int i = 0; i < agebs.size(); i++){
                    arrayAgebs.add(agebs.get(i).getAgebId());
                }

                TextView TITLE = new TextView(getApplicationContext());
                TITLE.setText("Seleccion el AGEB donde se encuentra");
                TITLE.setPadding(20, 30, 20, 30);
                TITLE.setTextSize(20F);
                TITLE.setBackgroundColor(Color.DKGRAY);
                TITLE.setTextColor(Color.WHITE);

                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View findAgeb = inflater.inflate(R.layout.custom_dialog_spinner_agebs, null, false);
                final Spinner agebSpinner = (Spinner) findAgeb.findViewById(R.id.agebsSpinner);
                final EditText mzObjetivo = (EditText) findAgeb.findViewById(R.id.manzanaObjetivo);

                ArrayAdapter<String> adp = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayAgebs);
                agebSpinner.setAdapter(adp);
                agebSpinner.setVisibility(findAgeb.VISIBLE);

                new androidx.appcompat.app.AlertDialog.Builder(SurveyActivity.this)
                        .setView(findAgeb)
                        .setCustomTitle(TITLE)
                        .setPositiveButton("Seleccionar", new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {
                                agebObjetivo = agebSpinner.getSelectedItem().toString();
                                manzanaObjetivo = mzObjetivo.getText().toString();
                                eInicio = Calendar.getInstance().getTime();
                                inicio = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(eInicio);

                                ViewSurveyAdapter viewSurveyAdapter= new ViewSurveyAdapter(getSupportFragmentManager());
                                viewSurveyAdapter.AddFragment(new FragmentQuestion26_2(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion1(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion2(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion3(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion4(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion5(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion6(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion7(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion8(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion9(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion10(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion11(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion12(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion13(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion14(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion15(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion16(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion17(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion18(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion19(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentQuestion20(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion21(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion22(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion23(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion24(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion25(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion26(), "Datos Personales");
////                                viewSurveyAdapter.AddFragment(new FragmentQuestion26_2(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion27(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion28(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion29(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion30(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion31(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion32(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion33(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion34(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion35(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion36(), "Datos Personales");
//                                viewSurveyAdapter.AddFragment(new FragmentQuestion1(), "Datos Personales");
                                viewSurveyAdapter.AddFragment(new FragmentFinal(), "Datos Personales");

                                viewPager.setAdapter(viewSurveyAdapter);
                            }
                        }).show();
            }else{
                Toast.makeText(getApplicationContext(),"AGEBS aun no asignados, espere a su asignación", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
                startActivity(intent);
            }
        }else{
            Toast.makeText(getApplicationContext(),"Ubicacion no actualizada, verifique su precision en configuracion", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    public void selectFragment(int position){
//        Log.d("mensaje", "selectFragment: "+viewPager.getCurrentItem());
        viewPager.setCurrentItem(position, true);
        // true is to animate the transaction
    }

    public void crearItem (Integer id, String tipo, List<String> response){
        int len = encuestaItems.size();
        Log.d("mensaje", "Me cago en todo" + len);
        if (id-321 == len+1 ){

            if(id > 418 && id < 422){
                if (id == 421){id=id+1;}
                id = id +9;
            }else if(id > 421 ){
                id = id-2;
            }
            Log.d("mensaje", "crearItem: "+ id + "-" +tipo + "-" + response);
            Encuestum enc = new Encuestum(id, tipo, response);
//            encuestaItems.add(new Encuestum(id, tipo, respuesta));
            encuestaItems.add(enc);
            Log.d("mensaje", "PruebaError: "+ encuestaItems.toString());


//            Log.d("mensaje", "crearItem1: " + respuesta);
            //Log.d("mensaje", "crearItem1: " + encuestaItems.toString());
            //Log.d("mensaje", "crearItem1: " + encuestaItems);
        }
    }

    public void borrarItem (int id){
        int len = encuestaItems.size();
        if (id-321 == len){
            encuestaItems.remove(len-1);
        }

        Log.d("mensaje", "borrarItem: " + encuestaItems +"-"+len);
    }

    public void sendPost(){

        efinal = Calendar.getInstance().getTime();
        finall = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(efinal);

        final SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        String token = sharedPref.getString("token", "");
        String instrumentoId = "prototipo-zCDK";

        String recorded = "ONLINE";
        data = new SurveySend(
                token,
                inicio,
                finall,
                lat,
                lng,
                instrumentoId,
                recorded,
                encuestaItems,
                agebObjetivo,
                manzanaObjetivo
        );

        if(token.equals("")){
            Toast.makeText(getApplicationContext(), "Error al enviar la encuesta, se almaceno de manera interna", Toast.LENGTH_LONG).show();
            Gson gson = new Gson();
            data.setRecorded("OFFLINE");
            String dataString = sharedPref.getString("EncuestaPrueba", "");
            if (!dataString.isEmpty()) {
                Type type = new TypeToken<List<SurveySend>>() {}.getType();
                List<SurveySend> surveyList2 = gson.fromJson(dataString, type);
                surveyList2.add(data);
                String json = gson.toJson(surveyList2);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("EncuestaPrueba", json);
                editor.apply();
            }else {
                surveyList.add(data);
                String json = gson.toJson(surveyList);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("EncuestaPrueba", json);
                editor.apply();
            }
            Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
            startActivity(intent);
        }else {
            final ProgressDialog progressDialog= ProgressDialog.show(SurveyActivity.this, "", "Cargando", true);
            Call<SurveySend> call = ApiClient.getApiClient().create(Interface.class).surveySend(data, token);
            call.enqueue(new Callback<SurveySend>() {
                @Override
                public void onResponse(Call<SurveySend> call, Response<SurveySend> response) {
                    Log.d("mensaje", "Respuesta de Envio: " + response.code());
                    if (response.code() == 400) {
                        try {
                            resp = new JSONObject(response.errorBody().string());
                            Toast.makeText(getApplicationContext(), "Error al enviar la encuesta, se almaceno de manera interna", Toast.LENGTH_LONG).show();
                            Gson gson = new Gson();
                            data.setRecorded("OFFLINE");
                            String dataString = sharedPref.getString("EncuestaPrueba", "");
                            if (!dataString.isEmpty()) {
                                Type type = new TypeToken<List<SurveySend>>() {}.getType();
                                List<SurveySend> surveyList2 = gson.fromJson(dataString, type);
                                surveyList2.add(data);
                                String json = gson.toJson(surveyList2);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putString("EncuestaPrueba", json);
                                editor.apply();
                            }else {
                                surveyList.add(data);
                                String json = gson.toJson(surveyList);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putString("EncuestaPrueba", json);
                                editor.apply();
                            }
                            Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }else if (response.code() == 200) {
                        if (response.body() != null) {
                            Log.d("mensaje", "Status" + response.body().getStatus());
                            if (response.body().getStatus().equals("Successful")) {
                                Toast.makeText(getApplicationContext(), "Encuesta Enviada", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
                                startActivity(intent);
                            }
                        } else{
                            Toast.makeText(getApplicationContext(), "Respuesta del servidor vacia", Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<SurveySend> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "No se encontró conexion, se almaceno de manera interna la encuesta", Toast.LENGTH_LONG).show();
                    Gson gson = new Gson();
                    data.setRecorded("OFFLINE");
                    String dataString = sharedPref.getString("EncuestaPrueba", "");
                    if (!dataString.isEmpty()) {
                        Type type = new TypeToken<List<SurveySend>>() {}.getType();
                        List<SurveySend> surveyList2 = gson.fromJson(dataString, type);
                        surveyList2.add(data);
                        String json = gson.toJson(surveyList2);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("EncuestaPrueba", json);
                        editor.apply();
                    }else {
                        surveyList.add(data);
                        String json = gson.toJson(surveyList);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("EncuestaPrueba", json);
                        editor.apply();
                    }
                    Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
                    startActivity(intent);
                    progressDialog.dismiss();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_survey, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.cancel:
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(SurveyActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                builder.setTitle("¿Desea Cancelar la Encuesta?")
                        .setMessage("Al cancelar la encuesta, esta no se enviará y será eliminada, seleccione el boton de cancelar en caso de estar de acuerdo, en caso contrario seleccione fuere este mensaje")
                        .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                encuestaItems.clear();
                                Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
                                startActivity(intent);
                            }
                        })
                        .show();
                return true;
            case R.id.sos:
                sendSms();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(SurveyActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        builder.setTitle("¿Desea Cancelar la Encuesta?")
                .setMessage("Al cancelar la encuesta, esta no se enviará y será eliminada, seleccione el boton de cancelar en caso de estar de acuerdo, en caso contrario seleccione fuere este mensaje")
                .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        encuestaItems.clear();
                        Intent intent = new Intent(SurveyActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                })
                .show();
    }

    @SuppressLint("RestrictedApi")
    public void sendSms() {
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(!enabled){
            Toast.makeText(getApplicationContext(), "Encienda su GPS", Toast.LENGTH_LONG).show();
            Intent GPSSettings = new Intent (Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(GPSSettings);
        }else{

            locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(5000);
            locationRequest.setFastestInterval(3000);
            locationRequest.setSmallestDisplacement(10);

            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    for (Location location : locationResult.getLocations()) {
                        lat = location.getLatitude();
                        lng = location.getLongitude();
                    }
                }
            };

            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(SurveyActivity.this);
            if (ActivityCompat.checkSelfPermission(SurveyActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SurveyActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) getApplicationContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                return;
            }
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(SurveyActivity.this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    lat = location.getLatitude();
                    lng = location.getLongitude();
                    SharedPreferences prefe = getSharedPreferences("Login", Context.MODE_PRIVATE);
                    String contacto1num = prefe.getString("num1", "");
                    String contacto2num = prefe.getString("num2", "");
                    String contacto3num = prefe.getString("num3", "");
                    String contacto4num = prefe.getString("num4", "");
                    String contacto5num = prefe.getString("num5", "");

                    String msj = "Mensaje enviado por el usuario. Ubicacion: " +" http://maps.google.com/?q="+String.valueOf(lat)+','+String.valueOf(lng);

                    try {
                        SmsManager smsManager = SmsManager.getDefault();
                        if (contacto1num != "") {
                            smsManager.sendTextMessage(contacto1num, null, msj, null, null);
                        }
                        if (contacto2num != "") {
                            smsManager.sendTextMessage(contacto2num, null, msj, null, null);
                        }
                        if (contacto3num != "") {
                            smsManager.sendTextMessage(contacto2num, null, msj, null, null);
                        }
                        if (contacto4num != "") {
                            smsManager.sendTextMessage(contacto2num, null, msj, null, null);
                        }
                        if (contacto5num != "") {
                            smsManager.sendTextMessage(contacto2num, null, msj, null, null);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getApplicationContext(), "Mensaje Enviado", Toast.LENGTH_LONG).show();

                }
            });
        }
    }

    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 187.191.20.12");
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 142.93.10.110");
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 187.155.220.180");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        }
        catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }

//    public void hideKeyboard() {
//        // Check if no view has focus:
//        View view = this.getCurrentFocus();
//        Log.d("mensaje","Teclado: paso");
//        if (view != null) {
//            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
//            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//            Log.d("mensaje","Teclado: no paso");
//        }
//    }
    public static void hideKeyboard(@NonNull View v) {
        InputMethodManager inputManager = (InputMethodManager) v.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
