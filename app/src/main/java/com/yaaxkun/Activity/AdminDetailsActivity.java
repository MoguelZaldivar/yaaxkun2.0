package com.yaaxkun.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.yaaxkun.Adapter.AdminAgebRecyclerviewAdapter;
import com.yaaxkun.Interface.ApiClient;
import com.yaaxkun.Interface.Interface;
import com.yaaxkun.Model.AdminReport;
import com.yaaxkun.Model.Estadistica;
import com.yaaxkun.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminDetailsActivity extends AppCompatActivity {

    private RecyclerView myrv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailxageb);

        myrv = (RecyclerView) findViewById(R.id.recyclerview_ageb);

        SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        final String token = sharedPref.getString("token", "");

        Call<AdminReport> call = ApiClient.getApiClient().create(Interface.class).AdminReport(token);
        call.enqueue(new Callback<AdminReport>() {
            @Override
            public void onResponse(Call<AdminReport> call, Response<AdminReport> response) {
                Log.d("mensaje", String.valueOf(response.code()));
                if (response.code() == 400) {
                    Toast.makeText(getApplicationContext(), "Error 400", Toast.LENGTH_LONG).show();
                }else if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("Sucessfull")) {
                            List<Estadistica> list = response.body().getReporte().getEstadisticas();

                            if(list.size() == 0){
                                android.app.AlertDialog.Builder builder;
                                builder = new android.app.AlertDialog.Builder(AdminDetailsActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                                builder.setTitle("Sin registros")
                                        .setMessage("Aun no cuenta con registros, porfavor de realizar encuestas para poder observar los registros.")
                                        .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(AdminDetailsActivity.this, MainActivity.class);
                                                startActivity(intent);
                                            }
                                        })
                                        .show();
                            }else{
                                AdminAgebRecyclerviewAdapter myAdapter = new AdminAgebRecyclerviewAdapter(getApplicationContext(), list);
                                myrv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                myrv.setAdapter(myAdapter);
                            }
                        }
                    } else
                        Toast.makeText(getApplicationContext(), "Error en response.body()", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<AdminReport> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error al establecer conexion", Toast.LENGTH_LONG).show();
            }
        });
    }
}
