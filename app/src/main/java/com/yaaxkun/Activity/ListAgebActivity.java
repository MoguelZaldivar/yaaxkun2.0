package com.yaaxkun.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.yaaxkun.Adapter.ListAgebRecyclerviewAdapter;
import com.yaaxkun.Model.Row;
import com.yaaxkun.R;

import java.lang.reflect.Type;
import java.util.List;

public class ListAgebActivity extends AppCompatActivity {

    private List<Row> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_ageb);

        SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        String data = sharedPref.getString("ListAgeb", "");
        Gson gson = new Gson();

        Type type = new TypeToken<List<Row>>() {}.getType();
        list = gson.fromJson(data, type);

        RecyclerView myrv = (RecyclerView) findViewById(R.id.recyclerview_listAgeb);
        ListAgebRecyclerviewAdapter myAdapter = new ListAgebRecyclerviewAdapter(getApplicationContext(), list);
        myrv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        myrv.setAdapter(myAdapter);
    }
}
