package com.yaaxkun.Activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.yaaxkun.R;

public class ReportActivity extends AppCompatActivity {
    private Button reportButton1, reportButton2, reportButton3, reportButton4, reportButton5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        reportButton1 = (Button) findViewById(R.id.report_button1);
        reportButton2 = (Button) findViewById(R.id.report_button2);
        reportButton3 = (Button) findViewById(R.id.report_button3);
        reportButton4 = (Button) findViewById(R.id.report_button4);
        reportButton5 = (Button) findViewById(R.id.report_button5);

        SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        final String token = sharedPref.getString("token", "");

        reportButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://ceiqroodev.ddns.net:3000/api/reporte/estadisticas/instrumento2/pdf"));
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://devcei.ddns.net:3000/api/reporte/estadisticas/prototipo-zCDK/pdf"));

                Bundle bundle = new Bundle();
                bundle.putString("token", token);
                browserIntent.putExtra(Browser.EXTRA_HEADERS, bundle);
                startActivity(browserIntent);
            }
        });

        reportButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://ceiqroodev.ddns.net:3000/api/reporte/estadisticas-agebs/instrumento2/pdf"));
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://devcei.ddns.net:3000/api/reporte/estadisticas-agebs/prototipo-zCDK/pdf"));
                Bundle bundle = new Bundle();
                bundle.putString("token", token);
                browserIntent.putExtra(Browser.EXTRA_HEADERS, bundle);
                startActivity(browserIntent);
            }
        });

        reportButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView TITLE = new TextView(getApplicationContext());
                TITLE.setText("Ingrese el ID de la Encuesta");
                TITLE.setPadding(20, 30, 20, 30);
                TITLE.setTextSize(20F);
                TITLE.setBackgroundColor(Color.DKGRAY);
                TITLE.setTextColor(Color.WHITE);

                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View findId = inflater.inflate(R.layout.custom_dialog_id, null, false);
                final EditText idFind = (EditText) findId.findViewById(R.id.findId);

                new AlertDialog.Builder(ReportActivity.this)
                        .setView(findId)
                        .setCustomTitle(TITLE)
                        .setPositiveButton("Buscar", new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {
//                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://ceiqroodev.ddns.net:3000/api/reporte/encuesta/"+idFind.getText().toString()+"/pdf"));
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://devcei.ddns.net:3000/api/reporte/encuesta/"+idFind.getText().toString()+"/pdf"));

                                Bundle bundle = new Bundle();
                                bundle.putString("token", token);
                                browserIntent.putExtra(Browser.EXTRA_HEADERS, bundle);
                                startActivity(browserIntent);
                            }
                        }).show();

            }
        });

        reportButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView TITLE = new TextView(getApplicationContext());
                TITLE.setText("Ingrese el ID del AGEB");
                TITLE.setPadding(20, 30, 20, 30);
                TITLE.setTextSize(20F);
                TITLE.setBackgroundColor(Color.DKGRAY);
                TITLE.setTextColor(Color.WHITE);

                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View findId = inflater.inflate(R.layout.custom_dialog_id, null, false);
                final EditText idFind = (EditText) findId.findViewById(R.id.findId);

                new AlertDialog.Builder(ReportActivity.this)
                        .setView(findId)
                        .setCustomTitle(TITLE)
                        .setPositiveButton("Buscar", new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {
//                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://ceiqroodev.ddns.net:3000/api/reporte/ageb/"+idFind.getText().toString()+"/instrumento2/pdf"));
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://devcei.ddns.net:3000/api/reporte/ageb/"+idFind.getText().toString()+"/prototipo-zCDK/pdf"));
                                Bundle bundle = new Bundle();
                                bundle.putString("token", token);
                                browserIntent.putExtra(Browser.EXTRA_HEADERS, bundle);
                                startActivity(browserIntent);
                            }
                        }).show();
            }
        });

        reportButton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView TITLE = new TextView(getApplicationContext());
                TITLE.setText("Ingrese el ID del Encuestador");
                TITLE.setPadding(20, 30, 20, 30);
                TITLE.setTextSize(20F);
                TITLE.setBackgroundColor(Color.DKGRAY);
                TITLE.setTextColor(Color.WHITE);

                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View findId = inflater.inflate(R.layout.custom_dialog_id, null, false);
                final EditText idFind = (EditText) findId.findViewById(R.id.findId);

                new AlertDialog.Builder(ReportActivity.this)
                        .setView(findId)
                        .setCustomTitle(TITLE)
                        .setPositiveButton("Buscar", new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {
//                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://ceiqroodev.ddns.net:3000/api/reporte/estadisticas/instrumento2/pdf?limitByEncuestador="+idFind.getText().toString()));
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://devcei.ddns.net:3000/api/reporte/estadisticas/prototipo-zCDK/pdf?limitByEncuestador="+idFind.getText().toString()));
                                Bundle bundle = new Bundle();
                                bundle.putString("token", token);
                                browserIntent.putExtra(Browser.EXTRA_HEADERS, bundle);
                                startActivity(browserIntent);
                            }
                        }).show();
            }
        });
    }
}
