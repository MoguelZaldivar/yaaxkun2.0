package com.yaaxkun.Activity;

import android.app.AlertDialog;
import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.yaaxkun.Interface.ApiClient;
import com.yaaxkun.Interface.Interface;
import com.yaaxkun.Model.SurveySend;
import com.yaaxkun.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class BackgroundService extends IntentService {

    public BackgroundService() {
        super("BackgroundService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        List<SurveySend> surveyList;
        JSONObject resp;
        SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        String dataString = sharedPref.getString("EncuestaPrueba", "");
        String token= sharedPref.getString("token", "");
        Gson gson = new Gson();
        if (!dataString.isEmpty()) {
            Type type = new TypeToken<List<SurveySend>>() {}.getType();
            surveyList = gson.fromJson(dataString, type);
            if(true){
                for (int i = 0; i < surveyList.size(); i++) {
                    if(surveyList.get(i).getEnviado().equals("false")){
                        surveyList.get(i).setToken(token);
                        Call<SurveySend> call = ApiClient.getApiClient().create(Interface.class).surveySend(surveyList.get(i), token);
                        try {
                            Response<SurveySend> execute = call.execute();
                            if(execute.code() == 200){
                                Log.d("mensaje", "Se envio y cambio: "+i);
                                surveyList.get(i).setEnviado("true");
                                Toast.makeText(getApplicationContext(), "Encuesta "+i+" enviada", Toast.LENGTH_LONG).show();
                            }else if (execute.code() == 400) {
                                Toast.makeText(getApplicationContext(), "Error al enviar encuesta, se mantendra guardada la encuesta", Toast.LENGTH_LONG).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.d("mensaje", String.valueOf(e));
                        }
                    }
                }
                String json = gson.toJson(surveyList);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("EncuestaPrueba", json);
                editor.apply();
            }
        }
    }

    private boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 187.191.20.12");
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 142.93.10.110");
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 187.155.220.180");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        }
        catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }
}
