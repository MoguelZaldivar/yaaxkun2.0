package com.yaaxkun.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.yaaxkun.Adapter.ListAdminAgebRecyclerviewAdapter;
import com.yaaxkun.Interface.ApiClient;
import com.yaaxkun.Interface.Interface;
import com.yaaxkun.Model.AdminReportXAgeb;
import com.yaaxkun.Model.Encuesta;
import com.yaaxkun.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListAdminAgebActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_ageb);

        Intent intent = getIntent();
        final String agebId = intent.getStringExtra("ageb");
        Log.d("mensaje", agebId);

        SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        final String token = sharedPref.getString("token", "");

        Call<AdminReportXAgeb> call = ApiClient.getApiClient().create(Interface.class).AdminReportXAgeb(token, agebId);
        call.enqueue(new Callback<AdminReportXAgeb>() {
            @Override
            public void onResponse(Call<AdminReportXAgeb> call, Response<AdminReportXAgeb> response) {
                Log.d("mensaje", String.valueOf(response.code()));
                if (response.code() == 400) {
                    Toast.makeText(getApplicationContext(), "Error 400", Toast.LENGTH_LONG).show();
                }else if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("sucessfull")) {
                            String motivo = response.body().getReporteXAgeb().getMotivos().get(0).getNombre();
                            List<Encuesta> list = response.body().getReporteXAgeb().getEncuestas();

                            if(list.size() == 0){
                                android.app.AlertDialog.Builder builder;
                                builder = new android.app.AlertDialog.Builder(ListAdminAgebActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                                builder.setTitle("Sin registros")
                                        .setMessage("Aun no cuenta con registros, porfavor de realizar encuestas para poder observar los registros.")
                                        .setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(ListAdminAgebActivity.this, MainActivity.class);
                                                startActivity(intent);
                                            }
                                        })
                                        .show();
                            }else{
                                RecyclerView myrv = (RecyclerView) findViewById(R.id.recyclerview_listAgeb);
                                ListAdminAgebRecyclerviewAdapter myAdapter = new ListAdminAgebRecyclerviewAdapter(getApplicationContext(), agebId, motivo, list);
                                myrv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                myrv.setAdapter(myAdapter);
                            }
                        }
                    } else
                        Toast.makeText(getApplicationContext(), "Error en response.body()", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<AdminReportXAgeb> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error al establecer conexion", Toast.LENGTH_LONG).show();
                Log.d("mensaje", String.valueOf(t));
            }
        });
    }
}
