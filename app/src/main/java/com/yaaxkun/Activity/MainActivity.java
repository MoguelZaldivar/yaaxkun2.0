package com.yaaxkun.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.provider.Settings;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.yaaxkun.Model.SurveySend;
import com.yaaxkun.R;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {


    private String[] Permissions = {Manifest.permission.INTERNET, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_CONTACTS, Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA};
    private List<SurveySend> surveyList;
    private boolean permission2 = false;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private double lat = 0.0;
    private double lng = 0.0;
    public int i = 0;
    private Date currenttimestamp;
    private Date pasttimestamp;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CardView survey = (CardView) findViewById(R.id.cardView1);
        CardView details = (CardView) findViewById(R.id.cardView2);
        CardView alert = (CardView) findViewById(R.id.cardView3);
        CardView logout = (CardView) findViewById(R.id.cardView4);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("token", "");
                editor.apply();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        survey.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (!hasPermissions(MainActivity.this, Permissions)) {
                    ActivityCompat.requestPermissions(MainActivity.this, Permissions, 1);
                } else {

                    LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
                    boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

                    if (!enabled) {
                        Toast.makeText(getApplicationContext(), "Encienda su GPS", Toast.LENGTH_LONG).show();
                        Intent GPSSettings = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(GPSSettings);
                    } else {
                        locationRequest = new LocationRequest();
                        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                        locationRequest.setInterval(5000);
                        locationRequest.setFastestInterval(3000);
                        locationRequest.setSmallestDisplacement(10);

                        locationCallback = new LocationCallback() {
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                for (Location location : locationResult.getLocations()) {
                                    lat = location.getLatitude();
                                    lng = location.getLongitude();
                                    /*Intent intent = new Intent(MainActivity.this, SurveyActivity.class);
                                    intent.putExtra("latitude", lat);
                                    intent.putExtra("longitude", lng);
                                    startActivity(intent);*/
                                }
                            }
                        };

                        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);
                        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) getApplicationContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                            return;
                        }
                        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
                        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(MainActivity.this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                lat = location.getLatitude();
                                lng = location.getLongitude();
                                Intent intent = new Intent(MainActivity.this, SurveyActivity.class);
                                intent.putExtra("latitude", lat);
                                intent.putExtra("longitude", lng);
                                Log.d("mensaje", String.valueOf(lat));
                                Log.d("mensaje", String.valueOf(lng));
                                startActivity(intent);
                            }
                        });

                    }
                }
            }
        });

        alert.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (!hasPermissions(MainActivity.this,Permissions)){
                    ActivityCompat.requestPermissions(MainActivity.this, Permissions, 1);
                }else {
                    Intent intent = new Intent(MainActivity.this, AlertActivity.class);
                    startActivity(intent);
                }
            }
        });

        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                startActivity(intent);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean hasPermissions(MainActivity mainActivity, String[] permissions) {

        SharedPreferences sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        permission2 = sharedPref.getBoolean("Permission", false);
        SharedPreferences.Editor editor = sharedPref.edit();

        if (getApplicationContext() != null && permissions != null){
            for (int j = 0; j < permissions.length; j++){
                String permission = permissions[j];
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), String.valueOf(permissions[j]))!= PackageManager.PERMISSION_GRANTED){
                    boolean showRationale = shouldShowRequestPermissionRationale( permission );
                    if (!showRationale && permission2) {
                        Toast.makeText(getApplicationContext(), "Conceda permisos manualmente para usar la aplicación", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                    editor.putBoolean("Permission",true);
                    editor.commit();
                    return false;
                }
            }
        } return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        String dataString = sharedPref.getString("EncuestaPrueba", "");
        String timestamp = sharedPref.getString("timestamp", "");
        currenttimestamp = Calendar.getInstance().getTime();
//        sharedPref.edit().clear().apply();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            pasttimestamp = format.parse(timestamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(currenttimestamp.getTime() - pasttimestamp.getTime() > 18000000){
            Toast.makeText(getApplicationContext(), "Inicie sesión de nuevo", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }else{
            Gson gson = new Gson();
            if (!dataString.isEmpty()) {
            //if (!dataString.isEmpty() && isOnline()) {
                Type type = new TypeToken<List<SurveySend>>() {}.getType();
                surveyList = gson.fromJson(dataString, type);
                for (i = 0; i < surveyList.size(); i++) {
                    Log.d("mensaje", String.valueOf(i));
                    Log.d("mensaje", surveyList.get(i).getEnviado());
                    if(surveyList.get(i).getEnviado().equals("false")){
                        Log.d("mensaje", "En if: " + i);
                        Intent intent = new Intent(MainActivity.this, BackgroundService.class);
                        startService(intent);
                        break;
                    }
                }
            }
        }
    }

    private boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 187.191.20.12");
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 142.93.10.110");
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 187.155.220.180");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        }
        catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        builder.setTitle("¿Desea Cerrar Sesión?")
                .setMessage("Al cerrar sesión, tendra que volver iniciar sesion para realizar una encuesta")
                .setPositiveButton("Cerrar Sesión", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                })
                .show();
    }
}
