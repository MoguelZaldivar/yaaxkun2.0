package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IdAgeb {

    @SerializedName("rows")
    @Expose
    private List<Row> rows;

    @SerializedName("rowCount")
    @Expose
    private Integer rowCount;

    @SerializedName("agebId")
    @Expose
    private String agebId;

    @SerializedName("objetivo")
    @Expose
    private Integer objetivo;

    public List<Row> getRows() { return rows; }
    public Integer getRowCount() { return rowCount; }
    public String getAgebId() { return agebId; }
    public Integer getObjetivo() { return objetivo; }
}
