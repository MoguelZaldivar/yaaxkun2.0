package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class ReportXEncuestados {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("validas")
    @Expose
    private Validas validas;
    @SerializedName("invalidas")
    @Expose
    private Invalidas invalidas;

    public String getStatus() { return status; }
    public Validas getValidas() { return validas; }
    public Invalidas getInvalidas() { return invalidas; }
}
