package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Encuestador {

    @SerializedName("agebs")
    @Expose
    private List<Ageb> agebs;

    public List<Ageb> getAgebs() { return agebs; }
}
