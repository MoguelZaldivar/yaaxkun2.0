package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdminReportXAgeb {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("reporte")
    @Expose
    private ReporteXAgeb reporteXAgeb;

    public String getStatus() { return status; }
    public ReporteXAgeb getReporteXAgeb() { return reporteXAgeb; }

}