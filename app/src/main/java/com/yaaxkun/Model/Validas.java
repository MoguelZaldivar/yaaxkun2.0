package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Validas {

    @SerializedName("rowCount")
    @Expose
    private Integer rowCount;

    public Integer getRowCount() { return rowCount; }
}