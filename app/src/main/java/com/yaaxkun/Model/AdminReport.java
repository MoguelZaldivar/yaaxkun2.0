package com.yaaxkun.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdminReport {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("reporte")
    @Expose
    private Reporte reporte;

    public String getStatus() { return status; }
    public Reporte getReporte() { return reporte; }
}