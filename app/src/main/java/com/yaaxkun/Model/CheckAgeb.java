package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckAgeb {
    @SerializedName("results")
    @Expose
    private String results;
    @SerializedName("country")
    @Expose
    private String country;

    private double lat;
    private double lon;

    public CheckAgeb(double lat, double lon){
        this.lat = lat;
        this.lon = lon;
    }

    public String getResults() { return results; }
    public String getCountry() { return country; }

    public void setResults(String results) { this.results = results; }
    public void setCountry(String country) { this.country = country; }
}
