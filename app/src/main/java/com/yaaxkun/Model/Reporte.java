package com.yaaxkun.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reporte {

    @SerializedName("estadisticas")
    @Expose
    private List<Estadistica> estadisticas = null;

    public List<Estadistica> getEstadisticas() { return estadisticas; }

}
