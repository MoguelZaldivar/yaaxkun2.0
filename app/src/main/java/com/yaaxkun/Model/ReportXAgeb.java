package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReportXAgeb {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("agebs")
    @Expose
    private List<IdAgeb> agebs;

    public String getStatus() { return status; }
    public List<IdAgeb> getAgebs() { return agebs; }

}
