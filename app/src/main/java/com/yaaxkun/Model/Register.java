package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import okhttp3.MultipartBody;

public class Register {
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    private String name;
    private String fName;
    private String sName;
    private String email;
    private String password;
    private String idDevice;
    private String cellphone;
    private String trade;
    private String model;
    private String oper;
    private String apiDevice;
    private boolean sex;
    //private MultipartBody.Part foto;

    public Register(String name, String fName, String sName, String email, String password, String idDevice, String cellphone, String trade, String model, String oper, String apiDevice, boolean sex) {
        this.name = name;
        this.fName = fName;
        this.sName = sName;
        this.email = email;
        this.password = password;
        this.idDevice = idDevice;
        this.cellphone = cellphone;
        this.trade = trade;
        this.model = model;
        this.oper = oper;
        this.apiDevice = apiDevice;
        this.sex = sex;
    }

    public String getStatus() { return status; }
    public String getMessage() { return message; }
}
