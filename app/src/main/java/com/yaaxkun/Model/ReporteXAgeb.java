package com.yaaxkun.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReporteXAgeb {

    @SerializedName("ageb")
    @Expose
    private String ageb;
    @SerializedName("motivos")
    @Expose
    private List<Motivo> motivos = null;
    @SerializedName("encuestas")
    @Expose
    private List<Encuesta> encuestas = null;

    public String getAgeb() { return ageb; }
    public List<Motivo> getMotivos() { return motivos; }
    public List<Encuesta> getEncuestas() { return encuestas; }

}