package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ageb {

    @SerializedName("ageb_id")
    @Expose
    private String agebId;

    public String getAgebId() { return agebId; }
}
