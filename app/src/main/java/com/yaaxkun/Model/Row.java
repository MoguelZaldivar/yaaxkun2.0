package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Row {
    @SerializedName("encuesta_id")
    @Expose
    private Integer encuestaId;
    @SerializedName("encuesta_inicio")
    @Expose
    private String encuestaInicio;
    @SerializedName("encuesta_fin")
    @Expose
    private String encuestaFin;
    @SerializedName("encuestador_id")
    @Expose
    private Integer encuestadorId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("encuesta_latitude")
    @Expose
    private Double encuestaLatitude;
    @SerializedName("encuesta_longitude")
    @Expose
    private Double encuestaLongitude;
    @SerializedName("instrumento_id")
    @Expose
    private String instrumentoId;
    @SerializedName("ageb_id")
    @Expose
    private String agebId;
    @SerializedName("grabado")
    @Expose
    private String grabado;

    public Integer getEncuestaId() { return encuestaId; }
    public String getEncuestaInicio() { return encuestaInicio; }
    public String getEncuestaFin() { return encuestaFin; }
    public Integer getEncuestadorId() { return encuestadorId; }
    public String getStatus() { return status; }
    public Double getEncuestaLatitude() { return encuestaLatitude; }
    public Double getEncuestaLongitude() { return encuestaLongitude; }
    public String getInstrumentoId() { return instrumentoId; }
    public String getAgebId() { return agebId; }
    public String getGrabado() { return grabado; }
}
