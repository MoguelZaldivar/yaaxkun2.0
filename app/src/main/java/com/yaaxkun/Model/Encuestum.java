package com.yaaxkun.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Encuestum {
    @SerializedName("itemID")
    @Expose
    private Integer itemID;
    @SerializedName("tipo")
    @Expose
    private String tipo;
    @SerializedName("respuesta")
    @Expose
    private List<String> respuesta = null;

    public Encuestum(Integer itemID, String tipo, List<String> respuesta) {
        this.itemID = itemID;
        this.tipo = tipo;
        this.respuesta = respuesta;
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<String> getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(List<String> respuesta) {
        this.respuesta = respuesta;
    }

    public String toString() {
//        Log.d("mensaje", getClass().getName() + "@" + Integer.toHexString(hashCode()))
        return this.getTipo() + " - " + this.getRespuesta() + " - "+String.valueOf(this.getItemID());
    }
}
