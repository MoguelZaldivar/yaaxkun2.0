package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {
    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("user")
    @Expose
    private String user;

    @SerializedName("encuestador")
    @Expose
    private Encuestador encuestador;

    private String email;
    private String password;
    private String idDevice;
    private String instrumentoId;

    public Login(String email, String password, String idDevice, String instrumentoId) {
        this.email = email;
        this.password = password;
        this.idDevice = idDevice;
        this.instrumentoId= instrumentoId;
    }

    public String getToken() { return token; }
    public String getStatus() { return status; }
    public String getUser() { return user; }
    public String getMessage() { return message; }
    public Encuestador getEncuestador() { return encuestador; }
}
