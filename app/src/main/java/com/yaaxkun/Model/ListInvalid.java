package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListInvalid {

    @SerializedName("invalidas")
    @Expose
    private List<Invalida> invalidas = null;

    public List<Invalida> getInvalidas() { return invalidas; }
}
