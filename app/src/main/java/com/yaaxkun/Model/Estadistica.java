package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Estadistica {

    @SerializedName("ageb_id")
    @Expose
    private String agebId;


    @SerializedName("total")
    @Expose
    private String total;


    public String getAgebId() { return agebId; }
    public String getTotal() { return total; }
}
