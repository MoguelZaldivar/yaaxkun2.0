package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Motivo {

    @SerializedName("motivo_id")
    @Expose
    private Integer motivoId;
    @SerializedName("nombre")
    @Expose
    private String nombre;

    public Integer getMotivoId() { return motivoId; }
    public String getNombre() { return nombre; }

}