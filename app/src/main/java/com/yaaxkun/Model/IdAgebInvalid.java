package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IdAgebInvalid {

    @SerializedName("rows")
    @Expose
    private List<RowInvalido> rows;

    @SerializedName("rowCount")
    @Expose
    private int rowCount;

    @SerializedName("agebId")
    @Expose
    private String agebId;

    public List<RowInvalido> getRows() { return rows; }
    public int getRowCount() { return rowCount; }
    public String getAgebId() { return agebId; }
}
