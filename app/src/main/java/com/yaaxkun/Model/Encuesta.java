package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Encuesta {

    @SerializedName("encuesta_id")
    @Expose
    private Integer encuestaId;
    @SerializedName("encuesta_inicio")
    @Expose
    private String encuestaInicio;
    @SerializedName("encuesta_fin")
    @Expose
    private String encuestaFin;
    @SerializedName("encuesta_latitude")
    @Expose
    private Double encuestaLatitude;
    @SerializedName("encuesta_longitude")
    @Expose
    private Double encuestaLongitude;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("encuestador_id")
    @Expose
    private Integer encuestadorId;
    @SerializedName("encuestador")
    @Expose
    private String encuestador;
    @SerializedName("grabado")
    @Expose
    private String grabado;

    public Integer getEncuestaId() { return encuestaId; }
    public String getEncuestaInicio() { return encuestaInicio; }
    public String getEncuestaFin() { return encuestaFin; }
    public Double getEncuestaLatitude() { return encuestaLatitude;}
    public Double getEncuestaLongitude() { return encuestaLongitude; }
    public String getStatus() { return status; }
    public Integer getEncuestadorId() { return encuestadorId; }
    public String getEncuestador() { return encuestador; }
    public String getGrabado() { return grabado; }
}