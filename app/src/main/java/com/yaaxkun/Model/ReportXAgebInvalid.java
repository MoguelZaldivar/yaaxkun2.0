package com.yaaxkun.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class ReportXAgebInvalid {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("agebs")
    @Expose
    private List<IdAgebInvalid> agebs;

    public String getStatus() { return status; }
    public List<IdAgebInvalid> getAgebs() { return agebs; }
}
