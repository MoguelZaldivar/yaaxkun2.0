package com.yaaxkun.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SurveySend {
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private  String message;

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("eInicio")
    @Expose
    private String eInicio;
    @SerializedName("eFinal")
    @Expose
    private String eFinal;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("instrumentoId")
    @Expose
    private String instrumentoId;
    @SerializedName("recorded")
    @Expose
    private String recorded;
    @SerializedName("encuesta")
    @Expose
    private List<Encuestum> encuesta = null;
    @SerializedName("agebObjetivo")
    @Expose
    private String agebObjetivo;
    @SerializedName("manzanaObjetivo")
    @Expose
    private String manzanaObjetivo;
    private String enviado;

    public SurveySend(String token, String eInicio, String eFinal, Double latitude, Double longitude, String instrumentoId, String recorded, List<Encuestum> encuesta, String agebObjetivo, String manzanaObjetivo) {
        this.token = token;
        this.eInicio = eInicio;
        this.eFinal = eFinal;
        this.latitude = latitude;
        this.longitude = longitude;
        this.instrumentoId = instrumentoId;
        this.recorded = recorded;
        this.encuesta = encuesta;
        this.agebObjetivo = agebObjetivo;
        this.manzanaObjetivo = manzanaObjetivo;
        this.enviado = "false";
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEnviado() { return enviado; }

    public void setEnviado(String enviado) { this.enviado = enviado; }

    public String getEInicio() {
        return eInicio;
    }

    public void setEInicio(String eInicio) {
        this.eInicio = eInicio;
    }

    public String getEfinal() {
        return eFinal;
    }

    public void setEfinal(String efinal) {
        this.eFinal = efinal;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getEncuestaId() {
        return instrumentoId;
    }

    public void setEncuestaId(String instrumentoId) {
        this.instrumentoId = instrumentoId;
    }

    public List<Encuestum> getEncuesta() {
        return encuesta;
    }

    public void setEncuesta(List<Encuestum> encuesta) {
        this.encuesta = encuesta;
    }

    public String getStatus() { return status; }

    public String getMessage() { return message; }

    public String getRecorded() { return recorded; }

    public void setRecorded(String recorded) {
        this.recorded = recorded;
    }

    public String getAgebObjetivo() { return agebObjetivo; }

    public String toString() {
//        Log.d("mensaje", getClass().getName() + "@" + Integer.toHexString(hashCode()))
        return this.getToken() + ", "+this.getEInicio()+ ", "+this.getEfinal()+", "+this.getLatitude()+", "+this.getLongitude()+ ", "+this.getAgebObjetivo() + ", " + this.getRecorded();
//        return this.getEncuestaId()  + " -check- " + this.recorded;
    }
}
