package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion31 extends Fragment {
    private String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null, factor_6=null, factor_7=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false, checkFactor_6 = false, checkFactor_7 = false;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question31, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor31);
        Button next = (Button) view.findViewById(R.id.nextFactor31);

        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor31);
        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor31_2);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor31_3);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor31_4);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor31_5);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor31_6);
        final RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.groupFactor31_7);
//        final RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.groupFactor31_8);
        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(586);
                ((SurveyActivity)getActivity()).borrarItem(585);
                ((SurveyActivity)getActivity()).borrarItem(584);
                ((SurveyActivity)getActivity()).borrarItem(583);
                ((SurveyActivity)getActivity()).borrarItem(582);
                ((SurveyActivity)getActivity()).borrarItem(581);
                ((SurveyActivity)getActivity()).borrarItem(580);
                ((SurveyActivity)getActivity()).borrarItem(579);
                ((SurveyActivity)getActivity()).borrarItem(578);

                ((SurveyActivity) getActivity()).selectFragment(21);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
                List<String> respuesta7 = new ArrayList<String>();

                if(!checkFactor_1|| !checkFactor_2 || !checkFactor_3 || !checkFactor_4 || !checkFactor_5 || !checkFactor_6){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(587, "simple", respuesta1);
                    respuesta2.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(588, "simple", respuesta2);
                    respuesta3.add(factor_3);
                    ((SurveyActivity)getActivity()).crearItem(589, "simple", respuesta3);
                    respuesta4.add(factor_4);
                    ((SurveyActivity)getActivity()).crearItem(590, "simple", respuesta4);
                    respuesta5.add(factor_5);
                    ((SurveyActivity)getActivity()).crearItem(591, "simple", respuesta5);
                    respuesta6.add(factor_6);
                    ((SurveyActivity)getActivity()).crearItem(592, "simple", respuesta6);
//                    respuesta7.add(factor_7);
//                    ((SurveyActivity)getActivity()).crearItem(184, "simple", respuesta7);

                    ((SurveyActivity) getActivity()).selectFragment(23);
                }

            }
        });
        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor31_2_1:
                        factor_1 = "1126";
                        break;
                    case R.id.radioFactor31_2_2:
                        factor_1 = "1127";
                        break;
//                    case R.id.radioFactor31_2_3:
//                        factor_1 = "461";
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor31_3_1:
                        factor_2 = "1128";
                        break;
                    case R.id.radioFactor31_3_2:
                        factor_2 = "1129";
                        break;
//                    case R.id.radioFactor31_3_3:
//                        factor_2 = "464";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor31_4_1:
                        factor_3 = "1130";
                        break;
                    case R.id.radioFactor31_4_2:
                        factor_3 = "1131";
                        break;
//                    case R.id.radioFactor31_4_3:
//                        factor_3 = "467";
//                        break;
                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor31_5_1:
                        factor_4 = "1132";
                        break;
                    case R.id.radioFactor31_5_2:
                        factor_4 = "1133";
                        break;
                    case R.id.radioFactor31_5_3:
                        factor_4 = "1180";
                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor31_6_1:
                        factor_5 = "1134";
                        break;
                    case R.id.radioFactor31_6_2:
                        factor_5 = "1135";
                        break;
                    case R.id.radioFactor31_6_3:
                        factor_5 = "1181";
                        break;
                }
            }
        });

        GFactor6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_6 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor31_7_1:
                        factor_6 = "1136";
                        break;
                    case R.id.radioFactor31_7_2:
                        factor_6 = "1137";
                        break;
                    case R.id.radioFactor31_7_3:
                        factor_6 = "1182";
                        break;
                }
            }
        });

        return view;
    }
}