package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion29 extends Fragment {
    private String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null, factor_6=null, factor_7=null, factor_8=null, factor_9=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false, checkFactor_6 = false, checkFactor_7 = false, checkFactor_8 = false, checkFactor_9 = false;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question29, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor29);
        Button next = (Button) view.findViewById(R.id.nextFactor29);

        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor29);
        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor29_2);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor29_3);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor29_4);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor29_5);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor29_6);
        final RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.groupFactor29_7);
        final RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.groupFactor29_8);
        final RadioGroup GFactor8 = (RadioGroup) view.findViewById(R.id.groupFactor29_9);
//        final RadioGroup GFactor9 = (RadioGroup) view.findViewById(R.id.groupFactor29_10);
        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((SurveyActivity)getActivity()).borrarItem(159);
                ((SurveyActivity)getActivity()).borrarItem(569);
                ((SurveyActivity)getActivity()).borrarItem(568);
                ((SurveyActivity)getActivity()).borrarItem(567);
                ((SurveyActivity)getActivity()).borrarItem(566);
                ((SurveyActivity)getActivity()).borrarItem(565);
                ((SurveyActivity)getActivity()).borrarItem(564);
                ((SurveyActivity)getActivity()).borrarItem(563);

                ((SurveyActivity) getActivity()).selectFragment(19);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
                List<String> respuesta7 = new ArrayList<String>();
                List<String> respuesta8 = new ArrayList<String>();
//                List<String> respuesta9 = new ArrayList<String>();

                if(!checkFactor_1|| !checkFactor_2 || !checkFactor_3 || !checkFactor_4 || !checkFactor_5 || !checkFactor_6 || !checkFactor_7 || !checkFactor_8){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(570, "simple", respuesta1);
                    respuesta2.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(571, "simple", respuesta2);
                    respuesta3.add(factor_3);
                    ((SurveyActivity)getActivity()).crearItem(572, "simple", respuesta3);
                    respuesta4.add(factor_4);
                    ((SurveyActivity)getActivity()).crearItem(573, "simple", respuesta4);
                    respuesta5.add(factor_5);
                    ((SurveyActivity)getActivity()).crearItem(574, "simple", respuesta5);
                    respuesta6.add(factor_6);
                    ((SurveyActivity)getActivity()).crearItem(575, "simple", respuesta6);
                    respuesta7.add(factor_7);
                    ((SurveyActivity)getActivity()).crearItem(576, "simple", respuesta7);
                    respuesta8.add(factor_8);
                    ((SurveyActivity)getActivity()).crearItem(577, "simple", respuesta8);
//                    respuesta9.add(factor_9);
//                    ((SurveyActivity)getActivity()).crearItem(168, "simple", respuesta9);

                    ((SurveyActivity) getActivity()).selectFragment(21);
                }


            }
        });

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor29_2_1:
                        factor_1 = "1092";
                        break;
                    case R.id.radioFactor29_2_2:
                        factor_1 = "1093";
                        break;
//                    case R.id.radioFactor29_2_3:
//                        factor_1 = "410";
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor29_3_1:
                        factor_2 = "1094";
                        break;
                    case R.id.radioFactor29_3_2:
                        factor_2 = "1095";
                        break;
//                    case R.id.radioFactor29_3_3:
//                        factor_2 = "413";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor29_4_1:
                        factor_3 = "1096";
                        break;
                    case R.id.radioFactor29_4_2:
                        factor_3 = "1097";
                        break;
//                    case R.id.radioFactor29_4_3:
//                        factor_3 = "416";
//                        break;
                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor29_5_1:
                        factor_4 = "1098";
                        break;
                    case R.id.radioFactor29_5_2:
                        factor_4 = "1099";
                        break;
//                    case R.id.radioFactor29_5_3:
//                        factor_4 = "419";
//                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor29_6_1:
                        factor_5 = "1100";
                        break;
                    case R.id.radioFactor29_6_2:
                        factor_5 = "1101";
                        break;
//                    case R.id.radioFactor29_6_3:
//                        factor_5 = "422";
//                        break;
                }
            }
        });

        GFactor6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_6 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor29_7_1:
                        factor_6 = "1102";
                        break;
                    case R.id.radioFactor29_7_2:
                        factor_6 = "1103";
                        break;
//                    case R.id.radioFactor29_7_3:
//                        factor_6 = "425";
//                        break;
                }
            }
        });

        GFactor7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_7 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor29_8_1:
                        factor_7 = "1104";
                        break;
                    case R.id.radioFactor29_8_2:
                        factor_7 = "1105";
                        break;
//                    case R.id.radioFactor29_8_3:
//                        factor_7 = "428";
//                        break;
                }
            }
        });

        GFactor8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_8 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor29_9_1:
                        factor_8 = "1106";
                        break;
                    case R.id.radioFactor29_9_2:
                        factor_8 = "1107";
                        break;
//                    case R.id.radioFactor29_9_3:
//                        factor_8 = "431";
//                        break;
                }
            }
        });

        return view;
    }}