package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion3 extends Fragment{

    private int itemID = 344, responseID = 265;

    CheckBox chkR3_1, chkR3_2, chkR3_3, chkR3_4, chkR3_5, chkR3_6, chkR3_7;
    CheckBox chkR4_1, chkR4_2, chkR4_3, chkR4_4, chkR4_5, chkR4_6, chkR4_7;
    EditText otros3, otros4;

    //    CheckBox chkR5_1, chkR5_2, chkR5_3, chkR5_4, chkR5_5, chkR5_6, chkR5_7;
    //    CheckBox chkR6_1, chkR6_2, chkR6_3, chkR6_4, chkR6_5, chkR6_6, chkR6_7;

    //    EditText otros5, otros6;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question3, container, false);
        Button next = (Button) view.findViewById(R.id.bNext);
        Button back = (Button) view.findViewById(R.id.bBack);



        final ScrollView scroll = (ScrollView) view.findViewById(R.id.SV1);
//        final TextView text = (TextView) view.findViewById(R.id.textViewF4);
//        final ConstraintLayout constrain = (ConstraintLayout) view.findViewById(R.id.constraintLayout2);
//        final ConstraintLayout constrai2 = (ConstraintLayout) view.findViewById(R.id.constraintLayout3);

        scroll.fullScroll(ScrollView.FOCUS_UP);
        chkR3_1 = (CheckBox) view.findViewById(R.id.chkOp1A);
        chkR3_2 = (CheckBox) view.findViewById(R.id.chkOp2A);
        chkR3_3 = (CheckBox) view.findViewById(R.id.chkOp3A);
        chkR3_4 = (CheckBox) view.findViewById(R.id.chkOp4A);
        chkR3_5 = (CheckBox) view.findViewById(R.id.chkOp5A);
        chkR3_6 = (CheckBox) view.findViewById(R.id.chkOp6A);
        chkR3_7 = (CheckBox) view.findViewById(R.id.chkOp7A);

        chkR4_1 = (CheckBox) view.findViewById(R.id.chkOp1B);
        chkR4_2 = (CheckBox) view.findViewById(R.id.chkOp2B);
        chkR4_3 = (CheckBox) view.findViewById(R.id.chkOp3B);
        chkR4_4 = (CheckBox) view.findViewById(R.id.chkOp4B);
        chkR4_5 = (CheckBox) view.findViewById(R.id.chkOp5B);
        chkR4_6 = (CheckBox) view.findViewById(R.id.chkOp6B);
//        chkR4_7 = (CheckBox) view.findViewById(R.id.chkOp7B);

        otros3 = (EditText) view.findViewById(R.id.otros3);
        otros4 = (EditText) view.findViewById(R.id.otros4);


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();

                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
//                List<String> respuesta5 = new ArrayList<String>();
//                List<String> respuesta6 = new ArrayList<String>();
//                List<String> respuesta7 = new ArrayList<String>();

                boolean checkFactor_Medic = false, checkFactor_Colonia = false;

                String result = "Medicina";
                if( chkR3_1.isChecked()){checkFactor_Medic = true; result += "\nAutomedicado";}
                if( chkR3_2.isChecked()){checkFactor_Medic = true; result += "\nParticular";}
                if( chkR3_3.isChecked()){checkFactor_Medic = true; result += "\nParticular cancun";}
                if( chkR3_4.isChecked()){checkFactor_Medic = true; result += "\nHospital general";}
                if( chkR3_5.isChecked()){checkFactor_Medic = true; result += "\nCruz roja";}
                if( chkR3_6.isChecked()){checkFactor_Medic = true; result += "\nFarmacia simi";}
                if( chkR3_7.isChecked()){checkFactor_Medic = true; result += "\notro5";}
                Log.d("mensaje", "Respuestas 5: "+ result);

                String factor_2 = otros3.getText().toString().toUpperCase();
                Log.d("mensaje", "Respuestas 6: "+ factor_2);

                String result2 = "colonia";
                if( chkR4_1.isChecked()){checkFactor_Colonia = true; result2 += "\nParque";}
                if( chkR4_2.isChecked()){checkFactor_Colonia = true; result2 += "\nDeportivo";}
                if( chkR4_3.isChecked()){checkFactor_Colonia = true; result2 += "\nJardines zen";}
                if( chkR4_4.isChecked()){checkFactor_Colonia = true; result2 += "\nCentro urbano";}
                if( chkR4_5.isChecked()){checkFactor_Colonia = true; result2 += "\nBabateca";}
                if( chkR4_6.isChecked()){checkFactor_Colonia = true; result2 += "\nOtro6";}
//                if( chkR4_7.isChecked()){checkFactor_Tienda = true; result2 += "\nOtro4";}
                Log.d("mensaje","Respuestas 7:"+  result2);

                String factor_4 = otros4.getText().toString().toUpperCase();
                Log.d("mensaje", "Respuestas 8: "+ factor_4);


                if(!checkFactor_Medic || !checkFactor_Colonia){
                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else{
                    if(  (chkR3_7.isChecked() && otros3.getText().toString().length() == 0) || (chkR4_6.isChecked() && otros4.getText().toString().length() == 0) ){
//
                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                    } else {

                        if(chkR3_1.isChecked()){respuesta1.add(Integer.toString(responseID));}
                        if(chkR3_2.isChecked()){respuesta1.add(Integer.toString(responseID+1));}
                        if(chkR3_3.isChecked()){respuesta1.add(Integer.toString(responseID+2));}
                        if(chkR3_4.isChecked()){respuesta1.add(Integer.toString(responseID+3));}
                        if(chkR3_5.isChecked()){respuesta1.add(Integer.toString(responseID+4));}
                        if(chkR3_6.isChecked()){respuesta1.add(Integer.toString(responseID+5));}
                        if(chkR3_7.isChecked()){respuesta1.add(Integer.toString(responseID+6));}

                        ((SurveyActivity)getActivity()).crearItem(itemID, "multiple", respuesta1);

                        respuesta2.add(factor_2);
                        ((SurveyActivity)getActivity()).crearItem(itemID+1, "text", respuesta2);

                        if(chkR4_1.isChecked()){respuesta3.add(Integer.toString(responseID+7));}
                        if(chkR4_2.isChecked()){respuesta3.add(Integer.toString(responseID+8));}
                        if(chkR4_3.isChecked()){respuesta3.add(Integer.toString(responseID+9));}
                        if(chkR4_4.isChecked()){respuesta3.add(Integer.toString(responseID+10));}
                        if(chkR4_5.isChecked()){respuesta3.add(Integer.toString(responseID+11));}
                        if(chkR4_6.isChecked()){respuesta3.add(Integer.toString(responseID+12));}
//                        if(chkR4_7.isChecked()){respuesta3.add(Integer.toString(responseID+13));}

                        ((SurveyActivity)getActivity()).crearItem(itemID+2, "multiple", respuesta3);

                        respuesta4.add(factor_4);
                        ((SurveyActivity)getActivity()).crearItem(itemID+3, "text", respuesta4);
                        ((SurveyActivity)getActivity()).hideKeyboard(v);
                        ((SurveyActivity)getActivity()).selectFragment(4);
                    }
                }


            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
                ((SurveyActivity)getActivity()).borrarItem(itemID-3);
                ((SurveyActivity)getActivity()).borrarItem(itemID-4);

                ((SurveyActivity)getActivity()).hideKeyboard(v);
                ((SurveyActivity)getActivity()).selectFragment(2);
            }
        });

//        scroll.setVisibility(View.GONE);
//        constrain.setVisibility(View.GONE);
//        constrai2.setVisibility(View.GONE);
        otros3.setVisibility(view.GONE);
        otros4.setVisibility(view.GONE);



        chkR3_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    Log.d("mensaje","otros 3 checado:");
                    otros3.setVisibility(View.VISIBLE);
                } else {
                    Log.d("mensaje","otros  no 3 checado:");
                    otros3.setVisibility(view.GONE);
                    otros3.setText("");
                }
            }
        });

        chkR4_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    Log.d("mensaje","otros 4 checado:");
                    otros4.setVisibility(View.VISIBLE);
                } else {
                    Log.d("mensaje","otros  no 4 checado:");
                    otros4.setVisibility(view.GONE);
                    otros4.setText("");
                }
            }
        });


        return view;
    }
}


