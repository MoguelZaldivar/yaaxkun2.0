package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.EditText;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion5 extends Fragment {
    private int itemID = 350, responseID = 288;
    private String factor_1=null, factor_2=null;
    boolean checkFactor_1 = false, checkFactor_2 = false;
    EditText otro1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question5, container, false);
        Button next = (Button) view.findViewById(R.id.bNext);
        Button back = (Button) view.findViewById(R.id.bBack);

        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.rG1);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.rG2);
        otro1 = (EditText) view.findViewById(R.id.otroText1);

        final ScrollView scroll = (ScrollView) view.findViewById(R.id.SV1);
//        final TextView text = (TextView) view.findViewById(R.id.textViewF4);


        scroll.fullScroll(ScrollView.FOCUS_UP);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();

                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();

                String factor_Otro = otro1.getText().toString().toUpperCase();

                if(!checkFactor_1 || !checkFactor_2 || (factor_1.equals( Integer.toString( responseID+6 ) ) && factor_Otro.equals("")) ){
                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else{
//                    if(checkFactor_1 && checkFactor_2){
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(itemID, "simple", respuesta1);

                    respuesta2.add(factor_Otro);
                    ((SurveyActivity)getActivity()).crearItem(itemID+1, "text", respuesta2);

                    respuesta3.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(itemID+2, "simple", respuesta3);

                    ((SurveyActivity)getActivity()).hideKeyboard(v);
                    ((SurveyActivity)getActivity()).selectFragment(6);
//                    } else {
//                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
//                    }
                }


            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
                ((SurveyActivity)getActivity()).hideKeyboard(v);
                ((SurveyActivity)getActivity()).selectFragment(4);
            }
        });
        otro1.setVisibility(view.GONE);

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.rdb1_1:
                        factor_1 = Integer.toString(responseID);
                        otro1.setVisibility(view.GONE);
                        otro1.setText("");
                        break;
                    case R.id.rdb1_2:
                        factor_1 = Integer.toString(responseID+1);
                        otro1.setVisibility(view.GONE);
                        otro1.setText("");
                        break;
                    case R.id.rdb1_3:
                        factor_1 = Integer.toString(responseID+2);
                        otro1.setVisibility(view.GONE);
                        otro1.setText("");
                        break;
                    case R.id.rdb1_4:
                        factor_1 = Integer.toString(responseID+3);
                        otro1.setVisibility(view.GONE);
                        otro1.setText("");
                        break;
                    case R.id.rdb1_5:
                        factor_1 = Integer.toString(responseID+4);
                        otro1.setVisibility(view.GONE);
                        otro1.setText("");
                        break;
                    case R.id.rdb1_6:
                        factor_1 = Integer.toString(responseID+5);
                        otro1.setVisibility(view.GONE);
                        otro1.setText("");
                        break;
                    case R.id.rdb1_7:
                        factor_1 = Integer.toString(responseID+6);
                        otro1.setVisibility(view.VISIBLE);
                        break;

                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.rdb2_1:
                        factor_2 = Integer.toString(responseID+7);
                        break;
                    case R.id.rdb2_2:
                        factor_2 = Integer.toString(responseID+8);
                        break;
                    case R.id.rdb2_3:
                        factor_2 = Integer.toString(responseID+9);
                        break;
                    case R.id.rdb2_4:
                        factor_2 = Integer.toString(responseID+10);
                        break;
                }
            }
        });

        return view;
    }
}