package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion1 extends Fragment {


    private String error="";
    private boolean checkSpinner1 = false, checkSpinner2 = false, checkSpinner3 = false, checkSpinner4 = false, checkSpinner5 = false, checkSpinner6 = false, checkSpinner7 = false, checkSpinner8 = false;
    private int itemID = 332,nPersonas = 0, nEstudiantes = 0, nMayores = 0, nMenores = 0, nJN = 0, nPrim = 0, nSecu = 0, nPrep = 0, nLic = 0, nPos =0;
    private Spinner menores, mayores, jardinN, primaria, secu, prepa, lic, posG;
//    private RadioGroup GSex, GMadre, GPadre, GCasa, GDuration, GDurationCasa;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_question1, container,  false);
        Button next = (Button) view.findViewById(R.id.bNext);
        Button back = (Button) view.findViewById(R.id.bBack);
//        GSex = (RadioGroup) view.findViewById(R.id.rGSexo);

        final ScrollView scroll = (ScrollView) view.findViewById(R.id.scrollFactor1);

        final TextView text5_1 = (TextView) view.findViewById(R.id.textViewF1);
        final TextView text5_2 = (TextView) view.findViewById(R.id.textViewF2);


        mayores = (Spinner) view.findViewById(R.id.mayorNumberspinner);
        menores = (Spinner) view.findViewById(R.id.menorNumberspinner);
        jardinN = (Spinner) view.findViewById(R.id.jNiniosNumberspinner);
        primaria = (Spinner) view.findViewById(R.id.primNumberspinner);
        secu = (Spinner) view.findViewById(R.id.secuNumberspinner);
        prepa = (Spinner) view.findViewById(R.id.prepNumberspinner);
        lic = (Spinner) view.findViewById(R.id.licNumberspinner);
        posG = (Spinner) view.findViewById(R.id.posNumberspinner);

        scroll.fullScroll(ScrollView.FOCUS_UP);
//
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkSpinner1 && checkSpinner2
//                        && checkSpinner3 && checkSpinner4 && checkSpinner5 && checkSpinner6 && checkSpinner7 && checkSpinner8
                ){
                    nPersonas = nMayores + nMenores;
                    nEstudiantes = nJN + nPrim + nSecu + nPrep + nLic + nPos;
                    if(nPersonas >= nEstudiantes){

//                        if((NPersonas == NHombres+NMujeres) && (NPersonas > 0)){
//                            Log.d("mensaje", "\nPersonas: "+ NPersonas+"\n"+"Hombres: "+ NHombres+"\n"+"Mujeres: "+ NMujeres+"\n"+"Menores: "+ NMenores+"\n"+"Meses: "+ NCasaM+"\n"+"Anios: "+ NCasaA+"\n");

                            List<String> respuesta1 = new ArrayList<String>();
                            List<String> respuesta2 = new ArrayList<String>();
                            List<String> respuesta3 = new ArrayList<String>();
                            List<String> respuesta4 = new ArrayList<String>();
                            List<String> respuesta5 = new ArrayList<String>();
                            List<String> respuesta6 = new ArrayList<String>();
                            List<String> respuesta7 = new ArrayList<String>();
                            List<String> respuesta8 = new ArrayList<String>();

                            respuesta1.add(String.valueOf(nMayores));
                            respuesta2.add(String.valueOf(nMenores));
                            respuesta3.add(String.valueOf(nJN));
                            respuesta4.add(String.valueOf(nPrim));
                            respuesta5.add(String.valueOf(nSecu));
                            respuesta6.add(String.valueOf(nPrep));
                            respuesta7.add(String.valueOf(nLic));
                            respuesta8.add(String.valueOf(nPos));


                            ((SurveyActivity)getActivity()).crearItem(itemID, "numeric", respuesta1);
                            ((SurveyActivity)getActivity()).crearItem(itemID+1, "numeric", respuesta2);
                            ((SurveyActivity)getActivity()).crearItem(itemID+2, "numeric", respuesta3);
                            ((SurveyActivity)getActivity()).crearItem(itemID+3, "numeric", respuesta4);
                            ((SurveyActivity)getActivity()).crearItem(itemID+4, "numeric", respuesta5);
                            ((SurveyActivity)getActivity()).crearItem(itemID+5, "numeric", respuesta6);
                            ((SurveyActivity)getActivity()).crearItem(itemID+6, "numeric", respuesta7);
                            ((SurveyActivity)getActivity()).crearItem(itemID+7, "numeric", respuesta8);

                            ((SurveyActivity)getActivity()).selectFragment(2);

//                        } else{
//                            Toast.makeText(getActivity().getApplicationContext(), "Mínimo debe haber una persona" ,Toast.LENGTH_SHORT).show();
//
//                        }
//                        Log.d("mensaje", "Personas: "+nMayores + " "+nMenores);
//                        Log.d("mensaje", "Estudiantes: "+ nJN + " " + nPrim + " " + nSecu + " " + nPrep + " " + nLic + " " + nPos);
//                        Log.d("mensaje", "total: "+nPersonas + " "+nEstudiantes);
                    }else{
                        Toast.makeText(getActivity().getApplicationContext(), "No puede haber mas personas estudiando de las que viven en casa"+error ,Toast.LENGTH_SHORT).show();
                        validate();
                    }

                }else {
                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos"+error ,Toast.LENGTH_SHORT).show();
                    validate();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
                ((SurveyActivity)getActivity()).borrarItem(itemID-3);
                ((SurveyActivity)getActivity()).borrarItem(itemID-4);
                ((SurveyActivity)getActivity()).borrarItem(itemID-5);
                ((SurveyActivity)getActivity()).borrarItem(itemID-6);
                ((SurveyActivity)getActivity()).borrarItem(itemID-7);
                ((SurveyActivity)getActivity()).borrarItem(itemID-8);
                ((SurveyActivity)getActivity()).borrarItem(itemID-9);
                ((SurveyActivity)getActivity()).borrarItem(itemID-10);
//                ((SurveyActivity)getActivity()).borrarItem(itemID-11);
//                ((SurveyActivity)getActivity()).hideKeyboard();
                ((SurveyActivity)getActivity()).selectFragment( 0);
            }
        });
        mayores.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.d("mensaje", "Personas: "+i + " - " + l);
                if (i>0){
                    checkSpinner1 = true;
                    nMayores = i-1;
                    error = "";
//                    Log.d("mensaje", "mayores: "+nMayores);
//

                }else{

                    nMayores = 0;
                    checkSpinner1 = false;
//
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        menores.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.d("mensaje", "Personas: "+i + " - " + l);
                if (i>0){
                    checkSpinner2 = true;
                    nMenores = i-1;
                    error = "";
//                    Log.d("mensaje", "menores: "+nMenores);
//

                }else{

                    nMenores = 0;
                    checkSpinner2 = false;
//
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        jardinN.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.d("mensaje", "Personas: "+i + " - " + l);
                if (i>0){
                    checkSpinner3 = true;
                    nJN = i-1;
                    error = "";
//                    Log.d("mensaje", "Jardin: "+nJN);
//
                }else{
                    nJN = 0;
                    checkSpinner3 = false;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
//
        primaria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.d("mensaje", "Personas: "+i + " - " + l);
                if (i>0){
                    checkSpinner4 = true;
                    nPrim = i-1;
                    error = "";
//                    Log.d("mensaje", "Primaria: "+nPrim);
//
                }else{
                    nPrim = 0;
                    checkSpinner4 = false;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        secu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.d("mensaje", "Personas: "+i + " - " + l);
                if (i>0){
                    checkSpinner5 = true;
                    nSecu = i-1;
                    error = "";
//                    Log.d("mensaje", "Secundaria: "+nSecu);
//
                }else{
                    nSecu = 0;
                    checkSpinner5 = false;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        prepa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.d("mensaje", "Personas: "+i + " - " + l);
                if (i>0){
                    checkSpinner6 = true;
                    nPrep = i-1;
                    error = "";
//                    Log.d("mensaje", "Prepa: "+nPrep);
//
                }else{
                    nPrep = 0;
                    checkSpinner6 = false;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        lic.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.d("mensaje", "Personas: "+i + " - " + l);
                if (i>0){
                    checkSpinner7 = true;
                    nLic = i-1;
                    error = "";
//                    Log.d("mensaje", "licenciatura: "+nLic);
//
                }else{
                    nLic = 0;
                    checkSpinner7 = false;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        posG.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.d("mensaje", "Personas: "+i + " - " + l);
                if (i>0){
                    checkSpinner8 = true;
                    nPos = i-1;
                    error = "";
//                    Log.d("mensaje", "Posgrado: "+nPos);
//
                }else{
                    nPos = 0;
                    checkSpinner8 = false;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return view;
    }

    private void validate(){
        Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);

        if (!checkSpinner1) {
//            Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);
            mayores.setFocusableInTouchMode(true);
            mayores.startAnimation(shake);
            mayores.requestFocusFromTouch();
            mayores.clearFocus();

        }else if (!checkSpinner2) {
//            Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);
            menores.setFocusableInTouchMode(true);
            menores.startAnimation(shake);
            menores.requestFocusFromTouch();
            menores.clearFocus();
        }else if (!checkSpinner3) {
//            Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);
            jardinN.setFocusableInTouchMode(true);
            jardinN.startAnimation(shake);
            jardinN.requestFocusFromTouch();
            jardinN.clearFocus();
        }else if (!checkSpinner4) {
//            Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);
            primaria.setFocusableInTouchMode(true);
            primaria.startAnimation(shake);
            primaria.requestFocusFromTouch();
            primaria.clearFocus();
        }else if (!checkSpinner5) {
//            Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);
            secu.setFocusableInTouchMode(true);
            secu.startAnimation(shake);
            secu.requestFocusFromTouch();
            secu.clearFocus();
        }else if (!checkSpinner6) {
//            Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);
            prepa.setFocusableInTouchMode(true);
            prepa.startAnimation(shake);
            prepa.requestFocusFromTouch();
            prepa.clearFocus();
        }else if (!checkSpinner7) {
//            Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);
            lic.setFocusableInTouchMode(true);
            lic.startAnimation(shake);
            lic.requestFocusFromTouch();
            lic.clearFocus();
        }else if (!checkSpinner8) {
//            Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);
            posG.setFocusableInTouchMode(true);
            posG.startAnimation(shake);
            posG.requestFocusFromTouch();
            posG.clearFocus();

        }
//
//        GSex.setFocusableInTouchMode(false);
        mayores.setFocusableInTouchMode(false);
        menores.setFocusableInTouchMode(false);
        jardinN.setFocusableInTouchMode(false);
        primaria.setFocusableInTouchMode(false);
        secu.setFocusableInTouchMode(false);
        prepa.setFocusableInTouchMode(false);
        lic.setFocusableInTouchMode(false);
        posG.setFocusableInTouchMode(false);

    }
}