package com.yaaxkun.Fragment;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion14 extends Fragment {
    private int itemID = 390, responseID = 426;
    private String factor_Main = null,factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null, factor_6=null;
    boolean checkFactor_Main = false, checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false, checkFactor_6 = false;
    EditText otro1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.fragment_question14, container, false);
        final ScrollView scroll = (ScrollView) view.findViewById(R.id.SV1);
        final ConstraintLayout dynCont = (ConstraintLayout) view.findViewById(R.id.constraintLayout2);
        Button next = (Button) view.findViewById(R.id.bNext);
        Button back = (Button) view.findViewById(R.id.bBack);
        RadioGroup GFactorMain = (RadioGroup) view.findViewById(R.id.rGM);
        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.rG1);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.rG2);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.rG3);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.rG4);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.rG5);
        final RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.rG6);
//        RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.rG6);
        otro1 = (EditText) view.findViewById(R.id.otroText1);
//        RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.rG7);
//        final EditText tOtros = (EditText)view.findViewById(R.id.eTOtros9);
        scroll.fullScroll(ScrollView.FOCUS_UP);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String factor_7 = tOtros.getText().toString().toUpperCase();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
                List<String> respuesta7 = new ArrayList<String>();
                List<String> respuesta8 = new ArrayList<String>();

//                List<String> respuesta7 = new ArrayList<String>();
//                ((SurveyActivity)getActivity()).hideKeyboard();
                String factor_Otro = otro1.getText().toString().toUpperCase();

                if(!checkFactor_Main || checkFactor_Main && !factor_Main.equals(Integer.toString(responseID)) && (!checkFactor_1 || !checkFactor_2 || !checkFactor_3 || !checkFactor_4 || !checkFactor_5 || !checkFactor_6 || (factor_6.equals(Integer.toString(responseID+15)) && factor_Otro.equals("") )) ){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {

                    respuesta1.add(factor_Main);
                    ((SurveyActivity)getActivity()).crearItem(itemID, "simple", respuesta1);

                    respuesta2.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(itemID+1, "simple", respuesta2);

                    respuesta3.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(itemID+2, "simple", respuesta3);

                    respuesta4.add(factor_3);
                    ((SurveyActivity)getActivity()).crearItem(itemID+3, "simple", respuesta4);

                    respuesta5.add(factor_4);
                    ((SurveyActivity)getActivity()).crearItem(itemID+4, "simple", respuesta5);

                    respuesta6.add(factor_5);
                    ((SurveyActivity)getActivity()).crearItem(itemID+5, "simple", respuesta6);

                    respuesta7.add(factor_6);
                    ((SurveyActivity)getActivity()).crearItem(itemID+6, "simple", respuesta7);

                    respuesta8.add(factor_Otro);
                    ((SurveyActivity)getActivity()).crearItem(itemID+7, "text", respuesta8);
//                    Log.d("mensaje", "onClick: -"+ tOtros.getText().toString().toUpperCase()+"--");
//                    ((SurveyActivity)getActivity()).hideKeyboard();
                    ((SurveyActivity)getActivity()).hideKeyboard(v);
                    ((SurveyActivity)getActivity()).selectFragment(15);
                }

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
                ((SurveyActivity)getActivity()).borrarItem(itemID-3);
                ((SurveyActivity)getActivity()).hideKeyboard(v);
//                ((SurveyActivity)getActivity()).borrarItem(itemID-4);

//                ((SurveyActivity)getActivity()).hideKeyboard();
                ((SurveyActivity)getActivity()).selectFragment(13);
            }
        });

        dynCont.setVisibility(view.GONE);
        otro1.setVisibility(View.GONE);

        GFactorMain.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_Main = true;
                switch(checkedId)
                {
                    case R.id.rdb1_1:
                        GFactor1.clearCheck();
                        GFactor2.clearCheck();
                        GFactor3.clearCheck();
                        GFactor4.clearCheck();
                        GFactor5.clearCheck();
                        GFactor6.clearCheck();

                        factor_Main = Integer.toString(responseID);
                        factor_1 = null;
                        factor_2 = null;
                        factor_3 = null;
                        factor_4 = null;
                        factor_5 = null;
                        factor_6 = null;
                        otro1.setText("");

                        checkFactor_1 = false;
                        checkFactor_2 = false;
                        checkFactor_3 = false;
                        checkFactor_4 = false;
                        checkFactor_5 = false;
                        checkFactor_6 = false;

                        dynCont.setVisibility(View.GONE);
                        otro1.setVisibility(View.GONE);
                        break;
                    case R.id.rdb1_2:
                        factor_Main = Integer.toString(responseID+1);

                        dynCont.setVisibility(View.VISIBLE);

//                        viewScroll.fullScroll(ScrollView.FOCUS_UP);
                        break;
                    case R.id.rdb1_3:
                        factor_Main = Integer.toString(responseID+2);

                        dynCont.setVisibility(View.VISIBLE);
//                        viewScroll.fullScroll(ScrollView.FOCUS_UP);
                        break;
                    case R.id.rdb1_4:
                        factor_Main = Integer.toString(responseID+3);

                        dynCont.setVisibility(View.VISIBLE);
//                        viewScroll.fullScroll(ScrollView.FOCUS_UP);
                        break;
                    case R.id.rdb1_5:
                        factor_Main = Integer.toString(responseID+4);

                        dynCont.setVisibility(View.VISIBLE);
                        break;

                }
            }
        });

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.rdb1Si:
                        factor_1 = Integer.toString(responseID+5);
                        break;
                    case R.id.rdb1No:
                        factor_1 = Integer.toString(responseID+6);
                        break;

                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.rdb2Si:
                        factor_2 = Integer.toString(responseID+7);
                        break;
                    case R.id.rdb2No:
                        factor_2 = Integer.toString(responseID+8);
                        break;

                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.rdb3Si:
                        factor_3 = Integer.toString(responseID+9);
                        break;
                    case R.id.rdb3No:
                        factor_3 = Integer.toString(responseID+10);
                        break;

                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.rdb4Si:
                        factor_4 = Integer.toString(responseID+11);
                        break;
                    case R.id.rdb4No:
                        factor_4 = Integer.toString(responseID+12);
                        break;

                }
            }
        });

        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.rdb5Si:
                        factor_5 = Integer.toString(responseID+13);
                        break;
                    case R.id.rdb5No:
                        factor_5 = Integer.toString(responseID+14);
                        break;

                }
            }
        });

        GFactor6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_6 = true;
                switch(checkedId)
                {
                    case R.id.rdb6Si:
                        factor_6 = Integer.toString(responseID+15);
                        otro1.setVisibility(View.VISIBLE);
                        break;
                    case R.id.rdb6No:
                        factor_6 = Integer.toString(responseID+16);
                        otro1.setVisibility(View.GONE);
                        otro1.setText("");
                        break;

                }
            }
        });



        return view;
    }
}
