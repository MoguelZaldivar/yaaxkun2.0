package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion23 extends Fragment {
    private String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false;
    private RadioGroup GFactor1, GFactor2;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question23, container, false);


        Button prev = (Button) view.findViewById(R.id.prevFactor23);
        Button next = (Button) view.findViewById(R.id.nextFactor23);
        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor23);
        GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor23_1);
//        final RadioGroup
        GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor23_2);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor23_3);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor23_4);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor23_5);
//        final EditText tOtros = (EditText)view.findViewById(R.id.eTOtros23);
        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(514);
                ((SurveyActivity)getActivity()).borrarItem(513);
                ((SurveyActivity)getActivity()).borrarItem(512);
                ((SurveyActivity)getActivity()).borrarItem(511);
                ((SurveyActivity)getActivity()).borrarItem(510);
//                ((SurveyActivity)getActivity()).borrarItem(310);
//                ((SurveyActivity)getActivity()).hideKeyboard();
                ((SurveyActivity)getActivity()).selectFragment(12);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();
//                String factor_6 = tOtros.getText().toString().toUpperCase();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
//                List<String> respuesta6 = new ArrayList<String>();
//                ((SurveyActivity)getActivity()).hideKeyboard();

//                validate();
                if(!checkFactor_1){
                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else{
                    if((factor_1 == "982" && (checkFactor_2 && checkFactor_3 && checkFactor_4 && checkFactor_5))|| factor_1=="983"){
                        respuesta1.add(factor_1);
                        ((SurveyActivity)getActivity()).crearItem(515, "simple", respuesta1);
                        respuesta2.add(factor_2);
                        ((SurveyActivity)getActivity()).crearItem(516, "simple", respuesta2);
                        respuesta3.add(factor_3);
                        ((SurveyActivity)getActivity()).crearItem(517, "simple", respuesta3);
                        respuesta4.add(factor_4);
                        ((SurveyActivity)getActivity()).crearItem(518, "simple", respuesta4);
                        respuesta5.add(factor_5);
                        ((SurveyActivity)getActivity()).crearItem(519, "simple", respuesta5);
//                        respuesta6.add(factor_6);
//                        ((SurveyActivity)getActivity()).crearItem(321, "text", respuesta6);
//                        Log.d("mensaje", "onClick: -"+ tOtros.getText().toString().toUpperCase()+"--");
//                        ((SurveyActivity)getActivity()).hideKeyboard();

                        ((SurveyActivity)getActivity()).selectFragment(14);
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor23_1_1:
                        factor_1 = "982";

                        viewScroll.setVisibility(View.VISIBLE);
                        viewScroll.fullScroll(ScrollView.FOCUS_UP);
                        break;
                    case R.id.radioFactor23_1_2:
                        GFactor2.clearCheck();
                        GFactor3.clearCheck();
                        GFactor4.clearCheck();
                        GFactor5.clearCheck();
                        factor_1 = "983";
                        factor_2 = null;
                        factor_3 = null;
                        factor_4 = null;
                        factor_5 = null;
                        checkFactor_2 = false;
                        checkFactor_3 = false;
                        checkFactor_4 = false;
                        checkFactor_5 = false;
//                        GFactor1.clearCheck();


                        viewScroll.setVisibility(View.GONE);
                        break;
//                    case R.id.radioFactor23_1_3:
//                    GFactor2.clearCheck();
//                    GFactor3.clearCheck();
//                    GFactor4.clearCheck();
//                    GFactor5.clearCheck();
//                        viewScroll.setVisibility(View.GONE);
//                        factor_1 = "248";
//                        factor_2 = "251";
//                        factor_3 = "254";
//                        factor_4 = "257";
//                        factor_5 = "260";
////                        GFactor1.clearCheck();
//
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor23_2_1:
                        factor_2 = "984";
                        break;
                    case R.id.radioFactor23_2_2:
                        factor_2 = "985";
                        break;
//                    case R.id.radioFactor23_2_3:
//                        factor_2 = "251";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor23_3_1:
                        factor_3 = "986";
                        break;
                    case R.id.radioFactor23_3_2:
                        factor_3 = "987";
                        break;
//                    case R.id.radioFactor23_3_3:
//                        factor_3 = "254";
//                        break;
                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor23_4_1:
                        factor_4 = "988";
                        break;
                    case R.id.radioFactor23_4_2:
                        factor_4 = "989";
                        break;
//                    case R.id.radioFactor23_4_3:
//                        factor_4 = "257";
//                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor23_5_1:
                        factor_5 = "990";
                        break;
                    case R.id.radioFactor23_5_2:
                        factor_5 = "991";
                        break;
//                    case R.id.radioFactor23_5_3:
//                        factor_5 = "260";
//                        break;
                }
            }
        });



        return view;
    }
//    private void validate(){
//        if (!checkFactor_1) {
//            Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);
//            GFactor1.startAnimation(shake);
//            Toast.makeText(getActivity().getApplicationContext(), "Email Vacio", Toast.LENGTH_SHORT).show();
//
//        }
//        if (!checkFactor_2) {
//            Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);
//            GFactor2.startAnimation(shake);
//            Toast.makeText(getActivity().getApplicationContext(), "Email Vacio2", Toast.LENGTH_SHORT).show();
//
//        }
//    }
}
