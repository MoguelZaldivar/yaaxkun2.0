package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion28 extends Fragment {
    private String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null, factor_6=null, factor_7=null, factor_8=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false, checkFactor_6 = false, checkFactor_7 = false, checkFactor_8 = false;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question28, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor28);
        Button next = (Button) view.findViewById(R.id.nextFactor28);

        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor28);
        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor28_2);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor28_3);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor28_4);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor28_5);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor28_6);
        final RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.groupFactor28_7);
        final RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.groupFactor28_8);
//        final RadioGroup GFactor8 = (RadioGroup) view.findViewById(R.id.groupFactor28_9);
        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(562);
                ((SurveyActivity)getActivity()).borrarItem(561);
                ((SurveyActivity)getActivity()).borrarItem(560);
                ((SurveyActivity)getActivity()).borrarItem(559);
                ((SurveyActivity)getActivity()).borrarItem(558);

                ((SurveyActivity) getActivity()).selectFragment(18);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
                List<String> respuesta7 = new ArrayList<String>();
//                List<String> respuesta8 = new ArrayList<String>();

                if(!checkFactor_1|| !checkFactor_2 || !checkFactor_3 || !checkFactor_4 || !checkFactor_5 || !checkFactor_6 || !checkFactor_7){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(563, "simple", respuesta1);
                    respuesta2.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(564, "simple", respuesta2);
                    respuesta3.add(factor_3);
                    ((SurveyActivity)getActivity()).crearItem(565, "simple", respuesta3);
                    respuesta4.add(factor_4);
                    ((SurveyActivity)getActivity()).crearItem(566, "simple", respuesta4);
                    respuesta5.add(factor_5);
                    ((SurveyActivity)getActivity()).crearItem(567, "simple", respuesta5);
                    respuesta6.add(factor_6);
                    ((SurveyActivity)getActivity()).crearItem(568, "simple", respuesta6);
                    respuesta7.add(factor_7);
                    ((SurveyActivity)getActivity()).crearItem(569, "simple", respuesta7);
//                    respuesta8.add(factor_8);
//                    ((SurveyActivity)getActivity()).crearItem(159, "simple", respuesta8);

                    ((SurveyActivity) getActivity()).selectFragment(20);
                }


            }
        });

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor28_2_1:
                        factor_1 = "1078";
                        break;
                    case R.id.radioFactor28_2_2:
                        factor_1 = "1079";
                        break;
//                    case R.id.radioFactor28_2_3:
//                        factor_1 = "386";
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor28_3_1:
                        factor_2 = "1080";
                        break;
                    case R.id.radioFactor28_3_2:
                        factor_2 = "1081";
                        break;
//                    case R.id.radioFactor28_3_3:
//                        factor_2 = "389";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor28_4_1:
                        factor_3 = "1082";
                        break;
                    case R.id.radioFactor28_4_2:
                        factor_3 = "1083";
                        break;
//                    case R.id.radioFactor28_4_3:
//                        factor_3 = "392";
//                        break;
                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor28_5_1:
                        factor_4 = "1084";
                        break;
                    case R.id.radioFactor28_5_2:
                        factor_4 = "1085";
                        break;
//                    case R.id.radioFactor28_5_3:
//                        factor_4 = "395";
//                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor28_6_1:
                        factor_5 = "1086";
                        break;
                    case R.id.radioFactor28_6_2:
                        factor_5 = "1087";
                        break;
//                    case R.id.radioFactor28_6_3:
//                        factor_5 = "398";
//                        break;
                }
            }
        });

        GFactor6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_6 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor28_7_1:
                        factor_6 = "1088";
                        break;
                    case R.id.radioFactor28_7_2:
                        factor_6 = "1089";
                        break;
//                    case R.id.radioFactor28_7_3:
//                        factor_6 = "401";
//                        break;
                }
            }
        });

        GFactor7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_7 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor28_8_1:
                        factor_7 = "1090";
                        break;
                    case R.id.radioFactor28_8_2:
                        factor_7 = "1091";
                        break;
//                    case R.id.radioFactor28_8_3:
//                        factor_7 = "404";
//                        break;
                }
            }
        });

        return view;
    }
}