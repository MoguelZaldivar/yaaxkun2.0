package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion11 extends Fragment {
    private int itemID = 375, responseID = 390;
    private String factor_1=null, factor_2=null;
    boolean checkFactor_1 = false, checkFactor_2 = false;
    CheckBox chkR4_1, chkR4_2, chkR4_3, chkR4_4, chkR4_5, chkR4_6, chkR4_7;
    EditText otro1, otro2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question11, container, false);
        Button next = (Button) view.findViewById(R.id.bNext);
        Button back = (Button) view.findViewById(R.id.bBack);

        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.rG1);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.rG2);
        otro1 = (EditText) view.findViewById(R.id.otroText1);
        otro2 = (EditText) view.findViewById(R.id.otroText2);

        chkR4_1 = (CheckBox) view.findViewById(R.id.chkOp1B);
        chkR4_2 = (CheckBox) view.findViewById(R.id.chkOp2B);
        chkR4_3 = (CheckBox) view.findViewById(R.id.chkOp3B);
        chkR4_4 = (CheckBox) view.findViewById(R.id.chkOp4B);
        chkR4_5 = (CheckBox) view.findViewById(R.id.chkOp5B);
        chkR4_6 = (CheckBox) view.findViewById(R.id.chkOp6B);
        chkR4_7 = (CheckBox) view.findViewById(R.id.chkOp7B);

        final ScrollView scroll = (ScrollView) view.findViewById(R.id.SV1);
//        final TextView text = (TextView) view.findViewById(R.id.textViewF4);


        scroll.fullScroll(ScrollView.FOCUS_UP);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();

                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();

                boolean checkFactor_Tradicion = false;

                String result2 = "fiestas paganas";
                if( chkR4_1.isChecked()){checkFactor_Tradicion = true; result2 += "\nFiesta Virgen Fatima";}
                if( chkR4_2.isChecked()){checkFactor_Tradicion = true; result2 += "\nFiestas patronales";}
                if( chkR4_3.isChecked()){checkFactor_Tradicion = true; result2 += "\nFestival Cultura Caribe";}
                if( chkR4_4.isChecked()){checkFactor_Tradicion = true; result2 += "\nCarnaval";}
                if( chkR4_5.isChecked()){checkFactor_Tradicion = true; result2 += "\nFin de anio";}
                if( chkR4_6.isChecked()){checkFactor_Tradicion = true; result2 += "\njalowin";}
                if( chkR4_7.isChecked()){checkFactor_Tradicion = true; result2 += "\nOtro2";}
                Log.d("mensaje","Respuestas 5:"+  result2);

                String factor_Otro1 = otro1.getText().toString().toUpperCase();
                String factor_Otro2 = otro2.getText().toString().toUpperCase();

                if(!checkFactor_1 || !checkFactor_Tradicion || (factor_1.equals(Integer.toString(responseID+6)) && factor_Otro1.equals("")) ||
//                        (factor_2.equals(Integer.toString(responseID+13)) && factor_Otro2.equals(""))
                        (chkR4_7.isChecked() && otro2.getText().toString().length() == 0)){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else{
//                    if(checkFactor_1 && checkFactor_2){
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(itemID, "simple", respuesta1);

                    respuesta2.add(factor_Otro1);
                    ((SurveyActivity)getActivity()).crearItem(itemID+1, "text", respuesta2);

                    if(chkR4_1.isChecked()){respuesta3.add(Integer.toString(responseID+7));}
                    if(chkR4_2.isChecked()){respuesta3.add(Integer.toString(responseID+8));}
                    if(chkR4_3.isChecked()){respuesta3.add(Integer.toString(responseID+9));}
                    if(chkR4_4.isChecked()){respuesta3.add(Integer.toString(responseID+10));}
                    if(chkR4_5.isChecked()){respuesta3.add(Integer.toString(responseID+11));}
                    if(chkR4_6.isChecked()){respuesta3.add(Integer.toString(responseID+12));}
                    if(chkR4_7.isChecked()){respuesta3.add(Integer.toString(responseID+13));}
//                    respuesta3.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(itemID+2, "multiple", respuesta3);

                    respuesta4.add(factor_Otro2);
                    ((SurveyActivity)getActivity()).crearItem(itemID+3, "text", respuesta4);

                    ((SurveyActivity)getActivity()).hideKeyboard(v);
                    ((SurveyActivity)getActivity()).selectFragment(12);
//                    } else {
//                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
//                    }
                }


            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
                ((SurveyActivity)getActivity()).borrarItem(itemID-3);
                ((SurveyActivity)getActivity()).borrarItem(itemID-4);
                ((SurveyActivity)getActivity()).borrarItem(itemID-5);
                ((SurveyActivity)getActivity()).borrarItem(itemID-6);
                ((SurveyActivity)getActivity()).hideKeyboard(v);
                ((SurveyActivity)getActivity()).selectFragment(10);
            }
        });
        otro1.setVisibility(view.GONE);
        otro2.setVisibility(view.GONE);

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.rdb1_1:
                        factor_1 = Integer.toString(responseID);
                        otro1.setVisibility(view.GONE);
                        otro1.setText("");
                        break;
                    case R.id.rdb1_2:
                        factor_1 = Integer.toString(responseID+1);
                        otro1.setVisibility(view.GONE);
                        otro1.setText("");
                        break;
                    case R.id.rdb1_3:
                        factor_1 = Integer.toString(responseID+2);
                        otro1.setVisibility(view.GONE);
                        otro1.setText("");
                        break;
                    case R.id.rdb1_4:
                        factor_1 = Integer.toString(responseID+3);
                        otro1.setVisibility(view.GONE);
                        otro1.setText("");
                        break;
                    case R.id.rdb1_5:
                        factor_1 = Integer.toString(responseID+4);
                        otro1.setVisibility(view.GONE);
                        otro1.setText("");
                        break;
                    case R.id.rdb1_6:
                        factor_1 = Integer.toString(responseID+5);
                        otro1.setVisibility(view.GONE);
                        otro1.setText("");
                        break;
                    case R.id.rdb1_7:
                        factor_1 = Integer.toString(responseID+6);
                        otro1.setVisibility(view.VISIBLE);
                        break;

                }
            }
        });

        chkR4_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    Log.d("mensaje","otros 2 checado:");
                    otro2.setVisibility(View.VISIBLE);
                } else {
                    Log.d("mensaje","otros  no 2 checado:");
                    otro2.setVisibility(view.GONE);
                    otro2.setText("");
                }
            }
        });



        return view;
    }
}

