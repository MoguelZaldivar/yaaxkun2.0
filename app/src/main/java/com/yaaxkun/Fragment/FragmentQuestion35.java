package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion35 extends Fragment {
    private RatingBar rBar1, rBar2, rBar3, rBar4, rBar5, rBar6;
    private int factor_1=0,factor_2=0,factor_3=0,factor_4=0,factor_5=0,factor_6=0;
    CheckBox chk1, chk2, chk3, chk4, chk5, chk6;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question35, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor35);
        Button next = (Button) view.findViewById(R.id.nextFactor35);
        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor35);
        rBar1 = (RatingBar) view.findViewById(R.id.ratingFactor35_2);
        rBar2 = (RatingBar) view.findViewById(R.id.ratingFactor35_3);
        rBar3 = (RatingBar) view.findViewById(R.id.ratingFactor35_4);
        rBar4 = (RatingBar) view.findViewById(R.id.ratingFactor35_5);
        rBar5 = (RatingBar) view.findViewById(R.id.ratingFactor35_6);
        rBar6 = (RatingBar) view.findViewById(R.id.ratingFactor35_7);
//        chk1 = (CheckBox) view.findViewById(R.id.cBNA35_1);
//        chk2 = (CheckBox) view.findViewById(R.id.cBNA35_2);
//        chk3 = (CheckBox) view.findViewById(R.id.cBNA35_3);
        chk4 = (CheckBox) view.findViewById(R.id.cBNA35_4);
//        chk5 = (CheckBox) view.findViewById(R.id.cBNA35_5);
//        chk6 = (CheckBox) view.findViewById(R.id.cBNA35_6);
        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(613);
                ((SurveyActivity)getActivity()).borrarItem(612);
                ((SurveyActivity)getActivity()).borrarItem(611);
                ((SurveyActivity)getActivity()).borrarItem(610);
                ((SurveyActivity)getActivity()).borrarItem(609);
                ((SurveyActivity)getActivity()).borrarItem(608);

                ((SurveyActivity) getActivity()).selectFragment(25);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();

                if(factor_1==0 || factor_2==0 || factor_3==0 ||factor_4==0 || factor_5==0 || factor_6==0){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    respuesta1.add(String.valueOf(factor_1));
                    ((SurveyActivity)getActivity()).crearItem(614, "numeric", respuesta1);
                    respuesta2.add(String.valueOf(factor_2));
                    ((SurveyActivity)getActivity()).crearItem(615, "numeric", respuesta2);
                    respuesta3.add(String.valueOf(factor_3));
                    ((SurveyActivity)getActivity()).crearItem(616, "numeric", respuesta3);
                    respuesta4.add(String.valueOf(factor_4));
                    ((SurveyActivity)getActivity()).crearItem(617, "numeric", respuesta4);
                    respuesta5.add(String.valueOf(factor_5));
                    ((SurveyActivity)getActivity()).crearItem(618, "numeric", respuesta5);
                    respuesta6.add(String.valueOf(factor_6));
                    ((SurveyActivity)getActivity()).crearItem(619, "numeric", respuesta6);

                    ((SurveyActivity) getActivity()).selectFragment(27);
                }


            }
        });

        rBar1.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override public void onRatingChanged(RatingBar ratingBar, float rating,
                                                  boolean fromUser) {
                if(rating<1.0f)
                    ratingBar.setRating(1.0f);
                factor_1 = (int) rBar1.getRating();
            }
        });
        rBar2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override public void onRatingChanged(RatingBar ratingBar, float rating,
                                                  boolean fromUser) {
                if(rating<1.0f)
                    ratingBar.setRating(1.0f);
                factor_2 = (int) rBar2.getRating();
            }
        });
        rBar3.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override public void onRatingChanged(RatingBar ratingBar, float rating,
                                                  boolean fromUser) {
                if(rating<1.0f)
                    ratingBar.setRating(1.0f);
                factor_3 = (int) rBar3.getRating();
            }
        });
        rBar4.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override public void onRatingChanged(RatingBar ratingBar, float rating,
                                                  boolean fromUser) {
                if(rating<1.0f)
                    ratingBar.setRating(1.0f);
                factor_4 = (int) rBar4.getRating();
            }
        });
        rBar5.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override public void onRatingChanged(RatingBar ratingBar, float rating,
                                                  boolean fromUser) {
                if(rating<1.0f)
                    ratingBar.setRating(1.0f);
                factor_5 = (int) rBar5.getRating();
            }
        });
        rBar6.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override public void onRatingChanged(RatingBar ratingBar, float rating,
                                                  boolean fromUser) {
                if(rating<1.0f)
                    ratingBar.setRating(1.0f);
                factor_6 = (int) rBar6.getRating();
            }
        });
//        chk1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean checked = ((CheckBox) v).isChecked();
//                // Check which checkbox was clicked
//                if (checked){
//                    // Do your coding
//                    factor_1 = -1;
//                    rBar1.setEnabled(false);
////                    rBar1.setRating(0);
//
//                }
//                else{
//                    // Do your coding
//                    factor_1 = (int) rBar1.getRating();
//                    rBar1.setEnabled(true);
//                }
//            }
//        });
//        chk2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean checked = ((CheckBox) v).isChecked();
//                // Check which checkbox was clicked
//                if (checked){
//                    // Do your coding
//                    factor_2 = -1;
//                    rBar2.setEnabled(false);
////                    rBar1.setRating(0);
//
//                }
//                else{
//                    // Do your coding
//                    factor_2 = (int) rBar2.getRating();
//                    rBar2.setEnabled(true);
//                }
//            }
//        });
//        chk3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean checked = ((CheckBox) v).isChecked();
//                // Check which checkbox was clicked
//                if (checked){
//                    // Do your coding
//                    factor_3 = -1;
//                    rBar3.setEnabled(false);
////                    rBar1.setRating(0);
//
//                }
//                else{
//                    // Do your coding
//                    factor_3 = (int) rBar3.getRating();
//                    rBar3.setEnabled(true);
//                }
//            }
//        });
        chk4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((CheckBox) v).isChecked();
                // Check which checkbox was clicked
                if (checked){
                    // Do your coding
                    factor_4 = -1;
                    rBar4.setEnabled(false);
//                    rBar1.setRating(0);

                }
                else{
                    // Do your coding
                    factor_4 = (int) rBar4.getRating();
                    rBar4.setEnabled(true);
                }
            }
        });
//        chk5.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean checked = ((CheckBox) v).isChecked();
//                // Check which checkbox was clicked
//                if (checked){
//                    // Do your coding
//                    factor_5 = -1;
//                    rBar5.setEnabled(false);
////                    rBar1.setRating(0);
//
//                }
//                else{
//                    // Do your coding
//                    factor_5 = (int) rBar5.getRating();
//                    rBar5.setEnabled(true);
//                }
//            }
//        });
//        chk6.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean checked = ((CheckBox) v).isChecked();
//                // Check which checkbox was clicked
//                if (checked){
//                    // Do your coding
//                    factor_6 = -1;
//                    rBar6.setEnabled(false);
//
//                }
//                else{
//                    // Do your coding
//                    factor_6 = (int) rBar6.getRating();
//                    rBar6.setEnabled(true);
//                }
//            }
//        });

        return view;
    }
}