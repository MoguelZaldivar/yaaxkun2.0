package com.yaaxkun.Fragment;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion16 extends Fragment {
    EditText tOtros;
    private int itemID = 409, responseID = 463, factor_nInundacion;
    private String factor_1=null, factor_2=null, factor_3=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkPrueba=false, checkPrueba2=false;
    EditText nInundacion, inundacion1, inundacion2;

//    CheckBox chkR3_1, chkR3_2, chkR3_3, chkR3_4, chkR3_5, chkR3_6, chkR3_7;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.fragment_question16, container, false);
        final ScrollView scroll = (ScrollView) view.findViewById(R.id.SV1);
        final ConstraintLayout conLay2 = (ConstraintLayout) view.findViewById(R.id.constraintLayout2);



        Button next = (Button) view.findViewById(R.id.bNext);
        Button back = (Button) view.findViewById(R.id.bBack);
        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.rG1);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.rG2);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.rG3);

//        RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.rG6);
        nInundacion = (EditText) view.findViewById(R.id.inundacionNumeroText);
        inundacion1 = (EditText) view.findViewById(R.id.inundacion1Text);
        inundacion2 = (EditText) view.findViewById(R.id.inundacion2Text);

//        RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.rG7);
//        final EditText tOtros = (EditText)view.findViewById(R.id.eTOtros9);
        scroll.fullScroll(ScrollView.FOCUS_UP);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String factor_7 = tOtros.getText().toString().toUpperCase();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
                List<String> respuesta7 = new ArrayList<String>();
//                List<String> respuesta8 = new ArrayList<String>();
//                List<String> respuesta9 = new ArrayList<String>();
//                List<String> respuesta10 = new ArrayList<String>();
//                List<String> respuesta11 = new ArrayList<String>();




//                List<String> respuesta7 = new ArrayList<String>();
//                ((SurveyActivity)getActivity()).hideKeyboard();
//                if ( nInundacion.getText().toString().length() > 0 ){factor_nInundacion = Integer.parseInt(nInundacion.getText().toString());}

                String factor_inundacion1 = inundacion1.getText().toString().toUpperCase();
                String factor_inundacion2 = inundacion2.getText().toString().toUpperCase();

                boolean pass1 = false, pass2 = false;
                if(checkFactor_2){if(factor_2.equals(Integer.toString(responseID+6)) && !factor_inundacion1.equals("")){checkPrueba = true;}}

                if(checkFactor_3){if(factor_3.equals(Integer.toString(responseID+12)) && ( !factor_inundacion2.equals("") )){checkPrueba2 = true;}}

                if(!checkFactor_1 || nInundacion.getText().toString().equals("") ){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    if ( nInundacion.getText().toString().length() > 0 ){factor_nInundacion = Integer.parseInt(nInundacion.getText().toString());}
                    if (factor_nInundacion == 0 || factor_nInundacion > 0 && (checkPrueba && checkPrueba2)) {


                        respuesta1.add(String.valueOf(factor_nInundacion));
                        ((SurveyActivity) getActivity()).crearItem(itemID, "numeric", respuesta1);

//                        respuesta2.add(null);
//                        ((SurveyActivity) getActivity()).crearItem(itemID + 1, "simple", respuesta2);

                        respuesta2.add(factor_1);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 1, "simple", respuesta2);

                        respuesta3.add(factor_2);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 2, "simple", respuesta3);

                        respuesta4.add(factor_inundacion1);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 3, "text", respuesta4);

                        respuesta5.add(factor_3);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 4, "simple", respuesta5);

                        respuesta6.add(factor_inundacion2);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 5, "text", respuesta6);

                        ((SurveyActivity)getActivity()).hideKeyboard(v);
                        ((SurveyActivity) getActivity()).selectFragment(17);
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
                ((SurveyActivity)getActivity()).borrarItem(itemID-3);
                ((SurveyActivity)getActivity()).borrarItem(itemID-4);
                ((SurveyActivity)getActivity()).borrarItem(itemID-5);
                ((SurveyActivity)getActivity()).borrarItem(itemID-6);
                ((SurveyActivity)getActivity()).borrarItem(itemID-7);
                ((SurveyActivity)getActivity()).borrarItem(itemID-8);
                ((SurveyActivity)getActivity()).borrarItem(itemID-9);
                ((SurveyActivity)getActivity()).borrarItem(itemID-10);
                ((SurveyActivity)getActivity()).borrarItem(itemID-11);
                ((SurveyActivity)getActivity()).hideKeyboard(v);
//                ((SurveyActivity)getActivity()).hideKeyboard();
                ((SurveyActivity)getActivity()).selectFragment(15);
            }
        });

        conLay2.setVisibility(view.GONE);
        inundacion1.setVisibility(View.GONE);
        inundacion2.setVisibility(View.GONE);

        nInundacion.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (nInundacion.getText().toString().length() > 0) {
                    if (Integer.parseInt(nInundacion.getText().toString()) > 0) {
                        conLay2.setVisibility(View.VISIBLE);
                    } else {
                        inundacion1.setVisibility(View.GONE);
                        inundacion2.setVisibility(View.GONE);
                        conLay2.setVisibility(View.GONE);

                        GFactor2.clearCheck();
                        GFactor3.clearCheck();
                        factor_2 = null;
                        factor_3 = null;
                        checkFactor_2 = false;
                        checkFactor_3 = false;
                        checkPrueba = false;
                        checkPrueba2 = false;
                        inundacion1.setText("");
                        inundacion2.setText("");

                    }
                } else{
                    inundacion1.setVisibility(View.GONE);
                    inundacion2.setVisibility(View.GONE);
                    conLay2.setVisibility(View.GONE);
//                    inundacion1.setVisibility(View.GONE);
//                    inundacion2.setVisibility(View.GONE);
                    GFactor2.clearCheck();
                    GFactor3.clearCheck();
                    factor_2 = null;
                    factor_3 = null;
                    checkFactor_2 = false;
                    checkFactor_3 = false;
                    checkPrueba = false;
                    checkPrueba2 = false;
                    inundacion1.setText("");
                    inundacion2.setText("");

                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (nInundacion.getText().toString().length() > 0) {
                    if (Integer.parseInt(nInundacion.getText().toString()) > 0) {
                        conLay2.setVisibility(View.VISIBLE);
                    } else {
                        inundacion1.setVisibility(View.GONE);
                        inundacion2.setVisibility(View.GONE);
                        conLay2.setVisibility(View.GONE);

                        GFactor2.clearCheck();
                        GFactor3.clearCheck();
                        factor_2 = null;
                        factor_3 = null;
                        checkFactor_2 = false;
                        checkFactor_3 = false;
                        checkPrueba = false;
                        checkPrueba2 = false;
                        inundacion1.setText("");
                        inundacion2.setText("");

                    }
                } else{
                    inundacion1.setVisibility(View.GONE);
                    inundacion2.setVisibility(View.GONE);
                    conLay2.setVisibility(View.GONE);
//                    inundacion1.setVisibility(View.GONE);
//                    inundacion2.setVisibility(View.GONE);
                    GFactor2.clearCheck();
                    GFactor3.clearCheck();
                    factor_2 = null;
                    factor_3 = null;
                    checkFactor_2 = false;
                    checkFactor_3 = false;
                    checkPrueba = false;
                    checkPrueba2 = false;
                    inundacion1.setText("");
                    inundacion2.setText("");

                }
            }
        });

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.rdb1Si:
                        factor_1 = Integer.toString(responseID);

                        break;
                    case R.id.rdb1No:

                        factor_1 = Integer.toString(responseID+1);

                        break;

                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.rdb2_1:
                        factor_2 = Integer.toString(responseID+2);
                        inundacion1.setVisibility(View.GONE);
                        inundacion1.setText("");
                        checkPrueba = true;
                        break;
                    case R.id.rdb2_2:
                        factor_2 = Integer.toString(responseID+3);
                        inundacion1.setVisibility(View.GONE);
                        inundacion1.setText("");
                        checkPrueba = true;
                        break;
                    case R.id.rdb2_3:
                        factor_2 = Integer.toString(responseID+4);
                        inundacion1.setVisibility(View.GONE);
                        inundacion1.setText("");
                        checkPrueba = true;
                        break;
                    case R.id.rdb2_4:
                        factor_2 = Integer.toString(responseID+5);
                        inundacion1.setVisibility(View.GONE);
                        inundacion1.setText("");
                        checkPrueba = true;
                        break;
                    case R.id.rdb2_5:
                        factor_2 = Integer.toString(responseID+6);
                        inundacion1.setVisibility(View.VISIBLE);
                        checkPrueba = false;

                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.rdb3_1:
                        factor_3 = Integer.toString(responseID+7);
                        inundacion2.setVisibility(View.GONE);
                        inundacion2.setText("");
                        checkPrueba2 = true;
                        break;
                    case R.id.rdb3_2:
                        factor_3 = Integer.toString(responseID+8);
                        inundacion2.setVisibility(View.GONE);
                        inundacion2.setText("");
                        checkPrueba2 = true;
                        break;
                    case R.id.rdb3_3:
                        factor_3 = Integer.toString(responseID+9);
                        inundacion2.setVisibility(View.GONE);
                        inundacion2.setText("");
                        checkPrueba2 = true;
                        break;
                    case R.id.rdb3_4:
                        factor_3 = Integer.toString(responseID+10);
                        inundacion2.setVisibility(View.GONE);
                        inundacion2.setText("");
                        checkPrueba2 = true;
                        break;
                    case R.id.rdb3_5:
                        factor_3 = Integer.toString(responseID+11);
                        inundacion2.setVisibility(View.GONE);
                        inundacion2.setText("");
                        checkPrueba2 = true;
                        break;
                    case R.id.rdb3_6:
                        factor_3 = Integer.toString(responseID+12);
                        inundacion2.setVisibility(View.VISIBLE);
                        inundacion2.setText("");
                        checkPrueba2 = false;
                        break;
                }
            }
        });






        return view;
    }
}
