package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.annotation.RestrictTo;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion21 extends Fragment {
    private String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null, factor_6=null, factor_7=null,factor_8=null, factor_9=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false, checkFactor_6 = false, checkFactor_7 = false, checkFactor_8 = false, checkFactor_9 = false;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question21, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor21);
        Button next = (Button) view.findViewById(R.id.nextFactor21);
        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor21);


        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor21_2);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor21_3);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor21_4);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor21_5);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor21_6);
        final RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.groupFactor21_7);
        final RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.groupFactor21_8);
        final RadioGroup GFactor8 = (RadioGroup) view.findViewById(R.id.groupFactor21_9);
//        final RadioGroup GFactor9 = (RadioGroup) view.findViewById(R.id.groupFactor21_10);

        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((SurveyActivity)getActivity()).borrarItem(86);
                ((SurveyActivity)getActivity()).borrarItem(501);
                ((SurveyActivity)getActivity()).borrarItem(500);
                ((SurveyActivity)getActivity()).borrarItem(499);
                ((SurveyActivity)getActivity()).borrarItem(498);
                ((SurveyActivity)getActivity()).borrarItem(497);

                ((SurveyActivity)getActivity()).selectFragment(10);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
                List<String> respuesta7 = new ArrayList<String>();
                List<String> respuesta8 = new ArrayList<String>();
//                List<String> respuesta9 = new ArrayList<String>();

                if(!checkFactor_1|| !checkFactor_2 || !checkFactor_3 || !checkFactor_4 || !checkFactor_5 || !checkFactor_6 || !checkFactor_7 || !checkFactor_8){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(502, "simple", respuesta1);
                    respuesta2.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(503, "simple", respuesta2);
                    respuesta3.add(factor_3);
                    ((SurveyActivity)getActivity()).crearItem(504, "simple", respuesta3);
                    respuesta4.add(factor_4);
                    ((SurveyActivity)getActivity()).crearItem(505, "simple", respuesta4);
                    respuesta5.add(factor_5);
                    ((SurveyActivity)getActivity()).crearItem(506, "simple", respuesta5);
                    respuesta6.add(factor_6);
                    ((SurveyActivity)getActivity()).crearItem(507, "simple", respuesta6);
                    respuesta7.add(factor_7);
                    ((SurveyActivity)getActivity()).crearItem(508, "simple", respuesta7);
                    respuesta8.add(factor_8);
                    ((SurveyActivity)getActivity()).crearItem(509, "simple", respuesta8);
//                    respuesta9.add(factor_9);
//                    ((SurveyActivity)getActivity()).crearItem(95, "simple", respuesta9);

                    ((SurveyActivity)getActivity()).selectFragment(12);
                }

            }
        });
//        viewScroll.setVisibility(View.GONE);
//        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
//        {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId)
//            {
//                checkFactor_1 = true;
//                switch(checkedId)
//                {
//                    case R.id.radioFactor21_2_1:
//                        factor_1 = "207";
//
//                        viewScroll.setVisibility(View.VISIBLE);
//                        break;
//                    case R.id.radioFactor21_2_2:
//                        factor_1 = "208";
//                        factor_2 = "212";
//                        factor_3 = "215";
//                        factor_4 = "218";
//                        factor_5 = "221";
//                        factor_6 = "224";
//                        factor_7 = "227";
////                        GFactor1.clearCheck();
//                        GFactor2.clearCheck();
//                        GFactor3.clearCheck();
//                        GFactor4.clearCheck();
//                        GFactor5.clearCheck();
//                        GFactor6.clearCheck();
//                        GFactor7.clearCheck();
//                        viewScroll.setVisibility(View.GONE);
//                        break;
//                    case R.id.radioFactor21_2_3:
//                        viewScroll.setVisibility(View.GONE);
//                        factor_1 = "209";
//                        factor_2 = "212";
//                        factor_3 = "215";
//                        factor_4 = "218";
//                        factor_5 = "221";
//                        factor_6 = "224";
//                        factor_7 = "227";
////                        GFactor1.clearCheck();
//                        GFactor2.clearCheck();
//                        GFactor3.clearCheck();
//                        GFactor4.clearCheck();
//                        GFactor5.clearCheck();
//                        GFactor6.clearCheck();
//                        GFactor7.clearCheck();
//                        break;
//                }
//            }
//        });
        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor21_2_1:
                        factor_1 = "956";
                        break;
                    case R.id.radioFactor21_2_2:
                        factor_1 = "957";
                        break;
//                    case R.id.radioFactor21_2_3:
//                        factor_1 = "209";
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor21_3_1:
                        factor_2 = "958";
                        break;
                    case R.id.radioFactor21_3_2:
                        factor_2 = "959";
                        break;
//                    case R.id.radioFactor21_3_3:
//                        factor_2 = "212";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor21_4_1:
                        factor_3 = "960";
                        break;
                    case R.id.radioFactor21_4_2:
                        factor_3 = "961";
                        break;
//                    case R.id.radioFactor21_4_3:
//                        factor_3 = "215";
//                        break;
                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor21_5_1:
                        factor_4 = "962";
                        break;
                    case R.id.radioFactor21_5_2:
                        factor_4 = "963";
                        break;
//                    case R.id.radioFactor21_5_3:
//                        factor_4 = "218";
//                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor21_6_1:
                        factor_5 = "964";
                        break;
                    case R.id.radioFactor21_6_2:
                        factor_5 = "965";
                        break;
//                    case R.id.radioFactor21_6_3:
//                        factor_5 = "221";
//                        break;
                }
            }
        });
        GFactor6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_6 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor21_7_1:
                        factor_6 = "966";
                        break;
                    case R.id.radioFactor21_7_2:
                        factor_6 = "967";
                        break;
//                    case R.id.radioFactor21_7_3:
//                        factor_6 = "224";
//                        break;
                }
            }
        });
        GFactor7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_7 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor21_8_1:
                        factor_7 = "968";
                        break;
                    case R.id.radioFactor21_8_2:
                        factor_7 = "969";
                        break;
//                    case R.id.radioFactor21_8_3:
//                        factor_7 = "227";
//                        break;
                }
            }
        });
        GFactor8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_8 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor21_9_1:
                        factor_8 = "970";
                        break;
                    case R.id.radioFactor21_9_2:
                        factor_8 = "971";
                        break;
//                    case R.id.radioFactor21_9_3:
//                        factor_8 = "227";
//                        break;
                }
            }
        });



        return view;
    }
}
