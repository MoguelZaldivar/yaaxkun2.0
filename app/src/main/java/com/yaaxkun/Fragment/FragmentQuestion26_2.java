package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion26_2 extends Fragment {
    private String factor_1=null;
    private boolean checkFactor_1 = false;
    private int itemID = 322;
    private RadioGroup GFactor1;
    private EditText edad, calle, nExt, nInt, colonia, mzna, sMzna, cPost, adicional;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.fragment_question26_2, container, false);
        final ScrollView scroll = (ScrollView) view.findViewById(R.id.scrollFactor_1);
        final Button next = (Button) view.findViewById(R.id.bNext);
//        Button back = (Button) view.findViewById(R.id.bBack);
        GFactor1 = (RadioGroup) view.findViewById(R.id.rG1);
//        RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.rG2);
//        RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.rG3);
//        RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.rG4);
//        RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.rG5);
//        RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.rG6);
//        RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.rG7);
        edad = (EditText)view.findViewById(R.id.edadText);
        calle = (EditText)view.findViewById(R.id.calleText);
        nExt = (EditText)view.findViewById(R.id.nExtText);
        nInt = (EditText)view.findViewById(R.id.nIntText);
        colonia = (EditText)view.findViewById(R.id.coloniaText);
        mzna = (EditText)view.findViewById(R.id.mznaText);
        sMzna = (EditText)view.findViewById(R.id.sMznaText);
        cPost = (EditText)view.findViewById(R.id.cPText);
        adicional = (EditText)view.findViewById(R.id.aditionalText);


        scroll.fullScroll(ScrollView.FOCUS_UP);
//        next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                if (checkFactor_1 && checkFactor_2 && checkFactor_3 && checkFactor_4 && checkFactor_5 && checkFactor_6 && checkFactor_7) {
//                    Log.d("mensaje", "Respuestas 11:\n11.1: " +factor_1 + "\n11.2: " + factor_2+ "\n11.3: " + factor_3+ "\n11.4: " + factor_4+ "\n11.5: " + factor_5+ "\n11.6: " + factor_6+ "\n11.7: " + factor_7);
//                    ((SurveyActivity)getActivity()).selectFragment(11);
//                } else {
//                    Toast.makeText(getActivity().getApplicationContext(), "Please Fill All the Details", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String factor_2 = edad.getText().toString().toUpperCase();
                String factor_3 = calle.getText().toString().toUpperCase();
                String factor_4 = nExt.getText().toString().toUpperCase();
                String factor_5 = nInt.getText().toString().toUpperCase();
                String factor_6 = colonia.getText().toString().toUpperCase();
                String factor_7 = mzna.getText().toString().toUpperCase();
                String factor_8 = sMzna.getText().toString().toUpperCase();
                String factor_9 = cPost.getText().toString().toUpperCase();
                String factor_10 = adicional.getText().toString().toUpperCase();

                Log.d("mensaje", "edad: "+ edad.getText().toString().length());

                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
                List<String> respuesta7 = new ArrayList<String>();
                List<String> respuesta8 = new ArrayList<String>();
                List<String> respuesta9 = new ArrayList<String>();
                List<String> respuesta10 = new ArrayList<String>();
                List<String> respuesta11 = new ArrayList<String>();
//                ((SurveyActivity)getActivity()).hideKeyboard();
//
                if(!checkFactor_1 || factor_2.equals("")){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                    validate();
                }else {
                    if(Integer.parseInt(factor_2) > 110){
                        Toast.makeText(getActivity().getApplicationContext(), "Favor de introducir una edad valida" ,Toast.LENGTH_SHORT).show();
                    }else if(Integer.parseInt(factor_2) < 18){
                        Toast.makeText(getActivity().getApplicationContext(), "El encuestado debe ser mayor de edad" ,Toast.LENGTH_SHORT).show();
                    }
                    else{
                        respuesta1.add(factor_1);
                        ((SurveyActivity)getActivity()).crearItem(itemID, "simple", respuesta1);
                        //respuesta.clear();
                        respuesta2.add(factor_2);
                        ((SurveyActivity)getActivity()).crearItem(itemID+1, "text", respuesta2);
                        //respuesta.clear();
                        respuesta3.add(factor_3);
                        ((SurveyActivity)getActivity()).crearItem(itemID+2, "text", respuesta3);
                        //respuesta.clear();
                        respuesta4.add(factor_4);
                        ((SurveyActivity)getActivity()).crearItem(itemID+3, "text", respuesta4);
                        //respuesta.clear();
                        respuesta5.add(factor_5);
                        ((SurveyActivity)getActivity()).crearItem(itemID+4, "text", respuesta5);
                        //respuesta.clear();
//                    respuesta6.add("");
//                    ((SurveyActivity)getActivity()).crearItem(itemID+5, "text", respuesta6);
                        //
                        respuesta6.add(factor_7);
                        ((SurveyActivity)getActivity()).crearItem(itemID+5, "text", respuesta6);
                        //respuesta.clear();
                        respuesta7.add(factor_8);
                        ((SurveyActivity)getActivity()).crearItem(itemID+6, "text", respuesta7);
                        //respuesta.clear();
                        respuesta8.add(factor_6);
                        ((SurveyActivity)getActivity()).crearItem(itemID+7, "text", respuesta8); //Corregir
                        //respuesta.clear();
                        respuesta9.add(factor_9);
                        ((SurveyActivity)getActivity()).crearItem(itemID+8, "text", respuesta9);
                        //respuesta.clear();
                        respuesta10.add(factor_10);
                        ((SurveyActivity)getActivity()).crearItem(itemID+9, "text", respuesta10);
                        //respuesta.clear();
//                    respuesta6.add(factor_6);
//                    ((SurveyActivity)getActivity()).crearItem(231, "simple", respuesta6);
                        //respuesta.clear();
//                    respuesta7.add(factor_7);
//                    ((SurveyActivity)getActivity()).crearItem(232, "text", respuesta7);
//                    Log.d("mensaje", "onClick: -"+ tOtros.getText().toString().toUpperCase()+"--");
//                    ((SurveyActivity)getActivity()).hideKeyboard();

                        ((SurveyActivity)getActivity()).selectFragment(1);
                    }

                }

            }
        });
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((SurveyActivity)getActivity()).borrarItem(225);
//                ((SurveyActivity)getActivity()).borrarItem(224);
//                ((SurveyActivity)getActivity()).borrarItem(223);
//                ((SurveyActivity)getActivity()).borrarItem(222);
//                ((SurveyActivity)getActivity()).borrarItem(221);
//                ((SurveyActivity)getActivity()).borrarItem(220);
//                ((SurveyActivity)getActivity()).borrarItem(219);
//                ((SurveyActivity)getActivity()).borrarItem(218);
//                ((SurveyActivity)getActivity()).borrarItem(217);
//                ((SurveyActivity)getActivity()).borrarItem(216);
//                ((SurveyActivity)getActivity()).hideKeyboard();
//                ((SurveyActivity)getActivity()).selectFragment(0);
//            }
//        });
        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.rdbHombre:
                        factor_1 = "248";
                        Log.d("mensaje", "factor 1: "+ factor_1);
                        break;
                    case R.id.rdbMujer:
                        factor_1 = "249";
                        Log.d("mensaje", "factor 1: "+ factor_1);
                        break;
                }
            }
        });



        return view;
    }

    private void validate(){
        Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);

        if (!checkFactor_1) {
//            Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);
            GFactor1.setFocusableInTouchMode(true);
            GFactor1.startAnimation(shake);
            GFactor1.requestFocusFromTouch();
            GFactor1.clearFocus();

        }else if (edad.getText().toString().equals("")) {
//            Animation shake = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.shake);
            edad.setFocusableInTouchMode(true);
            edad.startAnimation(shake);
            edad.requestFocusFromTouch();
            edad.clearFocus();
        }


//
//        GSex.setFocusableInTouchMode(false);
        GFactor1.setFocusableInTouchMode(false);
//        edad.setFocusableInTouchMode(false);


    }
}
