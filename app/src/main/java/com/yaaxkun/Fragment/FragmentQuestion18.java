package com.yaaxkun.Fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.MainActivity;
import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion18 extends Fragment {
    private int itemID = 419;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question18, container, false);

        Button prev = (Button) view.findViewById(R.id.prevSurveyS);
        Button next = (Button) view.findViewById(R.id.nextSurveyS);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
                ((SurveyActivity)getActivity()).borrarItem(itemID-3);
                ((SurveyActivity)getActivity()).borrarItem(itemID-4);


                ((SurveyActivity) getActivity()).selectFragment(17);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((SurveyActivity) getActivity()).selectFragment(19);
            }
        });

        return view;
    }
}
