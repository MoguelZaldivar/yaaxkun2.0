package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion22 extends Fragment {
    private String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false, checkFactor_6 = false;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question22, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor22);
        Button next = (Button) view.findViewById(R.id.nextFactor22);
        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor22);


        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor22_1);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor22_2);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor22_3);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor22_4);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor22_5);
//        final RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.groupFactor22_6);
//        final EditText tOtros = (EditText)view.findViewById(R.id.eTOtros22);

        viewScroll.fullScroll(ScrollView.FOCUS_UP);


        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((SurveyActivity)getActivity()).borrarItem(96);
                ((SurveyActivity)getActivity()).borrarItem(509);
                ((SurveyActivity)getActivity()).borrarItem(508);
                ((SurveyActivity)getActivity()).borrarItem(507);
                ((SurveyActivity)getActivity()).borrarItem(506);
                ((SurveyActivity)getActivity()).borrarItem(505);
                ((SurveyActivity)getActivity()).borrarItem(504);
                ((SurveyActivity)getActivity()).borrarItem(503);
                ((SurveyActivity)getActivity()).borrarItem(502);
//                ((SurveyActivity)getActivity()).hideKeyboard();
                ((SurveyActivity)getActivity()).selectFragment(11);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String factor_6 = tOtros.getText().toString().toUpperCase();
//                List<String> respuesta = new ArrayList<String>();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
//                List<String> respuesta6 = new ArrayList<String>();
//                ((SurveyActivity)getActivity()).hideKeyboard();

                if(!checkFactor_1){
                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else{
                    if((factor_1 == "972" && (checkFactor_2 && checkFactor_3 && checkFactor_4 && checkFactor_5))|| factor_1=="973"){
                        respuesta1.add(factor_1);
                        ((SurveyActivity)getActivity()).crearItem(510, "simple", respuesta1);
                        respuesta2.add(factor_2);
                        ((SurveyActivity)getActivity()).crearItem(511, "simple", respuesta2);
                        respuesta3.add(factor_3);
                        ((SurveyActivity)getActivity()).crearItem(512, "simple", respuesta3);
                        respuesta4.add(factor_4);
                        ((SurveyActivity)getActivity()).crearItem(513, "simple", respuesta4);
                        respuesta5.add(factor_5);
                        ((SurveyActivity)getActivity()).crearItem(514, "simple", respuesta5);
//                        respuesta6.add(factor_6);
//                        ((SurveyActivity)getActivity()).crearItem(315, "text", respuesta6);
//                        Log.d("mensaje", "onClick: -"+ tOtros.getText().toString().toUpperCase()+"--");
//                        ((SurveyActivity)getActivity()).hideKeyboard();

                        ((SurveyActivity)getActivity()).selectFragment(13);
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor22_1_1:
                        factor_1 = "972";

                        viewScroll.setVisibility(View.VISIBLE);
                        viewScroll.fullScroll(ScrollView.FOCUS_UP);
                        break;
                    case R.id.radioFactor22_1_2:
                        GFactor2.clearCheck();
                        GFactor3.clearCheck();
                        GFactor4.clearCheck();
                        GFactor5.clearCheck();
//                        GFactor6.clearCheck();
                        factor_1 = "973";
                        factor_2 = null;
                        factor_3 = null;
                        factor_4 = null;
                        factor_5 = null;
//                        factor_6 = null;
                        checkFactor_2 = false;
                        checkFactor_3 = false;
                        checkFactor_4 = false;
                        checkFactor_5 = false;
//                        checkFactor_6 = false;
//                        GFactor1.clearCheck();


                        viewScroll.setVisibility(View.GONE);
                        break;
//                    case R.id.radioFactor22_1_3:
//                    GFactor2.clearCheck();
//                    GFactor3.clearCheck();
//                    GFactor4.clearCheck();
//                    GFactor5.clearCheck();
//                    GFactor6.clearCheck();
//                        viewScroll.setVisibility(View.GONE);
//                        factor_1 = "230";
//                        factor_2 = "233";
//                        factor_3 = "236";
//                        factor_4 = "239";
//                        factor_5 = "242";
//                        factor_6 = "245";
////                        GFactor1.clearCheck();
//
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor22_2_1:
                        factor_2 = "974";
                        break;
                    case R.id.radioFactor22_2_2:
                        factor_2 = "975";
                        break;
//                    case R.id.radioFactor22_2_3:
//                        factor_2 = "233";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor22_3_1:
                        factor_3 = "976";
                        break;
                    case R.id.radioFactor22_3_2:
                        factor_3 = "977";
                        break;
//                    case R.id.radioFactor22_3_3:
//                        factor_3 = "236";
//                        break;
                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor22_4_1:
                        factor_4 = "978";
                        break;
                    case R.id.radioFactor22_4_2:
                        factor_4 = "979";
                        break;
//                    case R.id.radioFactor22_4_3:
//                        factor_4 = "239";
//                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor22_5_1:
                        factor_5 = "980";
                        break;
                    case R.id.radioFactor22_5_2:
                        factor_5 = "981";
                        break;
//                    case R.id.radioFactor22_5_3:
//                        factor_5 = "242";
//                        break;
                }
            }
        });

        return view;
    }
}
