package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion6 extends Fragment {
    private int itemID = 353, responseID = 299;
    private String factor_1=null, factor_2=null;
    boolean checkFactor_1 = false, checkFactor_2 = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question6, container, false);
        Button next = (Button) view.findViewById(R.id.bNext);
        Button back = (Button) view.findViewById(R.id.bBack);

        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.rG1);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.rG2);

        final ScrollView scroll = (ScrollView) view.findViewById(R.id.SV1);
//        final TextView text = (TextView) view.findViewById(R.id.textViewF4);


        scroll.fullScroll(ScrollView.FOCUS_UP);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();

                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();


                if(!checkFactor_1 || !checkFactor_2){
                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else{
//                    if(checkFactor_1 && checkFactor_2){
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(itemID, "simple", respuesta1);
                    respuesta2.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(itemID+1, "simple", respuesta2);


                    ((SurveyActivity)getActivity()).selectFragment(7);
//                    } else {
//                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
//                    }
                }


            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
                ((SurveyActivity)getActivity()).borrarItem(itemID-3);

                ((SurveyActivity)getActivity()).selectFragment(5);
            }
        });


        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.rdb1_1:
                        factor_1 = Integer.toString(responseID);
                        break;
                    case R.id.rdb1_2:
                        factor_1 = Integer.toString(responseID+1);
                        break;
                    case R.id.rdb1_3:
                        factor_1 = Integer.toString(responseID+2);
                        break;
                    case R.id.rdb1_4:
                        factor_1 = Integer.toString(responseID+3);
                        break;
                    case R.id.rdb1_5:
                        factor_1 = Integer.toString(responseID+4);
                        break;
                    case R.id.rdb1_6:
                        factor_1 = Integer.toString(responseID+5);
                        break;
                    case R.id.rdb1_7:
                        factor_1 = Integer.toString(responseID+6);
                        break;
                    case R.id.rdb1_8:
                        factor_1 = Integer.toString(responseID+7);
                        break;
                    case R.id.rdb1_9:
                        factor_1 = Integer.toString(responseID+8);
                        break;

                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.rdb2_1:
                        factor_2 = Integer.toString(responseID+9);
                        break;
                    case R.id.rdb2_2:
                        factor_2 = Integer.toString(responseID+10);
                        break;
                    case R.id.rdb2_3:
                        factor_2 = Integer.toString(responseID+11);
                        break;
                    case R.id.rdb2_4:
                        factor_2 = Integer.toString(responseID+12);
                        break;
                    case R.id.rdb2_5:
                        factor_2 = Integer.toString(responseID+13);
                        break;
                    case R.id.rdb2_6:
                        factor_2 = Integer.toString(responseID+14);
                        break;

                }
            }
        });

        return view;
    }
}
