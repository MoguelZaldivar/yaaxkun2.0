package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion33 extends Fragment {
    EditText tOtros;
    String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null, factor_6=null, factor_7=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false, checkFactor_6 = false, checkFactor_7 = true;



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question33, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor33);
        Button next = (Button) view.findViewById(R.id.nextFactor33);

        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor33);
        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor33_2);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor33_3);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor33_4);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor33_5);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor33_6);
        final RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.groupFactor33_7);
//        final RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.groupFactor33_8);
//        tOtros = (EditText)view.findViewById(R.id.eTOtros);
        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(601);
                ((SurveyActivity)getActivity()).borrarItem(600);
                ((SurveyActivity)getActivity()).borrarItem(599);
                ((SurveyActivity)getActivity()).borrarItem(598);
                ((SurveyActivity)getActivity()).borrarItem(597);
                ((SurveyActivity)getActivity()).borrarItem(596);
                ((SurveyActivity)getActivity()).borrarItem(595);
                ((SurveyActivity)getActivity()).borrarItem(594);
                ((SurveyActivity)getActivity()).borrarItem(593);
//                ((SurveyActivity)getActivity()).hideKeyboard();
                ((SurveyActivity) getActivity()).selectFragment(23);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
//                List<String> respuesta7 = new ArrayList<String>();
//                ((SurveyActivity)getActivity()).hideKeyboard();

                if(!checkFactor_1|| !checkFactor_2 || !checkFactor_3 || !checkFactor_4 || !checkFactor_5 || !checkFactor_6){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(602, "simple", respuesta1);
                    respuesta2.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(603, "simple", respuesta2);
                    respuesta3.add(factor_3);
                    ((SurveyActivity)getActivity()).crearItem(604, "simple", respuesta3);
                    respuesta4.add(factor_4);
                    ((SurveyActivity)getActivity()).crearItem(605, "simple", respuesta4);
                    respuesta5.add(factor_5);
                    ((SurveyActivity)getActivity()).crearItem(606, "simple", respuesta5);
                    respuesta6.add(factor_6);
                    ((SurveyActivity)getActivity()).crearItem(607, "simple", respuesta6);
//                  respuesta.add(factor_7);
//                ((SurveyActivity)getActivity()).crearItem(180, "simple", respuesta);
//                    respuesta7.add(tOtros.getText().toString().toUpperCase());
//                    ((SurveyActivity)getActivity()).crearItem(412, "text", respuesta7);
//                    Log.d("mensaje", "onClick: -"+ tOtros.getText().toString().toUpperCase()+"--");
//                    ((SurveyActivity)getActivity()).hideKeyboard();
                    ((SurveyActivity) getActivity()).selectFragment(25);
                }


            }
        });
        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor33_2_1:
                        factor_1 = "1156";
                        break;
                    case R.id.radioFactor33_2_2:
                        factor_1 = "1157";
                        break;
//                    case R.id.radioFactor33_2_3:
//                        factor_1 = "506";
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor33_3_1:
                        factor_2 = "1158";
                        break;
                    case R.id.radioFactor33_3_2:
                        factor_2 = "1159";
                        break;
//                    case R.id.radioFactor33_3_3:
//                        factor_2 = "509";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor33_4_1:
                        factor_3 = "1160";
                        break;
                    case R.id.radioFactor33_4_2:
                        factor_3 = "1161";
                        break;
//                    case R.id.radioFactor33_4_3:
//                        factor_3 = "512";
//                        break;
                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor33_5_1:
                        factor_4 = "1162";
                        break;
                    case R.id.radioFactor33_5_2:
                        factor_4 = "1163";
                        break;
//                    case R.id.radioFactor33_5_3:
//                        factor_4 = "515";
//                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor33_6_1:
                        factor_5 = "1164";
                        break;
                    case R.id.radioFactor33_6_2:
                        factor_5 = "1165";
                        break;
//                    case R.id.radioFactor33_6_3:
//                        factor_5 = "518";
//                        break;
                }
            }
        });

        GFactor6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_6 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor33_7_1:
                        factor_6 = "1166";
                        break;
                    case R.id.radioFactor33_7_2:
                        factor_6 = "1167";
                        break;
//                    case R.id.radioFactor33_7_3:
//                        factor_6 = "521";
//                        break;
                }
            }
        });

//        GFactor7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
//        {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId)
//            {
//                checkFactor_7 = true;
//                switch(checkedId)
//                {
//                    case R.id.radioFactor33_8_1:
//                        factor_7 = "522";
//                        break;
//                    case R.id.radioFactor33_8_2:
//                        factor_7 = "523";
//                        break;
//                    case R.id.radioFactor33_8_3:
//                        factor_7 = "524";
//                        break;
//                }
//            }
//        });


        return view;
    }
}


