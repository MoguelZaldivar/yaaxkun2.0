package com.yaaxkun.Fragment;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion17 extends Fragment {
    EditText tOtros;
    private int itemID = 415, responseID = 476, factor_nInundacion;
    private String factor_1=null, factor_2=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkPrueba=false, checkPrueba2=false;
    EditText riesgo, impacto;

    //    CheckBox chkR3_1, chkR3_2, chkR3_3, chkR3_4, chkR3_5, chkR3_6, chkR3_7;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.fragment_question17, container, false);
        final ScrollView scroll = (ScrollView) view.findViewById(R.id.SV1);
//        final ConstraintLayout conLay2 = (ConstraintLayout) view.findViewById(R.id.constraintLayout2);



        Button next = (Button) view.findViewById(R.id.bNext);
        Button back = (Button) view.findViewById(R.id.bBack);
        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.rG1);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.rG2);
//        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.rG3);

//        RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.rG6);
//        nInundacion = (EditText) view.findViewById(R.id.inundacionNumeroText);
        riesgo = (EditText) view.findViewById(R.id.riesgoText);
        impacto = (EditText) view.findViewById(R.id.impactoText);

//        RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.rG7);
//        final EditText tOtros = (EditText)view.findViewById(R.id.eTOtros9);
        scroll.fullScroll(ScrollView.FOCUS_UP);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String factor_7 = tOtros.getText().toString().toUpperCase();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();





//                List<String> respuesta7 = new ArrayList<String>();
//                ((SurveyActivity)getActivity()).hideKeyboard();
//                if ( nInundacion.getText().toString().length() > 0 ){factor_nInundacion = Integer.parseInt(nInundacion.getText().toString());}

                String factor_riesgo = riesgo.getText().toString().toUpperCase();
                String factor_impacto = impacto.getText().toString().toUpperCase();


                if(!checkFactor_1 || !checkFactor_2 ){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {

                    if ( (factor_1.equals(Integer.toString(responseID)) && factor_riesgo.equals("")) || (factor_2.equals(Integer.toString(responseID+2)) && factor_impacto.equals("")) ) {
                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos", Toast.LENGTH_SHORT).show();

                    } else {

                        respuesta1.add(String.valueOf(factor_1));
                        ((SurveyActivity) getActivity()).crearItem(itemID, "simple", respuesta1);

                        respuesta2.add(factor_riesgo);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 1, "text", respuesta2);

                        respuesta3.add(factor_2);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 2, "simple", respuesta3);

                        respuesta4.add(factor_impacto);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 3, "text", respuesta4);


                        ((SurveyActivity)getActivity()).hideKeyboard(v);
                        ((SurveyActivity) getActivity()).selectFragment(18);
                    }
                }

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
                ((SurveyActivity)getActivity()).borrarItem(itemID-3);
                ((SurveyActivity)getActivity()).borrarItem(itemID-4);
                ((SurveyActivity)getActivity()).borrarItem(itemID-5);
                ((SurveyActivity)getActivity()).borrarItem(itemID-6);
                ((SurveyActivity)getActivity()).hideKeyboard(v);
//                ((SurveyActivity)getActivity()).borrarItem(itemID-7); //corregir

//                ((SurveyActivity)getActivity()).hideKeyboard();
                ((SurveyActivity)getActivity()).selectFragment(16);
            }
        });


        riesgo.setVisibility(View.GONE);
        impacto.setVisibility(View.GONE);



        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.rdb1Si:
                        factor_1 = Integer.toString(responseID);
                        riesgo.setVisibility(View.VISIBLE);

                        break;
                    case R.id.rdb1No:

                        factor_1 = Integer.toString(responseID+1);
                        riesgo.setVisibility(View.GONE);
                        riesgo.setText("");
                        break;

                }
            }
        });

        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.rdb2Si:
                        factor_2 = Integer.toString(responseID+2);
                        impacto.setVisibility(View.VISIBLE);

                        break;
                    case R.id.rdb2No:

                        factor_2 = Integer.toString(responseID+3);
                        impacto.setVisibility(View.GONE);
                        impacto.setText("");
                        break;

                }
            }
        });






        return view;
    }
}
