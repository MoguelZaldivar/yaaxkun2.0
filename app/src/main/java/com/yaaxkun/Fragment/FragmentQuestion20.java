package com.yaaxkun.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class FragmentQuestion20 extends Fragment {
    private int itemID = 429;
    private String resPhoto = null;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private ImageView capturePhoto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question20, container, false);
        Button next = (Button) view.findViewById(R.id.bNext);
        Button back = (Button) view.findViewById(R.id.bBack);
        ImageView photo = (ImageView) view.findViewById(R.id.photo);
        capturePhoto = (ImageView) view.findViewById(R.id.capturePhoto);
        
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resPhoto = null;
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> respuesta1 = new ArrayList<String>();
                respuesta1.add(resPhoto);
                ((SurveyActivity)getActivity()).crearItem(itemID, "file", respuesta1);

                ((SurveyActivity) getActivity()).selectFragment(21);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resPhoto = null;
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
                ((SurveyActivity)getActivity()).borrarItem(itemID-3);
                ((SurveyActivity)getActivity()).borrarItem(itemID-4);
                ((SurveyActivity)getActivity()).borrarItem(itemID-5);
                ((SurveyActivity)getActivity()).borrarItem(itemID-6);
                ((SurveyActivity)getActivity()).borrarItem(itemID-7);
                ((SurveyActivity)getActivity()).borrarItem(itemID-8);
                ((SurveyActivity)getActivity()).borrarItem(itemID-9);
                ((SurveyActivity)getActivity()).borrarItem(itemID-10);

                ((SurveyActivity) getActivity()).selectFragment(19);
            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            capturePhoto.setImageBitmap(bitmap);
            resPhoto += "data:image/jpeg;base64," + encodeImage(bitmap);
        }
    }

    private String encodeImage(Bitmap bm)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

//    public static void longLog(String str) {
//        if (str.length() > 4000) {
//            Log.d("mensaje", str.substring(0, 4000));
//            longLog(str.substring(4000));
//        } else
//            Log.d("mensaje", str);
//    }
}
