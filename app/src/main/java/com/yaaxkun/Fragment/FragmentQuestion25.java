package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion25 extends Fragment {
    EditText tOtros;
    private String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null, factor_6=null, factor_7=null, factor_8=null, factor_9=null, factor_10=null, factor_11=null, factor_12=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false, checkFactor_6 = false, checkFactor_7 = false, checkFactor_8 = false, checkFactor_9 = false, checkFactor_10 = false, checkFactor_11 = false, checkFactor_12 = false;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question25, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor25);
        Button next = (Button) view.findViewById(R.id.nextFactor25);
        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor25);


        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor25_1);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor25_2);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor25_4);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor25_5);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor25_6);
        final RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.groupFactor25_7);
        final RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.groupFactor25_8);
        final RadioGroup GFactor8 = (RadioGroup) view.findViewById(R.id.groupFactor25_9);
        final RadioGroup GFactor9 = (RadioGroup) view.findViewById(R.id.groupFactor25_10);
        final RadioGroup GFactor10 = (RadioGroup) view.findViewById(R.id.groupFactor25_11);
        final RadioGroup GFactor11 = (RadioGroup) view.findViewById(R.id.groupFactor25_12);
        final RadioGroup GFactor12 = (RadioGroup) view.findViewById(R.id.groupFactor25_13);
//        tOtros = (EditText)view.findViewById(R.id.eTOtros);
        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(527);
                ((SurveyActivity)getActivity()).borrarItem(526);
                ((SurveyActivity)getActivity()).borrarItem(525);
                ((SurveyActivity)getActivity()).borrarItem(524);
                ((SurveyActivity)getActivity()).borrarItem(523);
                ((SurveyActivity)getActivity()).borrarItem(522);
                ((SurveyActivity)getActivity()).borrarItem(521);
                ((SurveyActivity)getActivity()).borrarItem(520);
//                ((SurveyActivity)getActivity()).hideKeyboard();
                ((SurveyActivity)getActivity()).selectFragment(14);
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
                List<String> respuesta7 = new ArrayList<String>();
                List<String> respuesta8 = new ArrayList<String>();
                List<String> respuesta9 = new ArrayList<String>();
                List<String> respuesta10 = new ArrayList<String>();
                List<String> respuesta11 = new ArrayList<String>();
//                List<String> respuesta12 = new ArrayList<String>();
                List<String> respuesta13 = new ArrayList<String>();
//                ((SurveyActivity)getActivity()).hideKeyboard();
                if(!checkFactor_1){
                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else{
                    if((factor_1 == "1008" && (checkFactor_2 && checkFactor_3 && checkFactor_4 && checkFactor_5 && checkFactor_6 && checkFactor_7 && checkFactor_8 && checkFactor_9 && checkFactor_10 && checkFactor_11 && checkFactor_12))|| factor_1=="1009"){
                        respuesta1.add(factor_1);
                        ((SurveyActivity)getActivity()).crearItem(528, "simple", respuesta1);
                        respuesta2.add(factor_2);
                        ((SurveyActivity)getActivity()).crearItem(529, "simple", respuesta2);
                        respuesta3.add(factor_3);
                        ((SurveyActivity)getActivity()).crearItem(530, "simple", respuesta3);
                        respuesta4.add(factor_4);
                        ((SurveyActivity)getActivity()).crearItem(531, "simple", respuesta4);
                        respuesta5.add(factor_5);
                        ((SurveyActivity)getActivity()).crearItem(532, "simple", respuesta5);
                        respuesta6.add(factor_6);
                        ((SurveyActivity)getActivity()).crearItem(533, "simple", respuesta6);
                        respuesta7.add(factor_7);
                        ((SurveyActivity)getActivity()).crearItem(534, "simple", respuesta7);
                        respuesta8.add(factor_8);
                        ((SurveyActivity)getActivity()).crearItem(535, "simple", respuesta8);
                        respuesta9.add(factor_9);
                        ((SurveyActivity)getActivity()).crearItem(536, "simple", respuesta9);
                        respuesta10.add(factor_10);
                        ((SurveyActivity)getActivity()).crearItem(537, "simple", respuesta10);
                        respuesta11.add(factor_11);
                        ((SurveyActivity)getActivity()).crearItem(538, "simple", respuesta11);
//                        respuesta12.add(tOtros.getText().toString().toUpperCase());
//                        ((SurveyActivity)getActivity()).crearItem(341, "text", respuesta12);
//                        Log.d("mensaje", "onClick: -"+ tOtros.getText().toString()+"--");
                        respuesta13.add(factor_12);
                        ((SurveyActivity)getActivity()).crearItem(539, "simple", respuesta13);
//                        ((SurveyActivity)getActivity()).hideKeyboard();
                        ((SurveyActivity)getActivity()).selectFragment(16);
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        viewScroll.setVisibility(View.GONE);

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor25_1_1:
                        factor_1 = "1008";

                        viewScroll.setVisibility(View.VISIBLE);
                        viewScroll.fullScroll(ScrollView.FOCUS_UP);
                        break;
                    case R.id.radioFactor25_1_2:
                        GFactor2.clearCheck();
                        GFactor3.clearCheck();
                        GFactor4.clearCheck();
                        GFactor5.clearCheck();
                        GFactor6.clearCheck();
                        GFactor7.clearCheck();
                        GFactor8.clearCheck();
                        GFactor9.clearCheck();
                        GFactor10.clearCheck();
                        GFactor11.clearCheck();
                        GFactor12.clearCheck();
                        factor_1 = "1009";
                        factor_2 = null;
                        factor_3 = null;
                        factor_4 = null;
                        factor_5 = null;
                        factor_6 = null;
                        factor_7 = null;
                        factor_8 = null;
                        factor_9 = null;
                        factor_10 = null;
                        factor_11 = null;
                        factor_12 = null;
                        checkFactor_2 = false;
                        checkFactor_3 = false;
                        checkFactor_4 = false;
                        checkFactor_5 = false;
                        checkFactor_6 = false;
                        checkFactor_7 = false;
                        checkFactor_8 = false;
                        checkFactor_9 = false;
                        checkFactor_10 = false;
                        checkFactor_11 = false;
                        checkFactor_12 = false;
//                        GFactor1.clearCheck();

//                        tOtros.setText(null);

                        viewScroll.setVisibility(View.GONE);
                        break;
//                    case R.id.radioFactor25_1_3:
//                    GFactor2.clearCheck();
//                    GFactor3.clearCheck();
//                    GFactor4.clearCheck();
//                    GFactor5.clearCheck();
//                    GFactor6.clearCheck();
//                    GFactor7.clearCheck();
//                    GFactor8.clearCheck();
//                    GFactor9.clearCheck();
//                    GFactor10.clearCheck();
//                    GFactor12.clearCheck();
//                        factor_1 = "284";
//                        factor_2 = "287";
//                        factor_3 = "290";
//                        factor_4 = "293";
//                        factor_5 = "296";
//                        factor_6 = "299";
//                        factor_7 = "302";
//                        factor_8 = "305";
//                        factor_9 = "308";
//                        factor_10 = "311";
////                        GFactor1.clearCheck();
//
//                        viewScroll.setVisibility(View.GONE);
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor25_2_1:
                        factor_2 = "1010";
                        break;
                    case R.id.radioFactor25_2_2:
                        factor_2 = "1011";
                        break;
//                    case R.id.radioFactor25_2_3:
//                        factor_2 = "287";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor25_4_1:
                        factor_3 = "1012";
                        break;
                    case R.id.radioFactor25_4_2:
                        factor_3 = "1013";
                        break;
//                    case R.id.radioFactor25_4_3:
//                        factor_3 = "290";
//                        break;
                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor25_5_1:
                        factor_4 = "1014";
                        break;
                    case R.id.radioFactor25_5_2:
                        factor_4 = "1015";
                        break;
//                    case R.id.radioFactor25_5_3:
//                        factor_4 = "293";
//                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor25_6_1:
                        factor_5 = "1016";
                        break;
                    case R.id.radioFactor25_6_2:
                        factor_5 = "1017";
                        break;
//                    case R.id.radioFactor25_6_3:
//                        factor_5 = "296";
//                        break;
                }
            }
        });

        GFactor6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_6 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor25_7_1:
                        factor_6 = "1018";
                        break;
                    case R.id.radioFactor25_7_2:
                        factor_6 = "1019";
                        break;
//                    case R.id.radioFactor25_7_3:
//                        factor_6 = "299";
//                        break;
                }
            }
        });

        GFactor7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_7 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor25_8_1:
                        factor_7 = "1020";
                        break;
                    case R.id.radioFactor25_8_2:
                        factor_7 = "1021";
                        break;
//                    case R.id.radioFactor25_8_3:
//                        factor_7 = "302";
//                        break;
                }
            }
        });

        GFactor8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_8 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor25_9_1:
                        factor_8 = "1022";
                        break;
                    case R.id.radioFactor25_9_2:
                        factor_8 = "1023";
                        break;
//                    case R.id.radioFactor25_9_3:
//                        factor_8 = "305";
//                        break;
                }
            }
        });

        GFactor9.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_9 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor25_10_1:
                        factor_9 = "1024";
                        break;
                    case R.id.radioFactor25_10_2:
                        factor_9 = "1025";
                        break;
//                    case R.id.radioFactor25_10_3:
//                        factor_9 = "308";
//                        break;
                }
            }
        });
        GFactor10.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_10 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor25_11_1:
                        factor_10 = "1026";
                        break;
                    case R.id.radioFactor25_11_2:
                        factor_10 = "1027";
                        break;
//                    case R.id.radioFactor25_11_3:
//                        factor_10 = "311";
//                        break;
                }
            }
        });
        GFactor11.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_11 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor25_12_1:
                        factor_11 = "1028";
                        break;
                    case R.id.radioFactor25_12_2:
                        factor_11 = "1029";
                        break;
//                    case R.id.radioFactor25_12_3:
//                        factor_11 = "311";
//                        break;
                }
            }
        });
        GFactor12.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_12 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor25_13_1:
                        factor_12 = "1030";
                        break;
                    case R.id.radioFactor25_13_2:
                        factor_12 = "1031";
                        break;
//                    case R.id.radioFactor25_13_3:
//                        factor_12 = "311";
//                        break;
                }
            }
        });

        return view;
    }
}