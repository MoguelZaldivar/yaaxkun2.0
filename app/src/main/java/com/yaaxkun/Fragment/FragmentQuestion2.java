package com.yaaxkun.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion2 extends Fragment {

    private int itemID = 340, responseID = 251;

    CheckBox chkR3_1, chkR3_2, chkR3_3, chkR3_4, chkR3_5, chkR3_6, chkR3_7;
    CheckBox chkR4_1, chkR4_2, chkR4_3, chkR4_4, chkR4_5, chkR4_6, chkR4_7;
    EditText otros3, otros4;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question2, container, false);
        Button next = (Button) view.findViewById(R.id.bNext);
        Button back = (Button) view.findViewById(R.id.bBack);



        final ScrollView scroll = (ScrollView) view.findViewById(R.id.SV1);
//        final TextView text = (TextView) view.findViewById(R.id.textViewF4);
//        final ConstraintLayout constrain = (ConstraintLayout) view.findViewById(R.id.constraintLayout2);
//        final ConstraintLayout constrai2 = (ConstraintLayout) view.findViewById(R.id.constraintLayout3);

        scroll.fullScroll(ScrollView.FOCUS_UP);
        chkR3_1 = (CheckBox) view.findViewById(R.id.chkOp1A);
        chkR3_2 = (CheckBox) view.findViewById(R.id.chkOp2A);
        chkR3_3 = (CheckBox) view.findViewById(R.id.chkOp3A);
        chkR3_4 = (CheckBox) view.findViewById(R.id.chkOp4A);
        chkR3_5 = (CheckBox) view.findViewById(R.id.chkOp5A);
        chkR3_6 = (CheckBox) view.findViewById(R.id.chkOp6A);
        chkR3_7 = (CheckBox) view.findViewById(R.id.chkOp7A);

        chkR4_1 = (CheckBox) view.findViewById(R.id.chkOp1B);
        chkR4_2 = (CheckBox) view.findViewById(R.id.chkOp2B);
        chkR4_3 = (CheckBox) view.findViewById(R.id.chkOp3B);
        chkR4_4 = (CheckBox) view.findViewById(R.id.chkOp4B);
        chkR4_5 = (CheckBox) view.findViewById(R.id.chkOp5B);
        chkR4_6 = (CheckBox) view.findViewById(R.id.chkOp6B);
        chkR4_7 = (CheckBox) view.findViewById(R.id.chkOp7B);

        otros3 = (EditText) view.findViewById(R.id.otros3);
        otros4 = (EditText) view.findViewById(R.id.otros4);


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();

                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
//                List<String> respuesta5 = new ArrayList<String>();
//                List<String> respuesta6 = new ArrayList<String>();
//                List<String> respuesta7 = new ArrayList<String>();

                boolean checkFactor_Vehiculo = false, checkFactor_Tienda = false;

                String result = "Vehiculos";
                if( chkR3_1.isChecked()){checkFactor_Vehiculo = true; result += "\nBus";}
                if( chkR3_2.isChecked()){checkFactor_Vehiculo = true; result += "\nCombi";}
                if( chkR3_3.isChecked()){checkFactor_Vehiculo = true; result += "\nTatsi";}
                if( chkR3_4.isChecked()){checkFactor_Vehiculo = true; result += "\nMotoTatsi";}
                if( chkR3_5.isChecked()){checkFactor_Vehiculo = true; result += "\nmoto";}
                if( chkR3_6.isChecked()){checkFactor_Vehiculo = true; result += "\nauto";}
                if( chkR3_7.isChecked()){checkFactor_Vehiculo = true; result += "\notro3";}
                Log.d("mensaje", "Respuestas 3: "+ result);

                String factor_2 = otros3.getText().toString().toUpperCase();
                Log.d("mensaje", "Respuestas 2: "+ factor_2);

                String result2 = "viveres";
                if( chkR4_1.isChecked()){checkFactor_Tienda = true; result2 += "\nMercado";}
                if( chkR4_2.isChecked()){checkFactor_Tienda = true; result2 += "\nTiendita";}
                if( chkR4_3.isChecked()){checkFactor_Tienda = true; result2 += "\nTiendottoa";}
                if( chkR4_4.isChecked()){checkFactor_Tienda = true; result2 += "\nSuper";}
                if( chkR4_5.isChecked()){checkFactor_Tienda = true; result2 += "\nottso";}
                if( chkR4_6.isChecked()){checkFactor_Tienda = true; result2 += "\nplaza";}
                if( chkR4_7.isChecked()){checkFactor_Tienda = true; result2 += "\nOtro4";}
                Log.d("mensaje","Respuestas 5:"+  result2);

                String factor_4 = otros4.getText().toString().toUpperCase();
                Log.d("mensaje", "Respuestas 4: "+ factor_4);


                if(!checkFactor_Vehiculo || !checkFactor_Tienda){
                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else{
                    if(  (chkR3_7.isChecked() && otros3.getText().toString().length() == 0) || (chkR4_7.isChecked() && otros4.getText().toString().length() == 0) ){
//
                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                    } else {


                        if(chkR3_1.isChecked()){respuesta1.add(Integer.toString(responseID));}
                        if(chkR3_2.isChecked()){respuesta1.add(Integer.toString(responseID+1));}
                        if(chkR3_3.isChecked()){respuesta1.add(Integer.toString(responseID+2));}
                        if(chkR3_4.isChecked()){respuesta1.add(Integer.toString(responseID+3));} // Corregir esta wea
                        if(chkR3_5.isChecked()){respuesta1.add(Integer.toString(responseID+4));}
                        if(chkR3_6.isChecked()){respuesta1.add(Integer.toString(responseID+5));}
                        if(chkR3_7.isChecked()){respuesta1.add(Integer.toString(responseID+6));}

                        ((SurveyActivity)getActivity()).crearItem(itemID, "multiple", respuesta1);

                        respuesta2.add(factor_2);
                        ((SurveyActivity)getActivity()).crearItem(itemID+1, "text", respuesta2);

                        if(chkR4_1.isChecked()){respuesta3.add(Integer.toString(responseID+7));}
                        if(chkR4_2.isChecked()){respuesta3.add(Integer.toString(responseID+8));}
                        if(chkR4_3.isChecked()){respuesta3.add(Integer.toString(responseID+9));}
                        if(chkR4_4.isChecked()){respuesta3.add(Integer.toString(responseID+10));}
                        if(chkR4_5.isChecked()){respuesta3.add(Integer.toString(responseID+11));}
                        if(chkR4_6.isChecked()){respuesta3.add(Integer.toString(responseID+12));}
                        if(chkR4_7.isChecked()){respuesta3.add(Integer.toString(responseID+13));}

                        ((SurveyActivity)getActivity()).crearItem(itemID+2, "multiple", respuesta3);

                        respuesta4.add(factor_4);
                        ((SurveyActivity)getActivity()).crearItem(itemID+3, "text", respuesta4);
                        ((SurveyActivity)getActivity()).hideKeyboard(v);
                        ((SurveyActivity)getActivity()).selectFragment(3);

                    }
                }


            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
                ((SurveyActivity)getActivity()).borrarItem(itemID-3);
                ((SurveyActivity)getActivity()).borrarItem(itemID-4);
                ((SurveyActivity)getActivity()).borrarItem(itemID-5);
                ((SurveyActivity)getActivity()).borrarItem(itemID-6);
                ((SurveyActivity)getActivity()).borrarItem(itemID-7);
                ((SurveyActivity)getActivity()).borrarItem(itemID-8);
                ((SurveyActivity)getActivity()).hideKeyboard(v);
                ((SurveyActivity)getActivity()).selectFragment(1);
            }
        });

//        scroll.setVisibility(View.GONE);
//        constrain.setVisibility(View.GONE);
//        constrai2.setVisibility(View.GONE);
        otros3.setVisibility(view.GONE);
        otros4.setVisibility(view.GONE);



        chkR3_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    Log.d("mensaje","otros 3 checado:");
                    otros3.setVisibility(View.VISIBLE);
                } else {
                    Log.d("mensaje","otros  no 3 checado:");
                    otros3.setVisibility(view.GONE);
                    otros3.setText("");
                }
            }
        });

        chkR4_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    Log.d("mensaje","otros 4 checado:");
                    otros4.setVisibility(View.VISIBLE);
                } else {
                    Log.d("mensaje","otros  no 4 checado:");
                    otros4.setVisibility(view.GONE);
                    otros4.setText("");
                }
            }
        });


        return view;
    }

}