package com.yaaxkun.Fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.yaaxkun.Activity.MainActivity;
import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

public class FragmentFinal extends Fragment {
    private int itemID = 428;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_final, container, false);

        Button prev = (Button) view.findViewById(R.id.prevSurveyS);
        Button next = (Button) view.findViewById(R.id.nextSurveyS);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
//                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
//                ((SurveyActivity)getActivity()).borrarItem(itemID-3);
//                ((SurveyActivity)getActivity()).borrarItem(itemID-4);
//                ((SurveyActivity)getActivity()).borrarItem(itemID-5);
//                ((SurveyActivity)getActivity()).borrarItem(itemID-6);
//                ((SurveyActivity)getActivity()).borrarItem(itemID-7);
//                ((SurveyActivity)getActivity()).borrarItem(itemID-8);

                ((SurveyActivity) getActivity()).selectFragment(20);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Encuesta enviada", Toast.LENGTH_SHORT).show();
                ((SurveyActivity)getActivity()).sendPost();
                Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }
}
