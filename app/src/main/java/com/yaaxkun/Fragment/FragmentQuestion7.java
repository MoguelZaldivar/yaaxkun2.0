package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion7 extends Fragment {
    private String error="";
    private int itemID = 355, responseID = 314, nMunicipio = 0;
    private String factor_1= null;
    boolean checkFactor_1 = false, checkSpinner1 = false;
    EditText otro1, otro2;
    TextView municipioT;
    private Spinner municipio;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question7, container, false);
        Button next = (Button) view.findViewById(R.id.bNext);
        Button back = (Button) view.findViewById(R.id.bBack);

        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.rG1);
//        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.rG2);
        otro1 = (EditText) view.findViewById(R.id.tColoniaText);
        otro2 = (EditText) view.findViewById(R.id.tMOtroText);
        municipioT = (TextView) view.findViewById(R.id.factorMOtro);
        municipio = (Spinner) view.findViewById(R.id.municipioSpinner);

        final ScrollView scroll = (ScrollView) view.findViewById(R.id.SV1);
//        final TextView text = (TextView) view.findViewById(R.id.textViewF4);


        scroll.fullScroll(ScrollView.FOCUS_UP);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();

                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();

                String factor_Otro = otro1.getText().toString().toUpperCase();
                String factor_Otro2 = otro2.getText().toString().toUpperCase();

                if(!checkFactor_1 || !checkSpinner1 || factor_Otro.equals("") || (nMunicipio == 11 && factor_Otro2.equals("")) ){
                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else{
//                    if(checkFactor_1 && checkFactor_2){
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(itemID, "simple", respuesta1);

                    respuesta2.add(factor_Otro);
                    ((SurveyActivity)getActivity()).crearItem(itemID+1, "text", respuesta2);

                    respuesta3.add(String.valueOf(nMunicipio+responseID+5));
                    ((SurveyActivity)getActivity()).crearItem(itemID+2, "simple", respuesta3);

                    respuesta4.add(factor_Otro2);
                    ((SurveyActivity)getActivity()).crearItem(itemID+3, "text", respuesta4);
                    ((SurveyActivity)getActivity()).hideKeyboard(v);

                    ((SurveyActivity)getActivity()).selectFragment(8);
//                    } else {
//                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
//                    }
                }


            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
//                ((SurveyActivity)getActivity()).borrarItem(itemID-3);
                ((SurveyActivity)getActivity()).hideKeyboard(v);
                ((SurveyActivity)getActivity()).selectFragment(6);
            }
        });

        otro2.setVisibility(View.GONE);
        municipioT.setVisibility(View.GONE);

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.rdb1_1:
                        factor_1 = Integer.toString(responseID);
                        break;
                    case R.id.rdb1_2:
                        factor_1 = Integer.toString(responseID+1);
                        break;
                    case R.id.rdb1_3:
                        factor_1 = Integer.toString(responseID+2);
                        break;
                    case R.id.rdb1_4:
                        factor_1 = Integer.toString(responseID+3);
                        break;
                    case R.id.rdb1_5:
                        factor_1 = Integer.toString(responseID+4);
                        break;


                }
            }
        });

        municipio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.d("mensaje", "Personas: "+i + " - " + l);
                if (i>0){
                    checkSpinner1 = true;
                    nMunicipio = i-1;
                    error = "";
//                    Log.d("mensaje", "Municipio: "+nMunicipio);
                    if (i == 12){
                        otro2.setVisibility(View.VISIBLE);
                        municipioT.setVisibility(View.VISIBLE);
                    }else {
                        otro2.setVisibility(View.GONE);
                        municipioT.setVisibility(View.GONE);
                        otro2.setText("");
                    }
//
                }else{
                    nMunicipio = 0;
                    checkSpinner1 = false;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        return view;
    }
}
