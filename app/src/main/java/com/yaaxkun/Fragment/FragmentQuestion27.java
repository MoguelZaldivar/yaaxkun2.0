package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion27 extends Fragment {
    private String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question27, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor27);
        Button next = (Button) view.findViewById(R.id.nextFactor27);

        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor27);
        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor27_2);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor27_3);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor27_4);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor27_5);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor27_6);
        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(557);
                ((SurveyActivity)getActivity()).borrarItem(556);
                ((SurveyActivity)getActivity()).borrarItem(555);
                ((SurveyActivity)getActivity()).borrarItem(554);
                ((SurveyActivity)getActivity()).borrarItem(553);
                ((SurveyActivity)getActivity()).borrarItem(552);
                ((SurveyActivity)getActivity()).borrarItem(551);
                ((SurveyActivity)getActivity()).borrarItem(550);
                ((SurveyActivity)getActivity()).borrarItem(549);
                ((SurveyActivity)getActivity()).borrarItem(548);
                ((SurveyActivity)getActivity()).borrarItem(547);
                ((SurveyActivity)getActivity()).borrarItem(546);

                ((SurveyActivity) getActivity()).selectFragment(17);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                List<String> respuesta = new ArrayList<String>();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();

                if(!checkFactor_1|| !checkFactor_2 || !checkFactor_3 || !checkFactor_4 || !checkFactor_5){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(558, "simple", respuesta1);
                    respuesta2.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(559, "simple", respuesta2);
                    respuesta3.add(factor_3);
                    ((SurveyActivity)getActivity()).crearItem(560, "simple", respuesta3);
                    respuesta4.add(factor_4);
                    ((SurveyActivity)getActivity()).crearItem(561, "simple", respuesta4);
                    respuesta5.add(factor_5);
                    ((SurveyActivity)getActivity()).crearItem(562, "simple", respuesta5);

                    ((SurveyActivity) getActivity()).selectFragment(19);
                }



            }
        });

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor27_2_1:
                        factor_1 = "1068";
                        break;
                    case R.id.radioFactor27_2_2:
                        factor_1 = "1069";
                        break;
//                    case R.id.radioFactor27_2_3:
//                        factor_1 = "409";
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor27_3_1:
                        factor_2 = "1070";
                        break;
                    case R.id.radioFactor27_3_2:
                        factor_2 = "1071";
                        break;
//                    case R.id.radioFactor27_3_3:
//                        factor_2 = "410";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor27_4_1:
                        factor_3 = "1072";
                        break;
                    case R.id.radioFactor27_4_2:
                        factor_3 = "1073";
                        break;
//                    case R.id.radioFactor27_4_3:
//                        factor_3 = "411";
//                        break;
                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor27_5_1:
                        factor_4 = "1074";
                        break;
                    case R.id.radioFactor27_5_2:
                        factor_4 = "1075";
                        break;
//                    case R.id.radioFactor27_5_3:
//                        factor_4 = "412";
//                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor27_6_1:
                        factor_5 = "1076";
                        break;
                    case R.id.radioFactor27_6_2:
                        factor_5 = "1077";
                        break;
//                    case R.id.radioFactor27_6_3:
//                        factor_5 = "413";
//                        break;
                }
            }
        });


        return view;
    }
}