package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;


public class FragmentQuestion26 extends Fragment {
    private String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null, factor_6=null, factor_7=null, factor_8=null, factor_9=null, factor_10=null, factor_11=null, factor_12=null, factor_13=null, factor_14=null, factor_15=null, factor_16=null, factor_17=null, factor_18=null, factor_19=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false, checkFactor_6 = false, checkFactor_7 = false, checkFactor_8 = false, checkFactor_9 = false, checkFactor_10 = false, checkFactor_11 = false, checkFactor_12 = false, checkFactor_13 = false, checkFactor_14 = false, checkFactor_15 = false, checkFactor_16 = false, checkFactor_17 = false, checkFactor_18 = false, checkFactor_19 = false;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question26, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor26);
        Button next = (Button) view.findViewById(R.id.nextFactor26);

        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor26);
        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor26_2);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor26_3);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor26_4);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor26_5);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor26_6);
        final RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.groupFactor26_7);
//        final RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.groupFactor26_8);
        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(539);
                ((SurveyActivity)getActivity()).borrarItem(538);
                ((SurveyActivity)getActivity()).borrarItem(537);
                ((SurveyActivity)getActivity()).borrarItem(536);
                ((SurveyActivity)getActivity()).borrarItem(535);
                ((SurveyActivity)getActivity()).borrarItem(534);
                ((SurveyActivity)getActivity()).borrarItem(533);
                ((SurveyActivity)getActivity()).borrarItem(532);
                ((SurveyActivity)getActivity()).borrarItem(531);
                ((SurveyActivity)getActivity()).borrarItem(530);
                ((SurveyActivity)getActivity()).borrarItem(529);
                ((SurveyActivity)getActivity()).borrarItem(528);
//                ((SurveyActivity)getActivity()).borrarItem(330);
                ((SurveyActivity) getActivity()).selectFragment(15);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
//                List<String> respuesta7 = new ArrayList<String>();


                if(!checkFactor_1|| !checkFactor_2 || !checkFactor_3 || !checkFactor_4 || !checkFactor_5 || !checkFactor_6  ){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(540, "simple", respuesta1);
                    respuesta2.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(541, "simple", respuesta2);
                    respuesta3.add(factor_3);
                    ((SurveyActivity)getActivity()).crearItem(542, "simple", respuesta3);
                    respuesta4.add(factor_4);
                    ((SurveyActivity)getActivity()).crearItem(543, "simple", respuesta4);
                    respuesta5.add(factor_5);
                    ((SurveyActivity)getActivity()).crearItem(544, "simple", respuesta5);
                    respuesta6.add(factor_6);
                    ((SurveyActivity)getActivity()).crearItem(545, "simple", respuesta6);
//                    respuesta7.add(factor_7);
//                    ((SurveyActivity)getActivity()).crearItem(349, "simple", respuesta7);


                    ((SurveyActivity) getActivity()).selectFragment(17);
                }

            }
        });
        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor26_2_1:
                        factor_1 = "1032";
                        break;
                    case R.id.radioFactor26_2_2:
                        factor_1 = "1033";
                        break;
//                    case R.id.radioFactor26_2_3:
//                        factor_1 = "314";
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor26_3_1:
                        factor_2 = "1034";
                        break;
                    case R.id.radioFactor26_3_2:
                        factor_2 = "1035";
                        break;
//                    case R.id.radioFactor26_3_3:
//                        factor_2 = "317";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor26_4_1:
                        factor_3 = "1036";
                        break;
                    case R.id.radioFactor26_4_2:
                        factor_3 = "1037";
                        break;
//                    case R.id.radioFactor26_4_3:
//                        factor_3 = "320";
//                        break;
                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor26_5_1:
                        factor_4 = "1038";
                        break;
                    case R.id.radioFactor26_5_2:
                        factor_4 = "1039";
                        break;
//                    case R.id.radioFactor26_5_3:
//                        factor_4 = "323";
//                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor26_6_1:
                        factor_5 = "1040";
                        break;
                    case R.id.radioFactor26_6_2:
                        factor_5 = "1041";
                        break;
//                    case R.id.radioFactor26_6_3:
//                        factor_5 = "326";
//                        break;
                }
            }
        });

        GFactor6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_6 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor26_7_1:
                        factor_6 = "1042";
                        break;
                    case R.id.radioFactor26_7_2:
                        factor_6 = "1043";
                        break;
//                    case R.id.radioFactor26_7_3:
//                        factor_6 = "329";
//                        break;
                }
            }
        });

//        GFactor7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
//        {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId)
//            {
//                checkFactor_7 = true;
//                switch(checkedId)
//                {
//                    case R.id.radioFactor26_8_1:
//                        factor_7 = "643";
//                        break;
//                    case R.id.radioFactor26_8_2:
//                        factor_7 = "644";
//                        break;
////                    case R.id.radioFactor26_8_3:
////                        factor_7 = "23";
////                        break;
//                }
//            }
//        });


        return view;
    }
}


