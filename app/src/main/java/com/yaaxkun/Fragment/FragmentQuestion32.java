package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion32 extends Fragment {
    String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null, factor_6=null, factor_7=null, factor_8=null, factor_9=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false, checkFactor_6 = false, checkFactor_7 = false, checkFactor_8 = false,checkFactor_9 = false;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question32, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor32);
        Button next = (Button) view.findViewById(R.id.nextFactor32);

        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor32);
        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor32_2);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor32_3);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor32_4);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor32_5);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor32_6);
        final RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.groupFactor32_7);
        final RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.groupFactor32_8);
        final RadioGroup GFactor8 = (RadioGroup) view.findViewById(R.id.groupFactor32_9);
        final RadioGroup GFactor9 = (RadioGroup) view.findViewById(R.id.groupFactor32_10);
        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((SurveyActivity)getActivity()).borrarItem(184);
                ((SurveyActivity)getActivity()).borrarItem(592);
                ((SurveyActivity)getActivity()).borrarItem(591);
                ((SurveyActivity)getActivity()).borrarItem(590);
                ((SurveyActivity)getActivity()).borrarItem(589);
                ((SurveyActivity)getActivity()).borrarItem(588);
                ((SurveyActivity)getActivity()).borrarItem(587);

                ((SurveyActivity) getActivity()).selectFragment(22);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
                List<String> respuesta7 = new ArrayList<String>();
                List<String> respuesta8 = new ArrayList<String>();
                List<String> respuesta9 = new ArrayList<String>();

                if(!checkFactor_1|| !checkFactor_2 || !checkFactor_3 || !checkFactor_4 || !checkFactor_5 || !checkFactor_6 || !checkFactor_7 || !checkFactor_8 || !checkFactor_9){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(593, "simple", respuesta1);
                    respuesta2.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(594, "simple", respuesta2);
                    respuesta3.add(factor_3);
                    ((SurveyActivity)getActivity()).crearItem(595, "simple", respuesta3);
                    respuesta4.add(factor_4);
                    ((SurveyActivity)getActivity()).crearItem(596, "simple", respuesta4);
                    respuesta5.add(factor_5);
                    ((SurveyActivity)getActivity()).crearItem(597, "simple", respuesta5);
                    respuesta6.add(factor_6);
                    ((SurveyActivity)getActivity()).crearItem(598, "simple", respuesta6);
                    respuesta7.add(factor_7);
                    ((SurveyActivity)getActivity()).crearItem(599, "simple", respuesta7);
                    respuesta8.add(factor_8);
                    ((SurveyActivity)getActivity()).crearItem(600, "simple", respuesta8);
                    respuesta9.add(factor_9);
                    ((SurveyActivity)getActivity()).crearItem(601, "simple", respuesta9);
                    ((SurveyActivity) getActivity()).selectFragment(24);
                }

            }
        });

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor32_2_1:
                        factor_1 = "1138";
                        break;
                    case R.id.radioFactor32_2_2:
                        factor_1 = "1139";
                        break;
//                    case R.id.radioFactor32_2_3:
//                        factor_1 = "482";
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor32_3_1:
                        factor_2 = "1140";
                        break;
                    case R.id.radioFactor32_3_2:
                        factor_2 = "1141";
                        break;
//                    case R.id.radioFactor32_3_3:
//                        factor_2 = "485";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor32_4_1:
                        factor_3 = "1142";
                        break;
                    case R.id.radioFactor32_4_2:
                        factor_3 = "1143";
                        break;
                    case R.id.radioFactor32_4_3:
                        factor_3 = "1183";
                        break;
                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor32_5_1:
                        factor_4 = "1144";
                        break;
                    case R.id.radioFactor32_5_2:
                        factor_4 = "1145";
                        break;
//                    case R.id.radioFactor32_5_3:
//                        factor_4 = "491";
//                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor32_6_1:
                        factor_5 = "1146";
                        break;
                    case R.id.radioFactor32_6_2:
                        factor_5 = "1147";
                        break;
//                    case R.id.radioFactor32_6_3:
//                        factor_5 = "494";
//                        break;
                }
            }
        });

        GFactor6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_6 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor32_7_1:
                        factor_6 = "1148";
                        break;
                    case R.id.radioFactor32_7_2:
                        factor_6 = "1149";
                        break;
                    case R.id.radioFactor32_7_3:
                        factor_6 = "1184";
                        break;
                }
            }
        });

        GFactor7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_7 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor32_8_1:
                        factor_7 = "1150";
                        break;
                    case R.id.radioFactor32_8_2:
                        factor_7 = "1151";
                        break;
//                    case R.id.radioFactor32_8_3:
//                        factor_7 = "500";
//                        break;
                }
            }
        });

        GFactor8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_8 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor32_9_1:
                        factor_8 = "1152";
                        break;
                    case R.id.radioFactor32_9_2:
                        factor_8 = "1153";
                        break;
//                    case R.id.radioFactor32_9_3:
//                        factor_8 = "503";
//                        break;
                }
            }
        });
        GFactor9.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_9 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor32_10_1:
                        factor_9 = "1154";
                        break;
                    case R.id.radioFactor32_10_2:
                        factor_9 = "1155";
                        break;
//                    case R.id.radioFactor32_10_3:
//                        factor_9 = "503";
//                        break;
                }
            }
        });
        return view;
    }
}