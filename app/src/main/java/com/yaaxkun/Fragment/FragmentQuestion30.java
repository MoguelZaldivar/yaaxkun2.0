package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion30 extends Fragment {
    private String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null, factor_6=null, factor_7=null, factor_8=null, factor_9=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false, checkFactor_6 = false, checkFactor_7 = false, checkFactor_8 = false, checkFactor_9 = false;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question30, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor30);
        Button next = (Button) view.findViewById(R.id.nextFactor30);
        final TextView text = (TextView) view.findViewById(R.id.textFactor30_3);
        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor30);
        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor30_2);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor30_3);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor30_4);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor30_5);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor30_6);
        final RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.groupFactor30_7);
        final RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.groupFactor30_8);
        final RadioGroup GFactor8 = (RadioGroup) view.findViewById(R.id.groupFactor30_9);
        final RadioGroup GFactor9 = (RadioGroup) view.findViewById(R.id.groupFactor30_10);
        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((SurveyActivity)getActivity()).borrarItem(168);
                ((SurveyActivity)getActivity()).borrarItem(577);
                ((SurveyActivity)getActivity()).borrarItem(576);
                ((SurveyActivity)getActivity()).borrarItem(575);
                ((SurveyActivity)getActivity()).borrarItem(574);
                ((SurveyActivity)getActivity()).borrarItem(573);
                ((SurveyActivity)getActivity()).borrarItem(572);
                ((SurveyActivity)getActivity()).borrarItem(571);
                ((SurveyActivity)getActivity()).borrarItem(570);
                ((SurveyActivity) getActivity()).selectFragment(20);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();

                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
                List<String> respuesta7 = new ArrayList<String>();
                List<String> respuesta8 = new ArrayList<String>();
                List<String> respuesta9 = new ArrayList<String>();
                if(!checkFactor_1|| !checkFactor_3 || !checkFactor_4 || !checkFactor_5 || !checkFactor_6 || !checkFactor_7 || !checkFactor_8 || !checkFactor_9){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    if (factor_1.equals("1109") || (factor_1.equals("1108") && checkFactor_2)){
                        respuesta1.add(factor_1);
                        ((SurveyActivity)getActivity()).crearItem(578, "simple", respuesta1);
                        respuesta2.add(factor_2);
                        ((SurveyActivity)getActivity()).crearItem(579, "simple", respuesta2);
                        respuesta3.add(factor_3);
                        ((SurveyActivity)getActivity()).crearItem(580, "simple", respuesta3);
                        respuesta4.add(factor_4);
                        ((SurveyActivity)getActivity()).crearItem(581, "simple", respuesta4);
                        respuesta5.add(factor_5);
                        ((SurveyActivity)getActivity()).crearItem(582, "simple", respuesta5);
                        respuesta6.add(factor_6);
                        ((SurveyActivity)getActivity()).crearItem(583, "simple", respuesta6);
                        respuesta7.add(factor_7);
                        ((SurveyActivity)getActivity()).crearItem(584, "simple", respuesta7);
                        respuesta8.add(factor_8);
                        ((SurveyActivity)getActivity()).crearItem(585, "simple", respuesta8);
                        respuesta9.add(factor_9);
                        ((SurveyActivity)getActivity()).crearItem(586, "simple", respuesta9);

                        ((SurveyActivity) getActivity()).selectFragment(22);
                    }else{
                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
        text.setVisibility(View.GONE);
        GFactor2.setVisibility(View.GONE);
        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor30_2_1:
                        factor_1 = "1108";

                        text.setVisibility(View.VISIBLE);
                        GFactor2.setVisibility(View.VISIBLE);

                        break;
                    case R.id.radioFactor30_2_2:
                        GFactor2.clearCheck();
                        factor_1 = "1109";
                        factor_2 = null;
                        checkFactor_2 = false;
                        text.setVisibility(View.GONE);
                        GFactor2.setVisibility(View.GONE);
                        break;
//                    case R.id.radioFactor30_2_3:
//                        factor_1 = "414";
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor30_3_1:
                        factor_2 = "1110";
                        break;
                    case R.id.radioFactor30_3_2:
                        factor_2 = "1111";
                        break;
//                    case R.id.radioFactor30_3_3:
//                        factor_2 = "415";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor30_4_1:
                        factor_3 = "1112";
                        break;
                    case R.id.radioFactor30_4_2:
                        factor_3 = "1113";
                        break;
//                    case R.id.radioFactor30_4_3:
//                        factor_3 = "416";
//                        break;
                }
            }
        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor30_5_1:
                        factor_4 = "1114";
                        break;
                    case R.id.radioFactor30_5_2:
                        factor_4 = "1115";
                        break;
//                    case R.id.radioFactor30_5_3:
//                        factor_4 = "417";
//                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor30_6_1:
                        factor_5 = "1116";
                        break;
                    case R.id.radioFactor30_6_2:
                        factor_5 = "1117";
                        break;
//                    case R.id.radioFactor30_6_3:
//                        factor_5 = "418";
//                        break;
                }
            }
        });

        GFactor6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_6 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor30_7_1:
                        factor_6 = "1118";
                        break;
                    case R.id.radioFactor30_7_2:
                        factor_6 = "1119";
                        break;
//                    case R.id.radioFactor30_7_3:
//                        factor_6 = "419";
//                        break;
                }
            }
        });

        GFactor7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_7 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor30_8_1:
                        factor_7 = "1120";
                        break;
                    case R.id.radioFactor30_8_2:
                        factor_7 = "1121";
                        break;
//                    case R.id.radioFactor30_8_3:
//                        factor_7 = "420";
//                        break;
                }
            }
        });

        GFactor8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_8 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor30_9_1:
                        factor_8 = "1122";
                        break;
                    case R.id.radioFactor30_9_2:
                        factor_8 = "1123";
                        break;
//                    case R.id.radioFactor30_9_3:
//                        factor_8 = "421";
//                        break;
                }
            }
        });
        GFactor9.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_9 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor30_10_1:
                        factor_9 = "1124";
                        break;
                    case R.id.radioFactor30_10_2:
                        factor_9 = "1125";
                        break;
//                    case R.id.radioFactor30_10_3:
//                        factor_9 = "422";
//                        break;
                }
            }
        });


        return view;
    }

}