package com.yaaxkun.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion24 extends Fragment {
    private String factor_1=null, factor_2=null, factor_3=null, factor_4=null, factor_5=null, factor_6=null, factor_7=null, factor_8=null;
    boolean checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkFactor_4 = false, checkFactor_5 = false, checkFactor_6 = false, checkFactor_7 = false, checkFactor_8 = false;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_question24, container, false);

        Button prev = (Button) view.findViewById(R.id.prevFactor24);
        Button next = (Button) view.findViewById(R.id.nextFactor24);
        final ScrollView viewScroll = (ScrollView) view.findViewById(R.id.scrollFactor24);

        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.groupFactor24_1);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.groupFactor24_2);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.groupFactor24_3);
        final RadioGroup GFactor4 = (RadioGroup) view.findViewById(R.id.groupFactor24_4);
        final RadioGroup GFactor5 = (RadioGroup) view.findViewById(R.id.groupFactor24_5);
        final RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.groupFactor24_6);
        final RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.groupFactor24_7);
        final RadioGroup GFactor8 = (RadioGroup) view.findViewById(R.id.groupFactor24_8);
        viewScroll.fullScroll(ScrollView.FOCUS_UP);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(519);
                ((SurveyActivity)getActivity()).borrarItem(518);
                ((SurveyActivity)getActivity()).borrarItem(517);
                ((SurveyActivity)getActivity()).borrarItem(516);
                ((SurveyActivity)getActivity()).borrarItem(515);
//                ((SurveyActivity)getActivity()).borrarItem(316);

                ((SurveyActivity)getActivity()).selectFragment(13);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<String> respuesta = new ArrayList<String>();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
                List<String> respuesta7 = new ArrayList<String>();
                List<String> respuesta8 = new ArrayList<String>();

                if(!checkFactor_1|| !checkFactor_2 || !checkFactor_3 || !checkFactor_4 || !checkFactor_5 || !checkFactor_6 || !checkFactor_7 || !checkFactor_8){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    respuesta1.add(factor_1);
                    ((SurveyActivity)getActivity()).crearItem(520, "simple", respuesta1);
                    respuesta2.add(factor_2);
                    ((SurveyActivity)getActivity()).crearItem(521, "simple", respuesta2);
                    respuesta3.add(factor_3);
                    ((SurveyActivity)getActivity()).crearItem(522, "simple", respuesta3);
                    respuesta4.add(factor_4);
                    ((SurveyActivity)getActivity()).crearItem(523, "simple", respuesta4);
                    respuesta5.add(factor_5);
                    ((SurveyActivity)getActivity()).crearItem(524, "simple", respuesta5);
                    respuesta6.add(factor_6);
                    ((SurveyActivity)getActivity()).crearItem(525, "simple", respuesta6);
                    respuesta7.add(factor_7);
                    ((SurveyActivity)getActivity()).crearItem(526, "simple", respuesta7);
                    respuesta8.add(factor_8);
                    ((SurveyActivity)getActivity()).crearItem(527, "simple", respuesta8);

                    ((SurveyActivity)getActivity()).selectFragment(15);
                }

            }
        });

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor24_1_1:
                        factor_1 = "992";

                        break;
                    case R.id.radioFactor24_1_2:
                        factor_1 = "993";

                        break;
//                    case R.id.radioFactor24_1_3:
//                        factor_1 = "263";
//
//                        break;
                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor24_2_1:
                        factor_2 = "994";
                        break;
                    case R.id.radioFactor24_2_2:
                        factor_2 = "995";
                        break;
//                    case R.id.radioFactor24_2_3:
//                        factor_2 = "266";
//                        break;
                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor24_3_1:
                        factor_3 = "996";
                        break;
                    case R.id.radioFactor24_3_2:
                        factor_3 = "997";
                        break;
//                    case R.id.radioFactor24_3_3:
//                        factor_3 = "269";
//                        break;
                }
            }
        });
//        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
//        {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId)
//            {
//                checkFactor_4 = true;
//                switch(checkedId)
//                {
//                    case R.id.radioFactor24_4_1:
//                        factor_4 = "270";
//
//                        viewScroll.setVisibility(View.VISIBLE);
//                        break;
//                    case R.id.radioFactor24_4_2:
//                        factor_4 = "271";
//
//                        factor_5 = "275";
//                        factor_6 = "278";
//                        factor_7 = "281";
////                        GFactor1.clearCheck();
////                        GFactor2.clearCheck();
////                        GFactor3.clearCheck();
////                        GFactor4.clearCheck();
//                        GFactor5.clearCheck();
//                        GFactor6.clearCheck();
//                        GFactor7.clearCheck();
//                        viewScroll.setVisibility(View.GONE);
//                        break;
//                    case R.id.radioFactor24_4_3:
//                        factor_4 = "272";
//
//                        factor_5 = "275";
//                        factor_6 = "278";
//                        factor_7 = "281";
////                        GFactor1.clearCheck();
////                        GFactor2.clearCheck();
////                        GFactor3.clearCheck();
////                        GFactor4.clearCheck();
//                        GFactor5.clearCheck();
//                        GFactor6.clearCheck();
//                        GFactor7.clearCheck();
//                        viewScroll.setVisibility(View.GONE);
//                        break;
//                }
//            }
//        });
        GFactor4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_4 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor24_4_1:
                        factor_4 = "998";
                        break;
                    case R.id.radioFactor24_4_2:
                        factor_4 = "999";
                        break;
//                    case R.id.radioFactor24_4_3:
//                        factor_4 = "272";
//                        break;
                }
            }
        });
        GFactor5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_5 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor24_5_1:
                        factor_5 = "1000";
                        break;
                    case R.id.radioFactor24_5_2:
                        factor_5 = "1001";

                        break;
//                    case R.id.radioFactor24_5_3:
//                        factor_5 = "275";
//                        break;
                }
            }
        });
        GFactor6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_6 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor24_6_1:
                        factor_6 = "1002";
                        break;
                    case R.id.radioFactor24_6_2:
                        factor_6 = "1003";
                        break;
//                    case R.id.radioFactor24_6_3:
//                        factor_6 = "278";
//                        break;
                }
            }
        });
        GFactor7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_7 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor24_7_1:
                        factor_7 = "1004";
                        break;
                    case R.id.radioFactor24_7_2:
                        factor_7 = "1005";
                        break;
//                    case R.id.radioFactor24_7_3:
//                        factor_7 = "281";
//                        break;
                }
            }
        });
        GFactor8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_8 = true;
                switch(checkedId)
                {
                    case R.id.radioFactor24_8_1:
                        factor_8 = "1006";
                        break;
                    case R.id.radioFactor24_8_2:
                        factor_8 = "1007";
                        break;
//                    case R.id.radioFactor24_8_3:
//                        factor_8 = "281";
//                        break;
                }
            }
        });

        return view;
    }
}
