package com.yaaxkun.Fragment;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.yaaxkun.Activity.SurveyActivity;
import com.yaaxkun.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentQuestion15 extends Fragment {
    private int itemID = 398, responseID = 443;
    private String factor_Main = null,factor_1=null, factor_2=null, factor_3=null;
    boolean checkFactor_Main = false, checkFactor_1 = false, checkFactor_2 = false, checkFactor_3 = false, checkPrueba=false, checkPrueba2=false;
    EditText prestada, rentada, otraSit, reubicacion,  monto, quien;

    CheckBox chkR3_1, chkR3_2, chkR3_3, chkR3_4, chkR3_5, chkR3_6, chkR3_7;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.fragment_question15, container, false);
        final ScrollView scroll = (ScrollView) view.findViewById(R.id.SV1);
        final ConstraintLayout conLay2 = (ConstraintLayout) view.findViewById(R.id.constraintLayout2);
        final ConstraintLayout conLay3 = (ConstraintLayout) view.findViewById(R.id.constraintLayout3);
        final ConstraintLayout conLay4 = (ConstraintLayout) view.findViewById(R.id.constraintLayout4);
        final ConstraintLayout conLay5 = (ConstraintLayout) view.findViewById(R.id.constraintLayout5);
        final ConstraintLayout conLay5_1 = (ConstraintLayout) view.findViewById(R.id.constraintLayout5_1);
        final ConstraintLayout conLay6 = (ConstraintLayout) view.findViewById(R.id.constraintLayout6);
        final ConstraintLayout conLay6_1 = (ConstraintLayout) view.findViewById(R.id.constraintLayout6_1);

        chkR3_1 = (CheckBox) view.findViewById(R.id.chkOp1A);
        chkR3_2 = (CheckBox) view.findViewById(R.id.chkOp2A);
        chkR3_3 = (CheckBox) view.findViewById(R.id.chkOp3A);
        chkR3_4 = (CheckBox) view.findViewById(R.id.chkOp4A);
        chkR3_5 = (CheckBox) view.findViewById(R.id.chkOp5A);
        chkR3_6 = (CheckBox) view.findViewById(R.id.chkOp6A);
        chkR3_7 = (CheckBox) view.findViewById(R.id.chkOp7A);

        Button next = (Button) view.findViewById(R.id.bNext);
        Button back = (Button) view.findViewById(R.id.bBack);
        RadioGroup GFactorMain = (RadioGroup) view.findViewById(R.id.rGM);
        final RadioGroup GFactor1 = (RadioGroup) view.findViewById(R.id.rG1);
        final RadioGroup GFactor2 = (RadioGroup) view.findViewById(R.id.rG2);
        final RadioGroup GFactor3 = (RadioGroup) view.findViewById(R.id.rG3);

//        RadioGroup GFactor6 = (RadioGroup) view.findViewById(R.id.rG6);
        prestada = (EditText) view.findViewById(R.id.prestadaText);
        rentada = (EditText) view.findViewById(R.id.rentadaText);
        otraSit = (EditText) view.findViewById(R.id.otraSituacionText);
        reubicacion = (EditText) view.findViewById(R.id.reubicaionText);
        monto = (EditText) view.findViewById(R.id.montoText);
        quien = (EditText) view.findViewById(R.id.quienText);
//        RadioGroup GFactor7 = (RadioGroup) view.findViewById(R.id.rG7);
//        final EditText tOtros = (EditText)view.findViewById(R.id.eTOtros9);
        scroll.fullScroll(ScrollView.FOCUS_UP);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String factor_7 = tOtros.getText().toString().toUpperCase();
                List<String> respuesta1 = new ArrayList<String>();
                List<String> respuesta2 = new ArrayList<String>();
                List<String> respuesta3 = new ArrayList<String>();
                List<String> respuesta4 = new ArrayList<String>();
                List<String> respuesta5 = new ArrayList<String>();
                List<String> respuesta6 = new ArrayList<String>();
                List<String> respuesta7 = new ArrayList<String>();
                List<String> respuesta8 = new ArrayList<String>();
                List<String> respuesta9 = new ArrayList<String>();
                List<String> respuesta10 = new ArrayList<String>();
                List<String> respuesta11 = new ArrayList<String>();

                boolean checkFactor_Service = false;

                String result = "Servicios";
                if( chkR3_1.isChecked()){checkFactor_Service = true; result += "\nTele";}
                if( chkR3_2.isChecked()){checkFactor_Service = true; result += "\nInternet";}
                if( chkR3_3.isChecked()){checkFactor_Service = true; result += "\ntelefono cel";}
                if( chkR3_4.isChecked()){checkFactor_Service = true; result += "\ntelefono fijo";}
                if( chkR3_5.isChecked()){checkFactor_Service = true; result += "\nLuz";}
                if( chkR3_6.isChecked()){checkFactor_Service = true; result += "\nagua";}
                if( chkR3_7.isChecked()){checkFactor_Service = true; result += "\nbasura";}

                Log.d("mensaje", "Respuestas 29: "+ result);
//                List<String> respuesta7 = new ArrayList<String>();
//                ((SurveyActivity)getActivity()).hideKeyboard();
                String factor_Prestada = prestada.getText().toString().toUpperCase();
                String factor_Rentada = rentada.getText().toString().toUpperCase();
                String factor_OtraSit = otraSit.getText().toString().toUpperCase();
                String factor_Reubicacion = reubicacion.getText().toString().toUpperCase();
                String factor_Monto = monto.getText().toString().toUpperCase();
                String factor_Quien = quien.getText().toString().toUpperCase();
                boolean pass = false;
                if(checkFactor_2){if(factor_2.equals(Integer.toString(responseID+7)) && !factor_Reubicacion.equals("")){pass = true;}}
                if(checkFactor_3){if(factor_3.equals(Integer.toString(responseID+12)) && ( !factor_Monto.equals("") && !factor_Quien.equals(""))){pass = true;}}

                if(!checkFactor_Main || !checkFactor_Service ){

                    Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos" ,Toast.LENGTH_SHORT).show();
                }else {
                    if (checkPrueba || checkPrueba2 && (
                            (factor_Main.equals(Integer.toString(responseID + 1)) && !factor_Prestada.equals("")) ||
                                    (factor_Main.equals(Integer.toString(responseID + 2)) && !factor_Rentada.equals("")) ||
                                    (factor_Main.equals(Integer.toString(responseID + 4)) && !factor_OtraSit.equals("")) || pass)) {


                        respuesta1.add(factor_Main);
                        ((SurveyActivity) getActivity()).crearItem(itemID, "simple", respuesta1);

                        respuesta2.add(factor_Prestada);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 1, "text", respuesta2);

                        respuesta3.add(factor_Rentada);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 2, "text", respuesta3);

                        respuesta4.add(factor_OtraSit);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 3, "text", respuesta4);

                        respuesta5.add(factor_1);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 4, "simple", respuesta5);

                        respuesta6.add(factor_2);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 5, "simple", respuesta6);

                        respuesta7.add(factor_Reubicacion);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 6, "text", respuesta7);
//
                        respuesta8.add(factor_3);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 7, "simple", respuesta8);

                        respuesta9.add(factor_Monto);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 8, "text", respuesta9);

                        respuesta10.add(factor_Quien);
                        ((SurveyActivity) getActivity()).crearItem(itemID + 9, "text", respuesta10);

                        if (chkR3_1.isChecked()) {
                            respuesta11.add(Integer.toString(responseID + 13));
                        }
                        if (chkR3_2.isChecked()) {
                            respuesta11.add(Integer.toString(responseID + 14));
                        }
                        if (chkR3_3.isChecked()) {
                            respuesta11.add(Integer.toString(responseID + 15));
                        }
                        if (chkR3_4.isChecked()) {
                            respuesta11.add(Integer.toString(responseID + 16));
                        }
                        if (chkR3_5.isChecked()) {
                            respuesta11.add(Integer.toString(responseID + 17));
                        }
                        if (chkR3_6.isChecked()) {
                            respuesta11.add(Integer.toString(responseID + 18));
                        }
                        if (chkR3_7.isChecked()) {
                            respuesta11.add(Integer.toString(responseID + 19));
                        }

                        ((SurveyActivity) getActivity()).crearItem(itemID + 10, "multiple", respuesta11);
                        ((SurveyActivity)getActivity()).hideKeyboard(v);
                        ((SurveyActivity) getActivity()).selectFragment(16);
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), "Favor de contestar todos los reactivos", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity)getActivity()).borrarItem(itemID-1);
                ((SurveyActivity)getActivity()).borrarItem(itemID-2);
                ((SurveyActivity)getActivity()).borrarItem(itemID-3);
                ((SurveyActivity)getActivity()).borrarItem(itemID-4);
                ((SurveyActivity)getActivity()).borrarItem(itemID-5);
                ((SurveyActivity)getActivity()).borrarItem(itemID-6);
                ((SurveyActivity)getActivity()).borrarItem(itemID-7);
                ((SurveyActivity)getActivity()).borrarItem(itemID-8);
                ((SurveyActivity)getActivity()).hideKeyboard(v);
//                ((SurveyActivity)getActivity()).hideKeyboard();
                ((SurveyActivity)getActivity()).selectFragment(14);
            }
        });

        conLay2.setVisibility(view.GONE);
        conLay3.setVisibility(view.GONE);
        conLay4.setVisibility(view.GONE);
        conLay5.setVisibility(view.GONE);
        conLay5_1.setVisibility(view.GONE);
        conLay6.setVisibility(view.GONE);
        conLay6_1.setVisibility(view.GONE);
        reubicacion.setVisibility(View.GONE);


        GFactorMain.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_Main = true;
                switch(checkedId)
                {
                    case R.id.rdb1_1:
                        GFactor1.clearCheck();
                        GFactor2.clearCheck();

                        factor_Main = Integer.toString(responseID);
                        factor_1 = null;
                        factor_2 = null;

                        prestada.setText("");
                        rentada.setText("");
                        otraSit.setText("");
                        reubicacion.setText("");

                        checkFactor_1 = false;
                        checkFactor_2 = false;
                        checkPrueba = false;
                        checkPrueba2 = false;


                        conLay2.setVisibility(view.GONE);
                        conLay3.setVisibility(view.GONE);
                        conLay4.setVisibility(view.GONE);
                        conLay5.setVisibility(view.GONE);
                        conLay5_1.setVisibility(view.GONE);
                        conLay6.setVisibility(view.VISIBLE);
//                        conLay6_1.setVisibility(view.GONE);
                        reubicacion.setVisibility(View.GONE);
                        break;
                    case R.id.rdb1_2:
                        GFactor1.clearCheck();
                        GFactor2.clearCheck();
                        GFactor3.clearCheck();

                        factor_Main = Integer.toString(responseID+1);
                        factor_1 = null;
                        factor_2 = null;
                        factor_3 = null;

                        rentada.setText("");
                        otraSit.setText("");
                        reubicacion.setText("");
                        monto.setText("");
                        quien.setText("");

                        checkFactor_1 = false;
                        checkFactor_2 = false;
                        checkFactor_3 = false;
                        checkPrueba = false;
                        checkPrueba2 = true;

                        conLay2.setVisibility(view.VISIBLE);
                        conLay3.setVisibility(view.GONE);
                        conLay4.setVisibility(view.GONE);
                        conLay5.setVisibility(view.GONE);
                        conLay5_1.setVisibility(view.GONE);
                        conLay6.setVisibility(view.GONE);
                        conLay6_1.setVisibility(view.GONE);
                        reubicacion.setVisibility(View.GONE);
//                        viewScroll.fullScroll(ScrollView.FOCUS_UP);
                        break;
                    case R.id.rdb1_3:
                        GFactor1.clearCheck();
                        GFactor2.clearCheck();
                        GFactor3.clearCheck();

                        factor_Main = Integer.toString(responseID+2);
                        factor_1 = null;
                        factor_2 = null;
                        factor_3 = null;

                        prestada.setText("");
                        otraSit.setText("");
                        reubicacion.setText("");
                        monto.setText("");
                        quien.setText("");

                        checkFactor_1 = false;
                        checkFactor_2 = false;
                        checkFactor_3 = false;
                        checkPrueba = false;
                        checkPrueba2 = true;

                        conLay2.setVisibility(view.GONE);
                        conLay3.setVisibility(view.VISIBLE);
                        conLay4.setVisibility(view.GONE);
                        conLay5.setVisibility(view.GONE);
                        conLay5_1.setVisibility(view.GONE);
                        conLay6.setVisibility(view.GONE);
                        conLay6_1.setVisibility(view.GONE);
                        reubicacion.setVisibility(View.GONE);
//                        viewScroll.fullScroll(ScrollView.FOCUS_UP);
                        break;
                    case R.id.rdb1_4:
                        GFactor3.clearCheck();

                        factor_Main = Integer.toString(responseID+3);
                        factor_3 = null;

                        prestada.setText("");
                        rentada.setText("");
                        otraSit.setText("");
                        monto.setText("");
                        quien.setText("");

                        checkFactor_3 = false;
                        checkPrueba = false;
                        checkPrueba2 = false;

                        conLay2.setVisibility(view.GONE);
                        conLay3.setVisibility(view.GONE);
                        conLay4.setVisibility(view.GONE);
                        conLay5.setVisibility(view.VISIBLE);
//                        conLay5_1.setVisibility(view.GONE);
                        conLay6.setVisibility(view.GONE);
                        conLay6_1.setVisibility(view.GONE);

//                        viewScroll.fullScroll(ScrollView.FOCUS_UP);
                        break;
                    case R.id.rdb1_5:
                        GFactor1.clearCheck();
                        GFactor2.clearCheck();
                        GFactor3.clearCheck();

                        factor_Main = Integer.toString(responseID+4);
                        factor_1 = null;
                        factor_2 = null;
                        factor_3 = null;

                        prestada.setText("");
                        rentada.setText("");
                        reubicacion.setText("");
                        monto.setText("");
                        quien.setText("");

                        checkFactor_1 = false;
                        checkFactor_2 = false;
                        checkFactor_3 = false;
                        checkPrueba = false;
                        checkPrueba2 = true;

                        conLay2.setVisibility(view.GONE);
                        conLay3.setVisibility(view.GONE);
                        conLay4.setVisibility(view.VISIBLE);
                        conLay5.setVisibility(view.GONE);
                        conLay5_1.setVisibility(view.GONE);
                        conLay6.setVisibility(view.GONE);
                        conLay6_1.setVisibility(view.GONE);
                        reubicacion.setVisibility(View.GONE);
                        break;

                }
            }
        });

        GFactor1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_1 = true;
                switch(checkedId)
                {
                    case R.id.rdb1Si:
                        factor_1 = Integer.toString(responseID+5);
                        checkPrueba = false;
                        checkPrueba2 = false;
                        conLay5_1.setVisibility(view.VISIBLE);
                        break;
                    case R.id.rdb1No:
                        GFactor2.clearCheck();

                        factor_1 = Integer.toString(responseID+6);
                        factor_2 = null;
                        checkPrueba = true;
                        checkPrueba2 = false;
                        checkFactor_2 = false;

                        conLay5_1.setVisibility(view.GONE);
                        reubicacion.setText("");
                        break;

                }
            }
        });
        GFactor2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_2 = true;
                switch(checkedId)
                {
//                    case R.id.rdb2_1:
//                        factor_2 = Integer.toString(responseID+7);
//                        reubicacion.setVisibility(View.VISIBLE);
//                        checkPrueba = false;
//                        checkPrueba2 = true;
//                        break;
//                    case R.id.rdb2_2:
//                        factor_2 = Integer.toString(responseID+8);
//                        reubicacion.setText("");
//                        reubicacion.setVisibility(View.GONE);
//                        checkPrueba = true;
//                        checkPrueba2 = false;
//                        break;
//                    case R.id.rdb2_3:
//                        factor_2 = Integer.toString(responseID+9);
//                        reubicacion.setText("");
//                        reubicacion.setVisibility(View.GONE);
//                        checkPrueba = true;
//                        checkPrueba2 = false;
//                        break;

                    case R.id.rdb2_1:
                        factor_2 = Integer.toString(responseID+9);
                        reubicacion.setText("");
                        reubicacion.setVisibility(View.GONE);
                        checkPrueba = true;
                        checkPrueba2 = false;
                        break;
                    case R.id.rdb2_2:
                        factor_2 = Integer.toString(responseID+8);
                        reubicacion.setText("");
                        reubicacion.setVisibility(View.GONE);
                        checkPrueba = true;
                        checkPrueba2 = false;
                        break;
                    case R.id.rdb2_3:
                        factor_2 = Integer.toString(responseID+7);
                        reubicacion.setVisibility(View.VISIBLE);
                        checkPrueba = false;
                        checkPrueba2 = true;
                        break;



                }
            }
        });
        GFactor3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                checkFactor_3 = true;
                switch(checkedId)
                {
                    case R.id.rdb3_1:
                        factor_3 = Integer.toString(responseID+10);
                        checkPrueba = true;
                        checkPrueba2 = false;
                        monto.setText("");
                        quien.setText("");
                        conLay6_1.setVisibility(view.GONE);
                        break;
                    case R.id.rdb3_2:
                        factor_3 = Integer.toString(responseID+11);
                        checkPrueba = true;
                        checkPrueba2 = false;
                        monto.setText("");
                        quien.setText("");

                        conLay6_1.setVisibility(view.GONE);
                        break;
                    case R.id.rdb3_3:
                        checkPrueba = false;
                        checkPrueba2 = true;
                        factor_3 = Integer.toString(responseID+12);
                        conLay6_1.setVisibility(view.VISIBLE);
                        break;
                }
            }
        });






        return view;
    }
}
