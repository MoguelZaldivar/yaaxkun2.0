package com.yaaxkun.Interface;

import com.yaaxkun.Model.AdminReport;
import com.yaaxkun.Model.AdminReportXAgeb;
import com.yaaxkun.Model.CheckAgeb;
import com.yaaxkun.Model.ListInvalid;
import com.yaaxkun.Model.Login;
import com.yaaxkun.Model.Register;
import com.yaaxkun.Model.ReportXAgeb;
import com.yaaxkun.Model.ReportXAgebInvalid;
import com.yaaxkun.Model.ReportXEncuestados;
import com.yaaxkun.Model.SurveySend;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

public interface Interface {
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("/api/test/search")
    Call<CheckAgeb> checkAgeb (@Body CheckAgeb data);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("api/encuestador/login")
    Call<Login> login (@Body Login data);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("api/administrador/login")
    Call<Login> loginAdmin (@Body Login data);

    @Multipart
    @POST("api/encuestador/register")
    Call<Register> registerEncuestador (
            @Part("name") RequestBody name,
            @Part("fName") RequestBody fName,
            @Part("sName") RequestBody sName,
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("idDevice") RequestBody idDevice,
            @Part("cellphone") RequestBody cellphone,
            @Part("trade") RequestBody marca,
            @Part("model") RequestBody modelo,
            @Part("oper") RequestBody operadora,
            @Part("apiDevice") RequestBody api,
            @Part("sex") RequestBody sex);
    //@Part MultipartBody.Part file);

    @Multipart
    @POST("api/administrador/register")
    Call<Register> registerAdmin (
            @Part("name") RequestBody name,
            @Part("fName") RequestBody fName,
            @Part("sName") RequestBody sName,
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("idDevice") RequestBody idDevice,
            @Part("cellphone") RequestBody cellphone,
            @Part("trade") RequestBody marca,
            @Part("model") RequestBody modelo,
            @Part("oper") RequestBody operadora,
            @Part("apiDevice") RequestBody api,
            @Part("sex") RequestBody sex);
    //@Part MultipartBody.Part file);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("api/encuesta/register")
    Call<SurveySend> surveySend (@Body SurveySend data, @Header("token") String token);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("/api/reporte/enc_ageb/prototipo-zCDK")
    Call<ReportXAgeb> reportXAgeb(@Header("token") String token);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("/api/reporte/encuestador/prototipo-zCDK")
    Call<ReportXEncuestados> reportXEncuestadoe(@Header("token") String token);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("/api/reporte/enc_ageb_invalid/prototipo-zCDK")
    Call<ReportXAgebInvalid> reportXAgebInvalid(@Header("token") String token);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("/api/reporte/estadisticas-agebs/prototipo-zCDK/json")
    Call<AdminReport> AdminReport (@Header("token") String token);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("/api/reporte/ageb/{agebId}/prototipo-zCDK/json")
    Call<AdminReportXAgeb> AdminReportXAgeb (@Header("token") String token, @Path("agebId") String agebId);
}